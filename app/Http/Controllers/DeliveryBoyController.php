<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryBoy;
use DB;

class DeliveryBoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('Index');
        $deliveryBoys = DeliveryBoy::all();
        // dd($deliveryBoys);
        return view('SellerDashboard.Delivery.viewDeliveryBoy')
         ->with('deliveryBoys', $deliveryBoys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('SellerDashboard.Delivery.addDeliveryBoy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('Store');
        // $this->validate($request )
        $deliveryBoy = new DeliveryBoy;
        $deliveryBoy->seller_id = $request->seller_id;

        $deliveryBoy->firstname = $request->firstname;
        $deliveryBoy->lastname = $request->lastname;
        $deliveryBoy->email = $request->email;
        $deliveryBoy->city = $request->city;
        $deliveryBoy->pin = $request->pin;
        $deliveryBoy->address = $request->address;
        $deliveryBoy->mobile_number = $request->mobile_number;
        $deliveryBoy->gender = $request->gender;
        $deliveryBoy->save();
        return back()->with("Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deliveryBoy = DeliveryBoy::findOrFail($id);
        // dd($deliveryBoy);
        return view('SellerDashboard.Delivery.editDeliveryBoy')
        ->with('deliveryBoy', $deliveryBoy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request, $id);
        $deliveryBoy = DeliveryBoy::findOrFail($id);
        $deliveryBoy->seller_id = $request->seller_id;
        $deliveryBoy->firstname = $request->firstname;
        $deliveryBoy->lastname = $request->lastname;
        $deliveryBoy->email = $request->email;
        $deliveryBoy->city = $request->city;
        $deliveryBoy->pin = $request->pin;
        $deliveryBoy->address = $request->address;
        $deliveryBoy->mobile_number = $request->mobile_number;
        $deliveryBoy->gender = $request->gender;
        $deliveryBoy->save();
        return redirect('/Delivery-boy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $deliveryBoy = DeliveryBoy::findOrFail($id);
        $deliveryBoy->delete();
        return back();
    }
}
