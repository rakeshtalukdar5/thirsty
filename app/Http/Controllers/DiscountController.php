<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount;
use Cart;
use Session;

class DiscountController extends Controller
{
    /*========================================================================
        Function to get the discount page in admin
    ==========================================================================*/   
    public function index()
    {
        $discounts = Discount::all();
        return view('Discount.index', compact('discounts'));
    }

    /*========================================================================
        Function to set discounts by admin
    ==========================================================================*/   
    public function setDiscount(Request $request) 
    {
        $discount = new Discount;
        $discount->code = $request->code;
        $discount->type = $request->type;
        $discount->value = $request->value;
        $discount->percent_off = $request->percent_off;
        $discount->status = false;
        $discount->save();
        return back();
    
    }

    /*========================================================================
        Function to change the discount status active or expired 
    ==========================================================================*/   
    public function changeDiscountStatus($id) {
        $discount = Discount::findOrfail($id);
        $discountStatus = $discount->status;
        if($discountStatus == false) {
            $discount->status = true;
        } else {
            $discount->status = false;
        }
        $discount->save();
        return back();
    }

    /*========================================================================
        Function to delete discount code 
    ==========================================================================*/   
    public function deleteDiscountCode($id) {
        $discount = Discount::findOrFail($id);
        $discount->delete();
        return back();
    }

    /*==============================================================================
        Function to check the discount code applied by user and send response
    ==============================================================================*/   
    public function store(Request $request)
    {
        $discount = Discount::where('code', $request->coupon)->first();
        if(!$discount) {
            return back()->withErrors('Invalid Coupon Code, Please Try Again !!');
        } else {
            session()->put('coupon', [
                'name' => $discount->code,
                'discount' => $discount->getByPercentOff(Cart::subtotal()),
            ]);
        }
        return back()->with('Success', 'Coupon Successfuly Applied!!');

    }

    /*========================================================================
        Function to remove the discunt from users cart 
    ==========================================================================*/   
    public function destroy()
    {
        session()->forget('coupon');
        return back()->with('Success','Coupon Successfully Removed');
    }

    public function getDiscountOnItemPage() 
    {
        return view('Discount/discountOnItem');
    }
}
