<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Blog;
use Auth;

class BlogController extends Controller
{

    public function index() {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(4);
        return view('UI.blog', compact('blogs'));
    }

    /*========================================================================
        Function to get the blog page in admin
    ==========================================================================*/   
    public function getBlogPage() {

        return view('adminDashboard/blogs/postBlog');
    }

    /*========================================================================
        Function to post a blog by admin
    ==========================================================================*/   
    public function postBlog(Request $request) 
    {
        //Upload the image
        if($request->hasFile('image')) {
            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension(); 
            $allowedExt = ['jpg', 'jpeg', 'png', 'pdf'];
            if(in_array($extension, $allowedExt)) {
                $fileNameToStore =  request('title').'-'.$filename.'-'.time().'.'.$extension;
                $path = $request->file('image')->storeAs('public/blog_images', $fileNameToStore);
            } else {
                dd("Error in File Extension");
            }
        }

        $blog = new Blog;
        $blog->title = $request->input('title');
        $blog->posts = $request->input('post');

        if($request->hasFile('image')) {
            $blog->image =  $fileNameToStore;
        } else {
            $blog->image = 'noimage.jpg';
          }

        $blog->save();
        return back();
    }

    /*========================================================================
        Function to get the blogs in  landing page/index of customers
    ==========================================================================*/   
    public function getBlogsInIndex()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(3);
        return view('UI/index', compact('blogs'));
    }

    /*========================================================================
        Function to get a single blog     
    ==========================================================================*/   
    public function getSingleBlog($id) {
        $blog = Blog::findOrFail($id);
        // dd(gettype($blog));
        return view('UI.blogDetail', compact('blog'));
    }

    public function blogsShowInAdmin() 
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(3);
        return view('adminDashboard/blogs/viewBlogs', compact('blogs'));
    }

    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('adminDashboard/blogs/editBlog', compact('blog'));
    }

    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->title = $request->input('title');
        $blog->posts = $request->input('post');
        $blog->image = $request->input('image');
        // $blog->admin_id = 1;
        $blog->save();
        return redirect()->route('admin.blogsView');

    }

    public function delete($id) 
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();
        return back();
    }
}
