<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

use App\User;
use Auth;
use Session;
use App\Cart;
use App\Order;
use App\Product;
use App\OrderProduct;
use App\PasswordReset;
use Mail;
use App\Mail\VerifyEmail;
use App\Mail\forgotPassword;
use Illuminate\Support\Facades\Hash;
use App\Feedback;
use App\Customer;
use DB;




class UserController extends Controller
{
    
    /*=============================================================
        Function to get the sign up page for user
    ===============================================================*/
    public function getSignup() {
        return view('user/signup');
    }

    /*============================================================
        Function to perform signup for user
    ==============================================================*/
    public function postSignup(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()) {
            return redirect()->route('user.signup')
                            ->withErrors($validator)
                            ->withInput();
        }
        else if(filter_var($request->email, FILTER_VALIDATE_EMAIL) === false) {
            return back()->with('error', 'Enter a valid email id!!');
        } else {
            $user = new User;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->verify_token = Str::random(40);
            $user->role = '3';
            $user->save();
            $thisUser = User::findOrFail($user->id);
            Mail::send(new verifyEmail($thisUser));
            Session::flash('status', 'A link has been sent to your email. Please verify and activate your account');
            return redirect()->route('user.signin');
        }
    }

    /*======================================================================
        Function to send verification mail to user after post signup
    ========================================================================*/
    public function sendVerifyEmailDone($email, $verify_token) 
    {
        $user = User::where(['email' => $email, 'verify_token' => $verify_token])->first();
        if($user) {
            User::where(['email' => $email, 'verify_token' => $verify_token])
                    ->update(['status' => true, 'verify_token' => null]);
            return redirect()->route('user.signin');
        } else {
            return "User Not Found";
        }
    }


    /*========================================================
        Function to get user signin page
    ============================================================*/
    public function getSignin(){
        return view('user.signin');
    }

    /*===========================================================
        Function to perform user signin
    =============================================================*/
    public function postSignin(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()) {
            return redirect()->route('user.signin')
                            ->withErrors($validator)
                            ->withInput();
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'user', 'status' => true] )) 
        {
            return redirect()->route('landing_page');
        }
        return back()->withErrors('User Name or Password Incorrect or Email Not Verified');   
    }

    /*==================================================
        Function to get the user profile
    ====================================================*/
    public function getUserProfile() 
    {
        $user = Auth::user();
        $customer = Customer::where('user_id', $user->id)->first();
        if(isset($customer)) {
            return view('user.profile', compact('user', 'customer'));
        } else {
            $customer = null;
            return view('user.profile', compact('user', 'customer'));
        }
    }

    /*==================================================
        Function to perform user logout
    ====================================================*/
    public function getLogout() {
        Auth::logout();
        return redirect('/');
    }

    /*=================================================
        Function to get forgot password page
    ===================================================*/
    public function getForgotPassword() 
    {
        return view('user.forgotPassword');
    }

    /*============================================================================
        Function to set the password token in the database
    ===============================================================================*/
    public function setPasswordToken($user, $token, $selector, $expires, $validator) 
    {
        $hashedToken = password_hash($token, PASSWORD_DEFAULT);   
        $passwordReset = new PasswordReset;
        $passwordReset->email = $user->email;
        $passwordReset->token = $hashedToken;
        $passwordReset->selector = $selector;
        $passwordReset->expires = $expires;
        $passwordReset->save();

        Mail::send( new forgotPassword($user, $selector, $validator));
        Session::flash('status', 'A password reset link has been sent to your email');
    }

    /*=======================================================================================
        Function to check mail existence and perform necessary tasks for forgot password  
    =========================================================================================*/
    public function postForgotPassword(Request $request)    
    {
        $getEmail = $request->input('forgot_password_email');
        
        if(!empty($getEmail))
        {
            $user = User::where('email', $getEmail)->first();
            if($user) 
            {
                $selector = bin2hex(random_bytes(8));
                $token = random_bytes(32);
                $validator = bin2hex($token);
                $expires = date("U") + 1800;
                $getExistingToken = PasswordReset::where('email', $user->email)->first();

                //Delete the existing token if the it exists before saving the new token
                if($getExistingToken !== null) {
                   $deleteExistingToken = PasswordReset::findOrFail($getExistingToken->id);
                   $deleteExistingToken->delete(); 
                   $this->setPasswordToken($user, $token, $selector, $expires, $validator);
                }
                else 
                {
                    $this->setPasswordToken($user, $token, $selector, $expires, $validator);    
                }
            } 
            else 
            {
                Session::flash('error', 'This email is not registered with us');   
            }    
        } 
        else
        {
            Session::flash('error', "Email field can't be empty! Enter a valid email");
        }
        
        return back();
    }

    /*========================================================================
        Function to get the tokens from url and check the format and validate
    ==========================================================================*/
    public function getPasswordReset(Request $request)
    {
        $selector =  $request->selector; //$_GET['selector'];
        $validator =  $request->validator; //$_GET['validator'];
        if(empty($selector) || empty($validator)) 
        {
            Session::flash('error', 'Sorry!! Couldn\'t validate your request!!');
            return back();   
        } 
        else 
        {
            if(ctype_xdigit($selector) !== false && ctype_xdigit($validator) !== false) 
            {
                return view('user.passwordReset', compact('selector', 'validator'));
            }
        }
    }

     /*========================================================================
        Function to perform the password reset
    ==========================================================================*/
    public function postPasswordReset(Request $request)
    {
        $selector = $request->selector;
        $validator = $request->validator; // $_POST['validator'];
        $password = $request->reset_password;

        if(empty($password) || strlen($password) < 6) 
        {
            Session::flash("error", "Password can't be empty or length should be atleast 6 characters");
            return back();
        } 
        else 
        {
            $currentTime = date("U");
            $passwordReset = PasswordReset::where('selector', $selector, 'expires' >= $currentTime)->first();

            if($passwordReset) 
            {
                $token = $passwordReset->token;
                $newValidator = hex2bin($validator); 
                $tokenCheck = password_verify($newValidator, $token);
                if($tokenCheck === false) 
                {
                    Session::flash("error", "Resubmit your request!!");
                } 
                else 
                {
                    $user = User::where('email', $passwordReset->email)->first();
                    $user->password = bcrypt($password);
                    $user->save();

                /*=============================================================================================
                    Deleting the entry from the password_reset table after successully reseting the password
                ==============================================================================================*/
                    $getExistingToken = PasswordReset::where('email', $user->email)->first();
                    if($getExistingToken !== null) 
                    {
                        $deleteExistingToken = PasswordReset::findOrFail($getExistingToken->id);
                        $deleteExistingToken->delete();
                    } 
                    //If the User is customer then it will redirect to the user login page
                    if($user->role === 'user') {
                        return redirect()->route('user.signin');
                    }
                    //If the User is seller then it will redirect to the seller login page
                    if($user->role === 'seller') {
                        return redirect()->route('seller.login');
                    }
                    
                }
            } 
            else 
            {
                Session::flash("error", "Try again to reset your password");
            }
        }
        return back(); //view('user.passwordReset');
    }

     /*========================================================================
        Function to get the user profile edit page
    ==========================================================================*/
    public function getProfileEdit($id) 
    {
        $user = Customer::where('user_id', $id)->first();
        if($user == null) 
        {
            $user = auth()->user();
            return view('user.updateProfile', compact('user')); 
        } else 
        {
            return view('user.updateProfile', compact('user')); 
        }
    }

    /*========================================================================
        Function to update the user profile
    ==========================================================================*/
    public function updateProfile(Request $request) 
    {
        $pin = '';
        $mobileNo = '';
        $firstName = '';
        $city = '';  

        if(strlen($request->firstname) >= 3 && strlen($request->city) >= 3) {
            $firstName = $request->firstname;
            $city = $request->city;  
        } else  {
            dd('First name and City should be more than 3 characters');
            Session::flash("First name and City should be more than 3 characters");
        }

        if(is_numeric($request->pin) && is_numeric($request->mobile_number) &&  strlen($request->pin) === 6 && strlen($request->mobile_number) === 10) 
        {
            $mobileNo = $request->mobile_number;
            $pin = $request->pin;
        } else {
            dd('Enter a valid Mobile number or Pin');
            Session::flash('error', 'Enter a valid Mobile number or Pin');
        }

        $userId = Auth::user()->id;
        // dd($userId);
        $user = Customer::where('user_id', $userId)->first();
        if($user != null && $user == TRUE) {
            $user->firstname =  $firstName;
            $user->lastname = $request->input('lastname');
            $user->mobile_number = $mobileNo;
            $user->city = $city;
            $user->pin = $pin;
            $user->locality = $request->input('locality');
            $user->street = $request->input('street');
            $user->houseno = $request->input('houseno');
            $user->gender = $request->input('gender');
            $user->save();
        } else {
            $user = new Customer;
            $user->user_id = $userId;
            $user->firstname =  $firstName;
            $user->lastname = $request->input('lastname');
            $user->mobile_number = $mobileNo;
            $user->city = $city;
            $user->pin = $pin;
            $user->locality = $request->input('locality');
            $user->street = $request->input('street');
            $user->houseno = $request->input('houseno');
            $user->gender = $request->input('gender');
            $user->save();
        }
        return redirect()->route('user.profile');

    }
    public function getPasswordEdit($id) {
        $user = User::findOrFail($id);
        return view('user.updatePassword', compact('user'));
    }

    public function getCheckPasssword(Request $request, $id) {
        $data = $request->all();
        $currentPassword = $data['currentPassword'];
        $checkPassword = User::where('id', $id)->first();
        // dd($checkPassword);
        if(Hash::check($currentPassword, $checkPassword->password)) {
            echo true;   
            die();
        } else {
            echo false;
            die();  
        }
    }

    /*========================================================================
        Function to update the user password
    ==========================================================================*/
    public function updatePassword(Request $request, $id) 
    {
        $user = User::findOrFail($id);
        $data = $request->all();
        $checkPassword = User::where('email', Auth::user()->email)->first();
        $currentPassword = $data['currentPassword'];
        if(Hash::check($currentPassword, $checkPassword->password)) {
            $user->password = bcrypt($request->input('newPassword'));
            $user->save();
            return redirect()->route('user.profile');
        } else {
            echo false;
            return back()->withErrors('The Current Password You Entered is Incorrect!!');
        }
    }

    
    // public function getUserDetailsOnCheckout() {
    //     $user = Auth::user()->first();
    //     dd($user);
    // }
    public function getContactPage() {
        return view('UI/contact');
    }
    /*========================================================================
        Function to post feedback from contact page to admin {database}
    ==========================================================================*/
    public function postFeedback(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'name' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        if($validator->fails()) {
            return redirect('/contact')
                    ->withErrors($validator)
                    ->withInput();
        }

        $feedback = new Feedback;
        $feedback->name = $request->input('name');
        $feedback->email = $request->input('email');
        $feedback->subject = $request->input('subject');
        $feedback->message = $request->input('message');

        $feedback->user_id = Auth::user() == true ? auth()->user()->id :  null;
        $feedback->save();
        return back()->with('status', 'Feedback Submitted Successfully');
    }

    /*========================================================================
        Function to get the orders done by the user
    ==========================================================================*/
    public function getMyOrders() 
    {
        $user = Auth::user();
        //Joining the orders and product table 
        $myOrders = DB::table('orders')
                    // ->join('users', 'orders.user_id', '=', 'users.id')
                    ->where('user_id', '=', $user->id)
                    ->join('order_product', 'orders.id', '=', 'order_product.order_id')
                    ->join('products', 'products.id', '=', 'order_product.product_id')
                    ->select('orders.*', 'order_product.*', 'products.*')
                    ->orderBy('orders.created_at', 'DESC')
                    ->get();
            //    dd($myOrders);     
        return view('user/myOrders', compact('myOrders'));
    }

    public function cancelOrder($id) {
        $order = Order::findOrFail($id);
        $order->order_status = '9';
        $order->save();
        return back();
    }

}
