<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Seller;
use App\Order;
use App\User;
use DB;
use App\Feedback;
use Auth;
use Mail;
use App\Mail\adminVerifyEmail;
use App\Mail\forgotPassword;
use Session;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Str;
use App\Customer;


class AdminController extends Controller
{
    /*========================================================================
        Function to get the dashboard of admin
    ==========================================================================*/    
    public function index()
    {
        return view('adminDashboard.index');
    }

    /*========================================================================
        Function to get all the sellers
    ==========================================================================*/  
    public function getSellers() 
    {
        $sellers = DB::table('users')
                        // ->where('role', '=', 'Seller')
                        ->join('sellers', 'users.id', '=', 'sellers.user_id')
                        ->select('users.email', 'users.status', 'sellers.*')
                        ->orderBy('sellers.created_at', 'DESC')
                        ->paginate(10);
    
        return view('adminDashboard/sellerManagement', compact('sellers'));
    }

    /*========================================================================
        Function to get all the users
    ==========================================================================*/  
    public function getUsers() 
    {
        $users = DB::table('users')
                        ->join('customers','users.id', '=', 'customers.user_id')
                        ->select('users.email','users.status', 'customers.*')
                        ->orderBy('users.created_at', 'DESC')
                        ->paginate(10);
        return view('adminDashboard/userManagement', compact('users'));
    }

     /*========================================================================
        Function to block a user or vice versa
    ==========================================================================*/  
    public function blockUser($id) 
    {
        $user = User::findOrFail($id);
        $user->status == true ? $user->status = false : $user->status = true;
        $user->save();
        return back();
    }
    
    /*========================================================================
        Function to change order status to approved by admin
    ==========================================================================*/  
    public function changeOrderStatusToApproved($id) 
    {
        $order = Order::findOrFail($id);
        $order->order_status = '2';
        // $order->shipped = false;
        $order->save();
        return back();
    }

    /*========================================================================
        Function to get approved order by admin
    ==========================================================================*/  
    public function getApprovedOrder() 
    {
        $approvedOrders = Order::where('order_status', 'Approved')->paginate(10);
        return view('adminDashboard/orders/approvedOrder', compact('approvedOrders'));
    }

    /*========================================================================
        Function to order status of all orders
    ==========================================================================*/  
    public function getOrderStatus() 
    {
        $orders = Order::all();
        return view('adminDashboard/orders/orderStatus', compact('orders'));    
    }

    /*========================================================================
        Function to get orders which are delivered
    ==========================================================================*/  
    public function getDeliveredOrder() {
        $deliveredOrders = Order::where('order_status', 'Delivered')->paginate(10);
        return view('adminDashboard/orders/deliveredOrder', compact('deliveredOrders'));
    }

    /*========================================================================
        Function to change order status to cancelled 
    ==========================================================================*/  
    public function changeOrderStatusToCancelled($id) 
    {
        $order = Order::findOrFail($id);
        $order->order_status = '7';
        // $order->shipped = false;
        $order->save();
        return back();
    }

    /*========================================================================
        Function to get only cancelled orders in admin panel
    ==========================================================================*/  
    public function getCancelledOrder() 
    {
        $cancelledOrders = Order::where('order_status', 'Cancelled')->paginate(10);
        return view('adminDashboard/orders/cancelledOrder', compact('cancelledOrders'));
    }

    /*========================================================================
        Function to get feedbacks submited by users in admin panel
    ==========================================================================*/  
    public function getFeedback() {

        $feedbacks = Feedback::paginate(10);
        return view('adminDashboard/getFeedback', compact('feedbacks'));
    }

    /*========================================================================
        Function to delete any feedback submitted by user
    ==========================================================================*/  
    public function deleteFeedback($id) {

        $feedback = Feedback::findOrFail($id);
        $feedback->delete();
        return back();
    }
   
    /*========================================================================
        Function to get all the counnters used in admin panel dashboard
    ==========================================================================*/  
    public function getCountersOnDashboard() {
        //Get Number of orders in Admin Dashboard
        $orders = Order::where('order_status', 'Pending')->get();
        $orderCounter = count($orders);

        //Get Number of Active Products in Admin Dashboard
        $products = Product::where('status', true)->get();
        $productCounter = count($products);

        //Get Total Customers
        $users = User::where('role', 'user')->get();
        $userCounter = count($users);

        //Get Total Customers
        $sellers = User::where('role', 'seller')->get();
        $sellerCounter = count($sellers);

        //Get total Earnings from Orders
        $totalOrderAmount = Order::get()->sum('amount');  //DB::select('SELECT sum(amount) FROM orders');
        // dd(gettype($totalOrderAmount) );
        return view('adminDashboard/index', compact('productCounter', 'orderCounter', 'userCounter', 'sellerCounter', 'totalOrderAmount'));
    }


    
    /*========================================================================
        Function to verify a seller by the admin
    ==========================================================================*/  
    public function verifySeller($id) 
    {
        $seller = Seller::findOrFail($id);
        $seller->isVerified == true ? $seller->isVerified = false : $seller->isVerified = true;
        $seller->save();
        return back();
    }

    /*========================================================================
        Function to search order status by the admin
    ==========================================================================*/  
    public function searchOrderStatus(Request $request) {
        $search = $request->search;
        $resultCount = '';
        if(!empty($search)) {
            $searchOrders = Order::where('id', 'LIKE', "%$search")
                                ->orWhere('pin', 'LIKE', "%$search")
                                ->orWhere('billing_email', 'LIKE', "%$search")
                                ->orWhere('mobile_number', 'LIKE', "%$search")
                                ->orWhere('city', 'LIKE', "%$search%")
                                ->orWhere('locality', 'LIKE', "%$search%")
                                ->get();
            
            $resultCount = count($searchOrders);
            return view('adminDashboard/orders/searchOrderStatus', compact('resultCount', 'searchOrders'));    
        } else {
            $resultCount = 0;
            return view('adminDashboard/orders/searchOrderStatus', compact('resultCount'));
        }
    }



    /*========================================================================
        Functions to perform login, logout, sign up
    ==========================================================================*/ 
    //Function to get the sign up page
    public function getRegister() 
    {
        return view('adminDashboard/Registrations/register');
    }
        
    //Function to perform the sign up for user
    public function postRegister(Request $request) {
        // dd(123);
        $this->validate($request, 
        [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:6',
        ]);
        $user = new User;
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->role = '1';
        // Auth::login($seller);
        if(strlen($user->password) < 6) {
            return back()->withErrors('Password Length should more than 5!!');
        } 
        $user->verify_token = Str::random(40);
        $user->save();
        $thisUser = User::findOrFail($user->id);
        // $this->sendVerificationMail($thisUser);
        Mail::send(new adminVerifyEmail($thisUser));
        // dd($thisUser);
        // Auth::login($user);
        Session::flash('status', 'A link has been sent to your email. Please verify and activate your account');
        return redirect()->route('admin.login');
    }
        
    public function sendVerifyAdminEmail($email, $verify_token) 
    {
        $user = User::where(['email' => $email, 'verify_token' => $verify_token])->first();
        if($user) {
            User::where(['email' => $email, 'verify_token' => $verify_token])
                    ->update(['status' => true, 'verify_token' => null]);
            return redirect()->route('admin.login');
        } else {
            return "User Not Found";
        }
    }
        
    
    //Function to get the User signin Page
    public function getLogin(){
        return view('adminDashboard/Registrations/login');
    }

    //Function for user sign in
    public function postLogin(Request $request) 
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required',
        ]);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'admin',  'status' => true])) 
        {
            return redirect()->route('admin.dashboard');
        }
        return back()->withErrors('User Name or Password Incorrect or Email not verified');   
    }
    
    //Function for user log out
    public function getLogout() 
    {
        Auth::logout();
        return redirect('/admin');
    }
        
}
