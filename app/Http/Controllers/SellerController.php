<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Validator;

use App\Seller;
use App\Product;
use App\User;
use Mail;
use App\Mail\sellerVerifyEmail;
use App\Mail\forgotPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use App\Order;

class SellerController extends Controller
{
  
    public function index()
    {
        return view('SellerDashboard.index');
    }

    /*==============================================================
            Function to get the sign up page
    ================================================================*/
     public function getRegister() {
        return view('SellerDashboard/Registrations/register');
    }

    /*==============================================================
            Function to perform sign up for Seller 
    ================================================================*/
    public function postRegister(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()) {
            return redirect()->route('seller.register')
                            ->withErrors($validator)
                            ->withInput();
        }

        $user = new User;
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        if(strlen($user->password) < 6) {
            return back()->withErrors('Password Length should more than 5!!');
        } 
        $user->verify_token = Str::random(40);
        $user->role = '2';
        $user->save();
        $thisUser = User::findOrFail($user->id);
        Mail::send(new sellerVerifyEmail($thisUser));
        Session::flash('status', 'A link has been sent to your email. Please verify and activate your account');
        return redirect()->route('seller.login');

    }
    
    /*==============================================================
            Function to sned verification maile after post reigster 
    ================================================================*/
    public function sendVerifySellerEmail($email, $verify_token) 
    {
        $user = User::where(['email' => $email, 'verify_token' => $verify_token])->first();
        if($user) {
            User::where(['email' => $email, 'verify_token' => $verify_token])
                    ->update(['status' => true, 'verify_token' => null]);
            return redirect()->route('seller.login');
        } else {
            return "User Not Found";
        }
    }
  

   /*==============================================================
            Function to get the login page
    ================================================================*/
    public function getLogin()
    {
        return view('SellerDashboard/Registrations/login');
    }

    /*==============================================================
            Function to perform login 
    ================================================================*/
    public function postLogin(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->route('seller.login')
                            ->withErrors($validator)
                            ->withInput();
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'seller', 'status' => true] ))
        {
            return redirect()->route('seller.dashboard');
        }
        return back()->withErrors('User Name or Password Incorrect or Email Not Verified');
    }

    /*==============================================================
            Function to perform logout
    ================================================================*/
    public function getLogout() 
    {
        Auth::logout();
        return redirect('/seller');
    }

       
    /*==============================================================
            Function to get sellet registration page
    ================================================================*/
    public function getSellerRegistration() 
    {
        return view('SellerDashboard/Registrations/registerDetails'); 
    }

     /*==============================================================
            Function to get the seller profile
    ================================================================*/
     public function getSellerProfile() 
     {
         $userId = auth()->user()->id;
         $userEmail = auth()->user()->email;
         $seller = Seller::where('user_id', $userId)->get();
         return view('SellerDashboard/sellerProfile', compact('seller', 'userEmail')); 
     }

    /*==============================================================
            Function to perform Seller profile registration
    ================================================================*/
    public function registerSellerDetails(Request $request) 
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'registration_id' => 'required',
            'mobile_number' => 'required|numeric|digits:10',
            'shop_name' => 'required',
            'pin' => 'required|digits:6|numeric',
            'city' => 'required',
            'locality' => 'required',
            'street' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/seller/Registration')
                        ->withErrors($validator)
                        ->withInput();
        }

        $seller = new Seller;
        $seller->user_id = $user->id;
        $seller->firstname = $request->input('first_name');
        $seller->lastname = $request->input('last_name');
        $seller->registration_id = $request->input('registration_id');
        $seller->mobile_number = $request->input('mobile_number');
        $seller->shop_name = $request->input('shop_name');
        $seller->pin = $request->input('pin');
        $seller->city = $request->input('city');
        $seller->locality = $request->input('locality');
        $seller->street = $request->input('street');
        $seller->save();
        return redirect()->route('seller.Profile');

    }
    
 
    /*==============================================================
        Function to check seller availabilty for particular pin 
    ================================================================*/
    public function locationCheck(Request $request) {
        $pin = $request->input('pin');
        $sellers = Seller::where('pin', $pin)->get()->all();
        return view('UI/checkSeller', compact('sellers'));
    }


    
    public function getRegistration() {

        return view('SellerDashboard/Registrations/registerDetails'); 
    }


/*
    public function getProductView() {
        // $productWithBrands = ProductWithBrand::all();
        
        $productWithBrands = DB::select('select * FROM products
        INNER JOIN product_with_brands ON product_with_brands.product_id = products.id');



        // $productWithBrands = DB::table('product_with_brands')
        //     ->join('products as t1', 'product_with_brands.product_id', '=', 't1.id' )
        //     // ->join('products as t2','product_with_brands.product_id', '=', 't2.id')
        //     ->select('t1.product_name','t1.brand_name','t1.description','product_with_brands.*')
        //     ->where('product_with_brands.product_id', 'products.product_id')
        //     ->get()->all();
        //  dd($productWithBrands);

        // $getProductId = ProductWithBrand::all()->pluck('product_id');
        // $getBrandId = ProductWithBrand::all()->pluck('brand_id');
        // dd($getProductId, $getBrandId);
        // $getProductName = Product::where()
        return view('SellerDashboard.Products.viewProducts', compact('productWithBrands'));
    }
    */

    //Get products from Product table to use in Seller Add Products
    public function getProducts() {
        $products = Product::all();
        // dd($products);
        return view('SellerDashboard.Products.addProducts', compact('products'));
    }
    //Get brands from Brand table to use in Seller Add Products
    public function getBrands() {

    }

    public function create()
    {
        $products = Product::all();
        $brands = Brand::all();
        // dd($products, $brands);
        return view('SellerDashboard.Products.addProducts', compact('products', 'brands'));
    }

    /*==============================================================
        Function to get all the counters in the seller dashboard
    ================================================================*/
    public function getCountersOnDashboard() {
        //Get Number of orders in seller Dashboard
        $isProfileUpdated = false;
        $userId = auth()->user()->id;
        $seller = Seller::where('user_id', $userId)->first();
        
        if($seller !== null) {
            $orders = Order::where('pin', $seller->pin)->where('order_status', 'Assigned')->get();
            $orderCounter = count($orders);
            $isProfileUpdated = true;
        } else {
            $orderCounter = 0;
        }


        //Get Number of Active Products in Admin Dashboard
        $products = Product::where('status', true)->get();
        $productCounter = count($products);

        //Get Total Customers
        $users = User::where('role', 'user')->get();
        $userCounter = count($users);

        //Get Total Delivered Orders
        $deliveredOrders = Order::where('seller_id', $userId)->where('order_status', 'Delivered')->get();
        $deliveredOrdersCounter = count($deliveredOrders);

        //Get total Earnings from Orders
        $totalOrderAmount = Order::where('seller_id', $userId)->get()->sum('amount'); 

        //Counting rejected orders
        $checkCanceller = false;
        if($seller == true) {
            $checkCanceller =  Order::where('pin', $seller->pin)
                                    ->where('order_status', 'Cancelled')
                                    ->first();
            
            if($checkCanceller == true && $userId !== $checkCanceller->seller_id) {
                $orders = Order::where('pin', $seller->pin)
                                ->where('order_status', '=', 'Cancelled')
                                ->get();
                $rejectedOrdersCounter = count($orders);
            } else {
                $orders = null;
                $rejectedOrdersCounter = 0;
            }
        } else {
            $orders = null;
            $rejectedOrdersCounter = 0;
            $checkCanceller = false;    
        }
        //End of rejected orders counting
        return view('SellerDashboard.index', compact('productCounter', 'orderCounter', 'userCounter', 'deliveredOrdersCounter', 'totalOrderAmount', 'rejectedOrdersCounter', 'seller'));
    }


    /*==============================================================
            Function to order status in seler dashboard 
    ================================================================*/
    public function getOrderStatus() {
        $userId = auth()->user()->id;
        $seller = Seller::where('user_id', $userId)->first();
        if($seller !== null) {
            $orders = Order::where('pin', $seller->pin)->where('order_status', 'Assigned')->get();
            return view('SellerDashboard/orders/orderStatus', compact('orders'));    
        } else {
            $msg = 'Update Your Profile First';
            dd($msg);
        }
    }

    /*==============================================================
            Function to get cancelled order
    ================================================================*/
    public function getCancelledOrder() 
    {
        $userId = auth()->user()->id;
        $seller = Seller::where('user_id', $userId)->first();
        if($seller !== null) {
            $cancelledOrders = Order::where('pin', $seller->pin)->where('order_status', 'Cancelled')->paginate(10);
            return view('SellerDashboard/orders/cancelledOrder', compact('cancelledOrders'));
        } else {
            $msg = 'Update Your Profile First';
            dd($msg);
        }
    }

    /*==============================================================
            Function to perform order cancellation by seller
    ================================================================*/
    public function changeOrderStatusToCancelled($id) 
    {
        $sellerId = auth()->user()->id;
        $order = Order::findOrFail($id);
        $order->order_status = '8';
        $order->seller_id = $sellerId;
        // $order->shipped = false;
        $order->save();
        return back();
    }

    /*======================================================================
        Function to get the orders rejected by other sellers on same pin
    ========================================================================*/
    public function rejectedOrders() {
        $userId = auth()->user()->id;
        $seller = Seller::where('user_id', $userId)->first();
        $checkCanceller =  Order::where('pin', $seller->pin)
                                ->where('order_status', 'Cancelled')
                                ->first();

        if($checkCanceller == true && $userId !== $checkCanceller->seller_id) {
            if($seller !== null) {
                $orders = Order::where('pin', $seller->pin)
                                ->where('order_status', '=', 'Cancelled')
                                ->get();
            } else {
                $orders = null;
            }
        } else {
            $orders = null;
        }
        return view('SellerDashboard/orders/RejectedOrder', compact('orders'));
    }

}
