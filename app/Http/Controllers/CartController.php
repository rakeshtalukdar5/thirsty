<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use App\User;
use Session;
use Auth;
use App\Order;
use App\OrderProduct;
use Mail;
use App\Mail\OrderPlaced;
use App\Customer;
// use Illuminate\Support\Facades\Validator;
// use Gloudemans\Shoppingcart\Facades\Cart;


class CartController extends Controller
{
   
    /*========================================================================
        Function to get the cart page with cart items
    ==========================================================================*/
    public function index()
    {
        if(auth()->user()) {
            $items = Cart::content();
            return view('Cart.shopping-cart', compact('items'));
        } else {
            return redirect()->route('user.signin');
        }

    }

    /*========================================================================
        Function to store order in database  
    ==========================================================================*/
    public function storeOrder(Request $request)
    {
        $order = $this->addToOrdersTable($request, null);
        Mail::send(new OrderPlaced($order));
        Cart::instance('default')->destroy();
        session()->forget('coupon');
        return view('Cart/orderplaced');
    }

    /*========================================================================
        Function to validate and store  the order details in databse
    ==========================================================================*/
    protected function addToOrdersTable(Request $request, $error) 
    {
        $this->validate($request, [
            'email' => 'email|required',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|digits:10', 
            'city' => 'required',
            'pin' => 'required|digits:6',
            'houseno' => 'required',
            'locality' => 'required',
            'street' => 'required',
        ]);


        $payment_mode = '';
        if($request->payment_mode === null) 
        {
            $payment_mode = 'COD';
        } else {
            $payment_mode = $request->payment_mode;
        }

        

        // $pin = $request->pin;
        // $mobileNo = $request->mobile_number;
        // $firstName = '';
        // $city = '';  
        // $validatedEmail = '';
        
        // if(filter_var($request->email, FILTER_VALIDATE_EMAIL) === false) {
        //     dd('Enter a valid Email');
        //     Session::flash('error', 'Enter a valid email id');
        // } else {
        //     $validatedEmail = $request->email;
        // }
        // if(strlen($request->first_name) >= 3 && strlen($request->city) >= 3) {
        //     $firstName = $request->first_name;
        //     $city = $request->city;  
        // } else  {
        //     dd('First name and City should be more than 3 characters');
        //     Session::flash("First name and City should be more than 3 characters");
        // }

        // if(is_numeric($pin) && is_numeric($mobileNo) &&  strlen($pin) === 6 && strlen($mobileNo) === 10) 
        // {
            $order = Order::create([
                'user_id'  => auth()->user() ? auth()->user()->id : null,
                'billing_email' => $request->email,
                'firstname' => $request->first_name ,
                'lastname' => $request->last_name,
                'mobile_number' => $request->mobile_number,
                'city' => $request->city,
                'pin' => $request->pin,
                'houseno' => $request->houseno,
                'locality' => $request->locality,
                'street' => $request->street,
                'schedule_date' => null, //$request->schedule_date,
                'schedule_time' => null,  //$request->schedule_time,
                'payment_mode' => $payment_mode,
                'quantity' => Cart::instance()->qty,
                'discount' => $this->getAmountIfHasCoupon()->get('discount'),
                'discount_code' => $this->getAmountIfHasCoupon()->get('code'),
                'tax' =>  $this->getAmountIfHasCoupon()->get('newTax'),
                'subtotal' => $this->getAmountIfHasCoupon()->get('newSubtotal'), 
                'total' =>  $this->getAmountIfHasCoupon()->get('newTotal'),
                'order_history' => 123,
                'order_status' => '1',
                'error' => $error,
            ]);


        foreach(Cart::content() as $item) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
            ]);
        }
        return $order;
    
    }


    public function store(Request $request)
    {
        // $product = $request->brand_name.' '.$request->product_name;
        // dd($request->price);
        $cartItem = Cart::add($request->id, $request->name, 1, $request->price)
                ->associate('App\Product');
       
        return back()->with('Success','Item Successfully Added To Your Cart');
    }

    /*========================================================================
        //Function to get the checkout page with all the prices
    ==========================================================================*/
    public function getCheckout() {
        $user = Auth::user();
        if($user == true) {
            $customer = Customer::where('user_id', $user->id)->first();
            if($customer !== null) {
                return view('Cart.checkout')->with([
                    'discount' => $this->getAmountIfHasCoupon()->get('discount'), 
                    'newSubtotal' => $this->getAmountIfHasCoupon()->get('newSubtotal'), 
                    'newTax' => $this->getAmountIfHasCoupon()->get('newTax'),
                    'newTotal' => $this->getAmountIfHasCoupon()->get('newTotal'),
                    'code' => $this->getAmountIfHasCoupon()->get('code'),
                    'user' => $user,
                    'customer' => $customer,
                    ]);
            } else {
                $customer = $user;
                return view('Cart.checkout')->with([
                    'discount' => $this->getAmountIfHasCoupon()->get('discount'), 
                    'newSubtotal' => $this->getAmountIfHasCoupon()->get('newSubtotal'), 
                    'newTax' => $this->getAmountIfHasCoupon()->get('newTax'),
                    'newTotal' => $this->getAmountIfHasCoupon()->get('newTotal'),
                    'code' => $this->getAmountIfHasCoupon()->get('code'),
                    'user' => $user,
                    'customer' => $customer,
                    ]);
            } 
        } else {
            return redirect()->route('user.signin');
        }

    } 

    /*========================================================================
        Function to calculate amount if coupon applied
    ==========================================================================*/    
    private function getAmountIfHasCoupon() {
        $tax = config('cart.tax') / 100;
        $discount = session()->get('coupon')['discount'] ?? 0;
        $code = session()->get('coupon')['name'] ?? null;
        // var_dump($code);
        $newSubtotal = (Cart::subtotal() - $discount);
        $newTax = $newSubtotal * $tax;
        $newTotal = $newSubtotal + $newTax;
        return collect ([
            'tax' => $tax, 
            'discount' => $discount,
            'code' => $code,
            'newSubtotal' => $newSubtotal,
            'newTax' => $newTax,
            'newTotal' => $newTotal,
        ]);
    }

    /*========================================================================
        Function to remove item form  the cart
    ==========================================================================*/
    public function destroy($id)
    {
        Cart::remove($id);
        return back()->with('Success', 'Item Successsfully removed');
    }

    /*========================================================================
        // Function to move items to save later section form the cart
    ==========================================================================*/
    public function saveForLater($id) {
        $item = Cart::get($id); 
        Cart::remove($item->rowId);
        $duplicates = Cart::instance('saveForLater')->search(function($cartItem, $rowId) use($id){
            return $rowId === $id;
        });
        if($duplicates->isNotEmpty()) {
            return back()->with('error','Item Already Saved For Later');        
        } else {
            Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price)
            ->associate('App\Product');
            return back()->with('Success','Item Successfully Saved For Later');
        }
    }

    /*=================================================================================
        //Function to update (increase by one) the item quantity in the cart section
    ===================================================================================*/
     
    public function increaseQtyByOne($id, $qty) {
        Cart::update($id, ++$qty);
        $items = Cart::content();
        return view('Cart.shopping-cart', compact('items'));
    }

    /*=================================================================================
        //Function to update (decrease by one) the item quantity in the cart section 
    ===================================================================================*/
    public function decreaseQtyByOne($id, $qty) {
        if($qty > 1) {
            Cart::update($id, --$qty);
            $items = Cart::content(); 
        } else {
            Cart::update($id, 1);
            $items = Cart::content();
        }
        return view('Cart.shopping-cart', compact('items'));
    }

    /*=================================================================================
        //Function to remove item from the save later section 
    ===================================================================================*/
    public function removeFromSaveLater($id) {
        Cart::instance('saveForLater')->remove($id);
        return back()->with('Success','Item Successfully removed from Save Later');
    }

    /*=========================================================================
        //Function to move an item from save later section to cart again
    ============================================================================*/
    public function moveToCartFromSaveLater($id) {
        $item = Cart::instance('saveForLater')->get($id); 
        Cart::instance('saveForLater')->remove($item->rowId);

        //Checking the duplicate item if already exists in the save later or not
        $duplicates = Cart::instance('default')->search(function($cartItem, $rowId) use($id){
            return $rowId === $id;
        });
        
        if($duplicates->isNotEmpty()) {
            return back()->with('error','Item already in your cart!!');        
        } else {
            Cart::instance('default')->add($item->id, $item->name, 1, $item->price)
            ->associate('App\Product');
            return back()->with('Success','Item Successfully moved to your cart');
        }
    }
}
