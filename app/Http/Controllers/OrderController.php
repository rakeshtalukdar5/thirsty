<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\User;
use Session;
use Auth;
use Cart;
use App\Seller;
use DB;


class OrderController extends Controller
{
    /*========================================================================
        Function to get order details view with products
    ==========================================================================*/   
    public function orderProductView($id)
    {
        $order = Order::findOrFail($id); 
        $products = $order->products;
        return view('adminDashboard/orders/viewOrder',compact('products', 'order'));
    }

    /*========================================================================
        Function to get appear new order in admin dashboard
    ==========================================================================*/
    public function getOrdersOnAdmin() {
        $orders = Order::where('order_status', 'Pending')->paginate(10);
        return view('adminDashboard/orders/newOrder', compact('orders'));
    }

    /*========================================================================
        Function to assign order by admin to seller in admin dashboard
    ==========================================================================*/
    public function assignOrder($id) 
    {
        $getOrder = Order::findOrFail($id);
        $getSellers = Seller::where('pin', $getOrder->pin)->first();
        if(isset($getSellers))
        {
            if($getOrder->pin == $getSellers->pin && $getSellers->isVerified == true) 
            {
                $getOrder->order_status = '3';
                $getOrder->save();
                Session::flash('status', 'Order Has been assigned');
                return back();
            } else 
            {
                Session::flash('status', 'No Seller Found For This Location');
                return back();
            }
        } else {
            Session::flash('status', 'No Seller Found');
            return back();
        }
    }

    /*========================================================================
        Function to get assigned orders appear in seller dashboard
    ==========================================================================*/
    public function getAssignedOrders() 
    {
        $user = Auth::user();
        $seller = Seller::where('user_id', $user->id)->first();
        if($seller != null && $user->role == 'seller' && $seller->isVerified == true) 
        {
            $getOrders = Order::where('pin', $seller->pin)->where('order_status', 'Assigned')->get();
        } else 
        {
            $getOrders = null;
        }
        return view('SellerDashboard/orders/pendingOrder', compact('getOrders'));
    }

    /*========================================================================
        Function to change the order status to accept order from seller side
    ==========================================================================*/
    public function changeOrderStatusToAccepted($id) 
    {
        $user = Auth::user();
        $order = Order::findOrFail($id);
        $order->order_status = '4';
        $order->seller_id = $user->id;
        $order->save();
        return back();
    } 
    /*========================================================================
       Function to get the accepted order in seller
    ==========================================================================*/
    public function getAcceptedOrder() 
    {
        $user = Auth::user();
        $acceptedOrders = Order::where('order_status', 'Accepted')->where('seller_id', $user->id)->paginate(10);
        return view('SellerDashboard/orders/acceptedOrder', compact('acceptedOrders'));
    }
    /*========================================================================
      Function to change the order status to Shipped order from seller side
    ==========================================================================*/
    public function changeOrderStatusToShipped($id) 
    {
        $order = Order::findOrFail($id);
        $order->order_status = '5';
        $order->save();
        return back();
    } 
    /*========================================================================
       Function to get the Shipped order in seller
    ==========================================================================*/
     public function getShippedOrder() 
     {
        $user = Auth::user();
         $shippedOrders = Order::where('order_status', 'Shipped')->where('seller_id', $user->id)->paginate(10);
         return view('SellerDashboard/orders/shippedOrder', compact('shippedOrders'));
     }
    /*========================================================================
       Function to change the order status to Delivered order from seller side
    ==========================================================================*/
    public function changeOrderStatusToDelivered($id) 
    {
        $order = Order::findOrFail($id);
        $order->order_status = '6';
        // $order->shipped = true;
        $order->save();
        return back();
    } 
    /*========================================================================
       Function to get the Shipped order in seller
    ==========================================================================*/
     public function getDeliveredOrder() 
     {
        $user = Auth::user();
        $deliveredOrders = Order::where('order_status', 'Delivered')->where('seller_id', $user->id)->paginate(10);
        return view('SellerDashboard/orders/deliveredOrder', compact('deliveredOrders'));
     }
    

}
