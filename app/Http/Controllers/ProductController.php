<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cart;
use DB;
use Session;
use App\Order;
use Auth;
use App\User;
use App\Blog;

class ProductController extends Controller
{
   
    public function index()
    {
        // $products = Product::all();
        // return view('SellerDashboard.Products.viewProducts')->with('products', $products);
    }

    /*========================================================================
        Function to get all the products in admin dashboard
    ==========================================================================*/    
    public function getProductView() {
        $products = Product::paginate(10);
        return view('adminDashboard.Products.viewProducts')->with('products', $products);
    }
    
    /*========================================================================
        Function to get add product page in admin dashboard
    ==========================================================================*/
    public function addProducts() {
        return view('/adminDashboard/Products/addProducts');
    }
    
    /*========================================================================
        Function to store products in database
    ==========================================================================*/
    public function storeProducts(Request $request) {
        $this->validate($request, [
            'product_name' => 'required',
            'brand_name' => 'required',
            'type' => 'required',
            'quantity' => 'required',
            'volume' => 'required',
            'price' => 'required',
            'banner_image' => 'required',
            'description' => 'required',
        ]);
               

         //Upload the image
         if($request->hasFile('banner_image')) {
            // //Get filename with extension
            $fileNameWithExt = $request->file('banner_image')->getClientOriginalName();
            //Get the filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //Get the extension
            $extension = $request->file('banner_image')->getClientOriginalExtension(); 
            $allowedExt = ['jpg', 'jpeg', 'png'];
            if(in_array($extension, $allowedExt)) {
                $fileNameToStore =  request('brand_name').'-'.request('product_name').'-'.$filename.'-'.time().'.'.$extension;
                // Image Upload Path
                $path = $request->file('banner_image')->storeAs('public/banner_images', $fileNameToStore);
            } else {
                dd("Error in File Extension");
            }

          }


          $product = new Product;
          $product->product_name = request('product_name');
          $product->brand_name = request('brand_name');
          $product->type = request('type');
          $product->quantity = request('quantity');
          $product->price = request('price');
          $product->volume = request('volume');
          $product->status = false;
          $product->description = request('description');
          $product->slug = str_slug($request->brand_name.' '.$request->product_name);
          if($request->hasFile('banner_image')) {
            $product->banner_image = $fileNameToStore;    
          } else {
            $product->banner_image = 'noimage.jpg';
          }
        // $product->banner_image = request('banner_image');
        $product->save();
        return back();
    }

    /*========================================================================
        Function to display a single product detials in  admin product view
    ==========================================================================*/
    public function show($id)
    {
        dd('Need Modifications');
        $product = Product::findOrFail($id);
        // dd($product);
        return view('adminDashboard/Products/productDetailsView', compact('product'));
    }

    /*========================================================================
        Function to get the product edi page in admin dashboard
    ==========================================================================*/
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('adminDashboard/Products/productsUpdate')->with('product', $product);
    }

    /*========================================================================
        Function to perform update in product in admin dashboard
    ==========================================================================*/
    public function update(Request $request, $id)
    {        
         //Upload the image
         if($request->hasFile('banner_image')) {
            $fileNameWithExt = $request->file('banner_image')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('banner_image')->getClientOriginalExtension(); 
            $allowedExt = ['jpg', 'jpeg', 'png'];
            if(in_array($extension, $allowedExt)) {
                $fileNameToStore =  request('brand_name').'-'.request('product_name').'-'.$filename.'-'.time().'.'.$extension;
                $path = $request->file('banner_image')->storeAs('public/banner_images', $fileNameToStore);
            } else {
                dd("Error in File Extension");
            }

          }


        $product = Product::findOrFail($id);
        $product->product_name= request('product_name');
        $product->brand_name= request('brand_name');
        $product->description = request('description');
        if($request->hasFile('banner_image')) {
            $product->banner_image = $fileNameToStore;    
          } else {
            $product->banner_image = 'noimage.jpg';
          }
        $product->slug = str_slug(request('product_name'.'brand_name'));
        $product->save();

        // echo ('Data Inserted Successfully');
        return redirect('admin/Products');
    }

    /*========================================================================
        Function to delete a single product
    ==========================================================================*/
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return back()->with('Success', 'Product Successfully Deleted');
    }


    //New Code other than resources
    public function getProductsInForm() {
    
        $products = Product::all();
        // dd($products);
        return view('landing-page', compact('products'));
        // return view('form')->with($products,'products');
    }

    /*========================================================================
        Function to update the newly added product status
    ==========================================================================*/
    public function productStatusUpdate(Request $request, $id) {

        $product = Product::findOrfail($id);
        $productStatus = $product->status;
        $productStatus == false ? $product->status = true : $product->status = false;
        $product->save();
        return back();
    }

    /*========================================================================
        Function to get the product in shop page for users
    ==========================================================================*/
    public function getProductsOnShop() {
        $products = Product::where('status', true)->paginate(20);
        return view('UI.shop', compact('products'));
    }

    /*========================================================================
        Function to get product and blog in the landing page
    ==========================================================================*/
    public function getProductsOnIndex() {
        $products = Product::where('status', true)->inRandomOrder()->take(4)->get();
        $blogs = Blog::orderBy('created_at', 'desc')->take(3)->get();
        return view('UI.index', compact('products', 'blogs'));
    }
    
    /*========================================================================
        Function to get a single product for users or customers
    ==========================================================================*/    
    public function getSingleProductShow($slug) {
        $product = Product::where('slug', $slug)->firstOrFail();
        $productMightAlsoLike = Product::where('slug', '!=', $slug)->inRandomOrder()->take(4)->get();
        return view('UI.product-detail', compact('product', 'productMightAlsoLike'));
    }

    /*========================================================================
        Function to get or add produc on cart
    ==========================================================================*/
    public function getAddToCart(Request $request, $id) {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        return back();
    }

    /*========================================================================
       Function to get the products searched by the user or customer
    ==========================================================================*/
    public function searchProduct(Request $request) {
        $search = $request->search;
        $resultCount = '';
        if(!empty($search)) {
            $searchProducts = Product::where('brand_name', 'LIKE', "%$search%")
                                ->orWhere('product_name', 'LIKE', "%$search%")
                                ->orWhere('type', 'LIKE', "%$search%")
                                ->orWhere('description', 'LIKE', "%$search%")
                                ->orWhere('volume', 'LIKE', "%$search%")
                                ->get();
            $resultCount = count($searchProducts);
            return view('UI.search', compact('resultCount', 'searchProducts'));
        } else {
            $resultCount = 0;
            return view('UI.search', compact('resultCount'));
        }

    }
}
