<?php

namespace App\Http\Middleware;

// use Illuminate\Auth\Middleware\SellerAuthenticate;

use Closure;

class SellerAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() != true) 
        {
            return redirect()->route('seller.login');
        }
        return $next($request);
    }
}
