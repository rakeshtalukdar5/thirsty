<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class forgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    // public $ur;
    public $validator;
    public $selector;


    public function __construct(User $user, $selector, $validator)
    {
        $this->user = $user;
        $this->validator = $validator;
        $this->selector = $selector;
        // dd($user, $url);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $selector = $this->selector;
        $validator = $this->validator;
        return $this->to($this->user->email, null)
                    ->subject('Password Reset Link')
                    // ->message("<a href='$this->url'>".$this->url."</a>")
                    ->view('emails/forgotPassword', compact('selector', 'validator'));
    }
}