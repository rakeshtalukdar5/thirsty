<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\Cart;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->order->billing_email, $this->order->firstname)
                    // ->cc('abc@abc.com', 'Abc')
                    // ->bcc('xyz@xyz.com', 'Xyz')
                    ->subject('Successful Order') 
                    ->view('emails/orders/placedOrder');
    }
}
