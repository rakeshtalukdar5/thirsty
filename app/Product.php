<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['product_name', 'brand_name', 'type', 'quantity', 'price', 'description', 'volume','status', 'banner_image', 'slug'];
}
