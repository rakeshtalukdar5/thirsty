<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public static function getByCode($code){
        return self::where('code', $code);
    }

    public function getByPercentOff($total)
     {
        if( $this->type == 'fixed') {
            return $this->value;
        } 
        elseif ($this->type == 'percent') {
            return ($this->percent_off / 100) * $total;
        } 
        else {
            return 0;
        }
        
    }

}
