<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sellers';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
