<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id','schedule_date','schedule_time','payment_mode','order_status','shipped', 'subtotal','total', 'tax', 'discount',
    'discount_code','quantity', 'product_id', 'order_history','firstname', 'lastname', 'billing_email', 'mobile_number', 'pin', 'city', 
    'houseno', 'locality', 'street', 'error',];
   
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function products() {
        return $this->belongsToMany('App\Product')->withPivot('quantity');
    }

}
