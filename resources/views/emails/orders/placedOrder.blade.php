{{-- @extends('UI.layouts.header')
@section('content')

<h2>The email is sent</h2>

@include('UI.layouts.footer')
@endsection --}}

<h2> <b> Thank Your Order Placed Successfully </b></h2>
<p >
    Order Id: {{$order->id}} <br>
    Order Email: {{$order->billing_email}} <br>
    Customer Name: {{$order->firstname. ' '.$order->lastname}} <br><br>
    <span> <b>Items Ordered</b>
    @foreach($order->products as $product)
    Name: {{$product->brand_name.' '.$product->product_name}} <br>
    Price: {{$product->price}} <br>
    Quantity: {{$product->pivot->quantity}} <br><br>
    @endforeach 
    </span>
    Amount: {{$order->total}} <br>
    Order Placed At: {{$order->created_at}} <br>
</p>   