 {{-- @include('layouts.master') --}}
{{-- @section('content') --}}
{{-- <div class="order_now" id="order">

    <h5 style="text-align: center; margin-top: 10px; color: #03acff;text-transform:uppercase;">
      Book Your Water
    </h5>
    <form method="POST" class="submit-form" method="POST" action="/submit-request" onsubmit="submitRequest();" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}

<!doctype html>
  <head>
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        {{-- Date Picker --}}
        <script  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        {{-- Time Piker --}}
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
          </head>
<body>


    <div id="bookModal" class="myModal">
		  <div class="modal-content">
			<span class="closeBtn">&times;</span>
			
			<div class="row">
        		<div class="col-md-12">
          			<div class="form-group">
                    @foreach($products as $product)
          				<select id="product" class="form-control" name="product"  required >
				          <option value="">Select Products</option>
				          <option value="{{ $product->id}}">{{ $product->name}}</option>
		            	</select>
		          	</div>
		        </div>
		      </div>

        {{-- <div class="row"> --}}
          <div class="col-md-12">
            <div class="form-group">
            <select id="brand" class="form-control" name="brand" required>
              <option value="" selected>Select Brands</option>
              <option value="{{ $product->id}}">{{ $product->brand_name}}</option>
            </select>
            </div>
          </div>
        {{-- </div> --}}
      


      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">
          <select  id="quantity" class="form-control" name="quantity" >
            <option value="">Select Quantity</option>
            <option value="{{ $product->id}}">{{ $product->quantity}}</option>
          </select>
          </div>
        </div>
      {{-- </div> --}}
      
      <div class="col-md-12">
          <div class="form-group">
          <select  id="type" class="form-control" name="type" >
            <option value="">Select Type</option>
            <option value="{{ $product->id}}">{{ $product->type}}</option>
          </select>
          </div>
        </div>

      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">
            <p type="text" name="amount" class="form-control">Amount</p>
            <option value="{{ $product->id}}">{{ $product->price}}</option>
          </div>
        </div>
      @endforeach
      {{-- </div> --}}
    
      {{-- <div class="row"> --}}
          <div class="col-md-12">
            <div class="form-group"> <p>
              <input type="text" class="form-control" id="orderDate" name="order_date" placeholder="Choose Date" required />
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> </p>
            </div>
          </div>
        {{-- </div> --}}
   
      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">          
          <select id="orderSchedule" required class="form-control" name="order_date">
            <option value="">Select Schedule</option>
            <option value="time"></option>
            <option value="time"></option>
          </select>
          </div>
        </div>
      {{-- </div> --}}

      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">
            <input minlength="10" type="text" class="form-control" id="mobile" 
            name="mobile" placeholder="Enter 10 digit mobile number"
              maxlength="10" required pattern="^[0-9]{10}$">
          </div>
        </div>
      {{-- </div> --}}

      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
          </div>
        </div>
      {{-- </div> --}}
    
      {{-- <div class="row"> --}}
        <div class="col-md-12">
          <div class="form-group">
	          <input required type="text" placeholder="Enter Location" id="locationSearchField" class="form-control" name="location">
          </div>
        </div>
      {{-- </div> --}}

      <div class="button-group">
      	{{-- <button class="btn btn-primary btn-xs">Add to Cart</button> --}}
      	<button class="btn btn-success flex-fill" id="buyNowBtn">Buy Now</button>
      </div>

	
	</div>
  </div>{{-- <div id="datepicker" > Hello </div> --}}

  <style>
      body {
        font-family: Arial, Helvetica, sans-serif;
      }
      
      /* The Modal (background) */
      .myModal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
      }
      
      /* Modal Content */
      .modal-content {
        background-color: #FEF7F7;
        margin: auto;
        padding: 40px;
        border: 1px solid #888;
        width: 40%;
      }
      
      /* The Close Button */
      .closeBtn {
        color: red;
        float: right;
        font-size: 28px;
        font-weight: bold;
      }
      
      .closeBtn:hover,
      .closeBtn:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
      }
      #buyNowBtn {
        font-size: 22px;
        margin: auto;
        /* width: 30%; */
        display: block;
        /* justify-content: center; */
        padding: 16px 60px;
        font-family: 'open_sanssemibold';
        border-radius: 50px;
        box-shadow:2px 2px 5px 0 rgba(55,50,40, .20);
        -webkit-box-shadow:2px 2px 5px 0 rgba(55,50,40, .20);
        -moz-box-shadow:2px 2px 5px 0 rgba(55,50,40, .20);
        -o-box-shadow:2px 2px 5px 0 rgba(55,50,40, .20);
        -ms-box-shadow:2px 2px 5px 0 rgba(55,50,40, .20);
      }
      </style>

<script>

// $(document).ready(function() {

//     $('#orderDate').datepicker({
//       minDate: 0,
//       maxDate: '+3D'
    
//     });

//     $('#delivery').timepicker({

//       minTime: 0,
//     });

// });
</script>


<script>
    //     $('#product').change(function() { 
    //         var productID = $(this).val();     
    //         getBrandByProduct(productID);
    //         getServicesByProduct(productID);
    //     });
    
    
    //     function getBrandByProduct(productID) {
       
    //     if(productID) {
            
    //         $.ajax ({
    //             type:"POST",
    //             //url:"{{('getBrandByProduct')}}?product_id="+productID,
    //             url: '/getBrandByProduct/' + productID,
    //             data: { product_id: productID, _token: '{{csrf_token()}}' },
    //             dataType: "json",
    
    //             success:function(response) {               
    //                 if(response) {
    //                     $("#brand").empty();
    //                     $("#brand").append('<option>Select Brands</option>');
    //                     $.each(response,function(key,value){
    //                         //alert(key, value);
    //                         $("#brand").append('<option value="'+key+'">'+value+'</option>');
    //                     });
    //                 } else {
    //                     $("#brand").empty();
    //                 }
    //             }
    //         });
    //         } else {
    //             $("#brand").empty();
    //         }    
    //     }
    
    //     function getServicesByProduct(productID) {
    //         if(productID) {
    //             $.ajax ({
    //                 type:"POST",
    //                 //url:"{{('get-service-list')}}?product_id="+productID,
    //                 url: '/getServicesByProduct/' + productID,
    //                 data: { product_id: productID, _token: '{{csrf_token()}}' },
    //                 dataType: "json",
    
    //                 success:function(response){               
    //                     if(response){ 
    //                         $("#service").empty();
    //                         $("#service").append('<option>Select Services</option>');
    //                         $.each(response,function(key,value) {
    //                             $("#service").append('<option value="'+key+'">'+value+'</option>');
    //                         });
    //                     } else {
    //                         $("#service").empty();
    //                     }
    //                 }
    //             });
    //         } else {
    //             $("#service").empty();
    //         }
    //     }
    // });

      </script>
     {{-- @endsection() --}}
</body>
</html>

  