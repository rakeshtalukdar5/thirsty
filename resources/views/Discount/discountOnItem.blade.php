@extends('adminDashboard.layouts.dashboard')
@section('body')
<h2>Discount Management</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
    <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div id="DiscountsAdd" class="">

        <div id="addButtons" style="flex: right;">
            <a href="#addDiscount" class="btn btn-primary" id="addDiscountBtn" data-toggle="collapse" role="button">Add Discount</a>
        </div>

        {{-- Add Products Starts Here --}}
        <div class="tab form-group collapse" id="addDiscount">
            <h3>Add Discount</h3> 
            <form action="{{route('admin.setDiscount')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Code</label>
                    <input type="text" class="form-control" name="code" placeholder="Enter Code">
                </div>
                
                <div class="form-group">
                    <label>Type</label>
                    <select class="form-control" name="type">
                        <option>fixed</option>
                        <option id="percent">percent</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label>Value</label>
                    <input type="text" class="form-control" name="value" placeholder="Value">
                </div>
                
                <div class="form-group">
                    <label>Percent Off</label>
                    <input type="text" name="percent_off" class="form-control" placeholder="Percent Off">            
                </div>
                <button class="btn btn-primary" type="submit" name="submit">Save</button>
            </form>
        </div>  
</div>
<div id="DiscountsView" >
        <table class="table table-striped table-hover col-md-4">
                <thead>
                    <tr>
                        <th>SL. No</th>
                        <th>Code</th>
                        <th>Type</th>
                        <th>Value</th>
                        <th>Percent_Off</th>
                        <th>Status</th>
                        <th>Actions</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    {{-- @foreach($discounts as $discount)
                    <tr>
                        <td> {{$i++}} </td>
                        <td> {{$discount->code}} </td>
                        <td> {{$discount->type}}</td>
                        <td> {{$discount->value}} </td>
                        <td> {{$discount->percent_off}} </td>
                        @if($discount->status == false)
                            <td><form action="{{route('admin.discountStatus', $discount->id)}}" method="POST">@csrf <button class="btn btn-danger btn-sm">Expired</button></form></td>
                        @else
                            <td><form action="{{route('admin.discountStatus', $discount->id)}}" method="POST">@csrf <button class="btn btn-success btn-sm">Active</button></form></td>
                        @endif
                        <td><form action="{{route('admin.editProduct', ['id' => $discount->id])}}" method="POST"> @csrf <button class="btn btn-secondary btn-sm"  type="submit" name="edit"> Edit</button> </form></td>
                        <td><form action="{{route('admin.deleteDiscount', $discount->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Delete</button> </form></td>
                    </tr>
                    @endforeach
                </tbody> --}}
            </table>
</div>    
   
@endsection