@extends('UI.layouts.header')
@section('content')

 <section id="aa-myaccount">
  <div class="container">
      @if(Session::has('Success'))
      <div class="alert alert-success" style="font-size: 20px; text-align: center;">
        {{Session::get('Success')}}
      </div>
      @endif
      @if(session()->has('status'))
      <div class="alert alert-success">
        {{session()->get('status')}}
      </div>
      @endif

    @if(count($errors) > 0)
      <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
       <div class="aa-myaccount-area">         
           <div class="row">
             <div class="col-md-9">
               <div class="aa-myaccount-login">
               <h4>Login</h4>
               <form action="{{route('user.signin')}}" method="POST" class="aa-login-form">
                @csrf 
                 <label for="">Email address<span>*</span></label>
                  <input type="email" name="email"  placeholder=" Enter Email" autofocus required>
                  <label for="">Password<span>*</span></label>
                   <input type="password" placeholder="Password" name="password" required>
                   <button type="submit" class="aa-browse-btn">Login</button>
                   {{-- <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label> --}}
                    <p class="aa-lost-password"><a href="{{route('user.forgotPassword')}}">Lost your password?</a></p> <hr>
                 </form>
                 <p> Don't have an account?<a href="{{route('user.signup')}}">Register now!</a> </p>
              </div>
             </div>
             {{-- <div class="col-md-6">
               <div class="aa-myaccount-register">                 
                <h4>Register</h4>
                <form action="" class="aa-login-form">
                   <label for="">Username or Email address<span>*</span></label>
                   <input type="text" placeholder="Username or email">
                   <label for="">Password<span>*</span></label>
                   <input type="password" placeholder="Password">
                   <button type="submit" class="aa-browse-btn">Register</button>                    
                 </form>
               </div>
             </div> --}}
           </div>          
        </div>
      </div>
    </div>
  </div>
</section>
{{-- <div id="addButtons" style="flex: right;">
  <a href="#login-modal" class="btn btn-primary" id="addDiscountBtn" data-toggle="collapse" role="button">Add Discount</a>
</div>

</div>

 <!-- Login Modal -->  
 <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">                      
       <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4>Login or Register</h4>
         <form class="aa-login-form" action="">
           <label for="">Username or Email address<span>*</span></label>
           <input type="email" placeholder="Username or email">
           <label for="">Password<span>*</span></label>
           <input type="password" placeholder="Password">
           <button class="aa-browse-btn" type="submit">Login</button>
           <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
           <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
           <div class="aa-register-now">
             Don't have an account?<a href="account.html">Register now!</a>
           </div>
         </form>
       </div>                        
     </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
 </div> --}}




@include('UI.layouts.footer')
@endsection