@extends('UI.layouts.header')
@section('content')
<div class="container">
	<div class="row">        
       <div class="col-md-12">

             <div class="panel panel-primary">
                <div class="panel-heading" id="heading">  <h4 >My Profile</h4></div>
                    <div class="panel-body">
                        <div class="col-sm-9">
                            <table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>  Email: </b></td>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Password:  </b></td>
                                        <td>xxxxxxxx</td>
                                    </tr>
                                    @if($customer != null)
                                    <tr>
                                        <td><b>Name:  </b></td>
                                        <td>{{$customer->firstname.' '.$customer->lastname}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Mobile:  </b></td>
                                        <td>{{$customer->mobile_number}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Pin: </b></td>
                                        <td>{{$customer->pin}} </td>
                                    </tr>
                                    <tr>
                                        <td><b>City:  </b></td>
                                        <td>{{$customer->city}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>House No:  </b></td>
                                        <td>{{$customer->houseno}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Locality:  </b></td>
                                        <td>{{$customer->locality}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Street:  </b></td>
                                        <td>{{$customer->street}}</td>
                                    </tr>
                                    @else 
                                        <h5 class="alert alert-danger">Update Your Complete Profile</h5>
                                    @endif
                                    <tr> 
                                        <td> <span><a href=" {{route('user.editPassword', $user->id)}} " class="btn">Update Passsword</a></span></td>
                                        <td><span><a href="{{route('user.editProfile', $user->id)}}" class="btn">Update Profile</a></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    <!-- /.box-body -->
                    </div>
                <!-- /.box -->
            </div>
        </div> 
    </div>
</div>


<style>
    input.hidden {
        position: absolute;
        left: -9999px;
    }

    #profile-image1 {
        cursor: pointer;
    
        width: 100px;
        height: 100px;
        border:2px solid #03b1ce ;}
        .tital{ font-size:16px; font-weight:500;}
	    .bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0
     }	
     h4 {
         text-align: center;
         text-transform: uppercase;
     }
     .panel {
         margin-top: 5%;
         margin-bottom: 15%;
     }
     #heading {
         background-color: #ff6666;
     }
     .btn {
         margin-top: 10px;
         background-color: #ff6666;
         color: #fff;
         font-size: 15px;
     }
     td b {
        margin-right: 20px;
     }
     .btn:hover {
         background-color: #f8f8f8;
         color: #000;
     }
</style>



@include('UI.layouts.footer')
@endsection