@extends('UI.layouts.header')
@section('content')

 <section id="aa-myaccount">
  <div class="container">
    @if(Session::has('error'))
      <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
          {{Session::get('error')}}
      </div>
    @endif

    @if(count($errors) > 0)
      <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <div class="row">
      <div class="col-md-12">
       <div class="aa-myaccount-area">         
           <div class="row">
             <div class="col-md-9">
               <div class="aa-myaccount-register">                 
                <h4>Register</h4>
                <form action="{{route('user.signup')}}" name="myform" method="POST" class="aa-login-form" onsubmit="return validatepassword()">
                  @csrf 
                  <label for="">Email address<span>*</span></label>
                   <input type="email" name="email" id="email" placeholder=" Enter Email" required>
                   <label for="">Password*<span id="error"></span></label>
                    <input type="password" placeholder="Password" id="password" onkeyup="checkPasswordLength()" name="password" required>
                    <button type="submit" class="aa-browse-btn">Register</button>
                  </form>
                  <p> Already have an account?<a href="{{route('user.signin')}}">Login</a> </p>
               </div>
             </div>
           </div>          
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  function checkPasswordLength() { 
    error = "";
    let password = document.getElementById('password').value;
    if(password.length < 6) {  
      let error = " Password must be minimum 6 characters!!";
      document.getElementById('error').innerHTML = error;  
    } else {
      document.getElementById('error').innerHTML = error;  
    }
  }  

</script>  
 

@include('UI.layouts.footer')
@endsection


