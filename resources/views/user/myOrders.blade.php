@extends('UI.layouts.header')
@section('content')


<div class="container" id="userProfile">
    <div class="panel panel-primary">
        <div class="panel-heading">My Orders</div>
        <div class="row">
            @foreach($myOrders as $myOrder)
                <div class="col-md-8">
                    <div class="panel-body">
                        <ul class="list-group" >
                            <li>Order ID: {{$myOrder->id}}</li>
                            <li> Product: 
                                <a href="{{route('shop.product', $myOrder->slug)}}">
                                    <b>{{$myOrder->brand_name.' '.$myOrder->product_name}}</b> 
                                </a>
                            </li> 
                            <li>Volume: {{$myOrder->volume.' Ltr'}}</li>
                            <li> Price: {{$myOrder->price}}</li>
                            <li> Tax: {{$myOrder->tax}}</li>
                            <li> Total: {{$myOrder->total}}</li>
                            <li> Order Date: <b> {{$myOrder->created_at}} </b></li> 
                            <li>Status: <b> {{$myOrder->order_status}} </b></li>
                            @if($myOrder->order_status == 'CancelledByUser' || $myOrder->order_status == 'CancelledBySeller')
                                <li><a href="#" class="btn btn-danger btn-sm disabled">Cancelled</a></li>
                            @elseif($myOrder->order_status == 'Delivered' || $myOrder->order_status == 'Cancelled')
                            
                            @else
                                <li><a href="{{route('user.cancelOrder', $myOrder->order_id)}}" class="btn btn-danger btn-sm">Cancel</a></li>
                            @endif
                        </ul>
                        <hr>
                    </div>
                </div>
                <div class="col-md-4">
                    <br>
                    <a href="{{route('shop.product', $myOrder->slug)}}">
                        <img src="{{asset('/storage/banner_images/'.$myOrder->banner_image)}}" height="150px" width="190px" alt="{{$myOrder->brand_name.' '.$myOrder->product_name}}" />
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>


<style>
    #userProfile {
        margin-top: 10%;
        margin-bottom: 10%;
        color: black;
        /* border: 1px solid black;
        border-radius: 15px; */
        padding: 5px 5px;
        /* background-color: #DCDCDC; */
    }
    .panel-heading {
        text-align: center;
        font-size: 25px;
        text-transform: uppercase;
    }
</style>


@include('UI.layouts.footer')
@endsection