@extends('UI.layouts.header')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="updateProfile">
                <form action=" {{route('user.updateProfile', $user->id)}} " method="POST">
                    @csrf
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name='firstname' class="form-control" value="{{$user->firstname}}" >
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name='lastname' class="form-control" value="{{$user->lastname}}" >
                    </div>
                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="text" name='mobile_number' maxlength="10" minlength="10" class="form-control" value="{{$user->mobile_number}}">
                    </div>
                    <div class="form-group">
                        <label>city</label>
                        <input type="text" name='city' class="form-control" value="{{$user->city}}">
                    </div>
                    <div class="form-group">
                        <label>Pin</label>
                        <input type="text" name='pin' maxlength="6" minlength="6" class="form-control" value="{{$user->pin}}">
                    </div>
                    <div class="form-group">
                        <label>Locality</label>
                        <input type="text" name='locality' class="form-control" value="{{$user->locality}}">
                    </div>
                    <div class="form-group">
                        <label>Street</label>
                        <input type="text" name='street' class="form-control" value="{{$user->street}}">
                    </div>
                    <div class="form-group">
                        <label>House Number</label>
                        <input type="text" name='houseno' class="form-control" value="{{$user->houseno}}">
                    </div>
                    <div class="form-group">
                        <label>Gender</label>
                        <input type="text" name='gender' class="form-control" value="{{$user->gender}}" >
                    </div>
                    <button type="submit" class="btn">Update</button>                                                                                           
                </form>
            </div>  
        </div>
    </div>
</div>

<style>
.updateProfile {
    border: 3px solid #ff6666;
    padding: 20px;
    margin: 40px 10px 10px 10px;
    border-radius: 8px;
    margin-bottom: 15%;

}
.btn  {
    margin: auto;
    display: block;
    background-color: #ff6666;
    color: #fff;
}
</style>

@include('UI.layouts.footer')
@endsection