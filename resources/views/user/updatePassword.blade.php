@extends('UI.layouts.header')
@section('content')
<div class="container">
        @if(count($errors) > 0)
        <div class="alert alert-danger" style="font-size: 14px; text-align: center;">
          <ul>
            @foreach($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
      @endif
    <div class="row">
        <div class="col-md-9">
            <div class="updatePassword">
                <input type="hidden" id="userId" value={{$user->id}}>
                <form action=" {{route('user.updatePassword', $user->id)}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Current Passowrd</label>
                        <input type="text" name='currentPassword' class="form-control" minlength="6" id="currentPassword" placeholder="Enter Current Password">
                        <span id="checkPassword"> </span>
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" name='newPassword' class="form-control" minlength="6" id="newPassword" placeholder="Enter New Password">
                    </div>
                    <button type="submit" class="btn">Update</button>                                                                                           
                </form>
            </div>  
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script>
    $(document).ready(function(){
        $("#newPassword").click(function(){
            let currentPassword = $("#currentPassword").val();
            let userId = $("#userId").val();
            // alert(userId);
            // alert(currentPassword);
            $.ajax ({
                type: 'POST',
                url: '/user/check-password/' + userId,
                data: { currentPassword : currentPassword, _token: '{{csrf_token()}}'},
                success:function(response) {
                    if(response == false) {
                        $("#checkPassword").html("<font color = 'red'> Current Password is incorrect")
                    } else if(response == true) {
                        $("#checkPassword").html("<font color = 'green'> Current Password is correct")
                    }
                }, error:function(error) {
                    alert(error);
                }
            });
        });
    });
</script>


<style>
.updatePassword {
    border: 3px solid #ff6666;
    padding: 20px;
    margin: 40px 10px 10px 10px;
    border-radius: 8px;
    margin-bottom: 30%;

}
.btn  {
    margin: auto;
    display: block;
    background-color: #ff6666;
    color: #fff;
}
</style>


@include('UI.layouts.footer')
@endsection