@extends('UI.layouts.header')
@section('content')
<div class="container">
    @if(session()->has('status'))
        <div class="alert alert-success">
            {{session()->get('status')}}
        </div>
    @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
    @endif
    <div class="row">
        <div class="col-md-9">
            <div class="forgotPassword">
                <form action="{{route('user.forgotPassword')}}" method="POST">
                    @csrf
                    <div class="from-group">
                        <input type="email" class="form-control" name="forgot_password_email" placeholder="Enter your email">
                    </div>
                    <button type="submit" class="btn btn-success" >Send</button>
                </form>
            </div>
        </div>
    </div>
</div>
    
    <style>
    .forgotPassword {
        margin-top: 10%;
        margin-bottom: 20%;
        padding: 50px;
        border: 3px dashed tomato;
        background-color: #000;
        color: #fff;
        border-radius: 4px;
    }
    .btn {
        margin: auto;
        display: block;
        top: 5px;
        margin-top: 5px;
    }
   
    </style>





@include('UI.layouts.footer')
@endsection