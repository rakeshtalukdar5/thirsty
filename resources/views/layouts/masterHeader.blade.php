<!doctype html>

<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Thirsty - Online Drinking Water Order and Delivery Service</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Thirsty, Water, Online Drinking Water, Order Water, Delivery">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
      <!--For Plugins external css-->
        {{-- <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/lora-web-font.css" />
        <link rel="stylesheet" href="assets/css/opensans-web-font.css" />
        <link rel="stylesheet" href="assets/css/magnific-popup.css"> --}}

        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--Theme custom css -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />
        <script src="{{asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    </head>
    
    <body data-spy="scroll" data-target="#main_navbar">
		{{-- <div class='preloader'><div class='loaded'>&nbsp;</div></div> --}}
        <nav class="navbar navbar-default navbar-fixed-top" id="main_navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><b>ThirstY</b></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/">Home</a></li>
                        <li><a href="/shop">Shop</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#contact">Contact Us</a></li>
                        {{-- <li><a href="{{route('register')}}">Register</a></li>
                        <li><a href="{{route('login')}}">Login</a></li> --}}
                        <li><a href="{{route('shoppingCart')}}" ><i class="fa fa-shopping-cart" style="font-size:17px" aria-hidden="true">Cart</i>
                            <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span>
                        </a>
                    </li>                        <li class="dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user" aria-hidden="true"></i> User Management <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @if(Auth::check())
                                <li> <a class="dropdown-item" href="#">My Orders</a></li>
                                <li role="separator" class="divider"></li>
                                <li> <a class="dropdown-item" href="/user/profile">Edit Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li> <a class="dropdown-item" href="/user/logout">Logout</a></li>
                              @else
                                <li> <a class="dropdown-item" href="/user/signup">Signup</a></li>
                                <li role="separator" class="divider"></li>
                                <li> <a class="dropdown-item" href="/user/signin">Signin</a></li>
                              @endif
                          </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        
        @yield('content')    
         
        
        <!-- footer -->  
        <footer id="aa-footer">
            <!-- footer bottom -->
            <div class="aa-footer-top">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                <div class="aa-footer-top-area">
                    <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="aa-footer-widget">
                        <h3>Main Menu</h3>
                        <ul class="aa-footer-nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Our Products</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="aa-footer-widget">
                        <div class="aa-footer-widget">
                            <h3>Be a Partner</h3>
                            <ul class="aa-footer-nav">
                            <li><a href="#">Be a Seller</a></li>
                            <li><a href="#">Advertise Products</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="aa-footer-widget">
                        <div class="aa-footer-widget">
                            <h3>Useful Links</h3>
                            <ul class="aa-footer-nav">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Products</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Suppliers</a></li>
                            <li><a href="#">FAQ</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="aa-footer-widget">
                        <div class="aa-footer-widget">
                            <h3>Contact Us</h3>
                            <address>
                            <p> XYZ, Ghy 12345, INDIA</p>
                            <p><span class="fa fa-phone"></span>+91 1234567890</p>
                            <p><span class="fa fa-envelope"></span>qwerty@gmail.com</p>
                            </address>
                            <div class="aa-footer-social">
                            <a href="#"><span class="fa fa-facebook"></span></a>
                            <a href="#"><span class="fa fa-twitter"></span></a>
                            <a href="#"><span class="fa fa-google-plus"></span></a>
                            <a href="#"><span class="fa fa-youtube"></span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            </div>    
        </footer>
        <!-- / footer -->
       
            <style>
                    /*For Menu Bar*/
                    .navbar-default {
                        background-color:#00a5ff;
                        border-color: transparent;
                    }
                    .nav li a {
                        top: -15px;
                        margin-right: 15px;
        
                    }
                    .navbar {
                        height: 40px;
                        margin-bottom: 40px;
                        padding: 15px 0;
                    }
                    .navbar-brand {
                        position: absolute;
                        top: 0px;
                        left: 16px;
                        font-size: 18px;
                    }
                    .navbar-toggle {
                        top: -10px;
                    }
                    .navbar-default .navbar-nav>li>a {
                        color: #ffffff;
                        text-transform: uppercase;
                        font-family: 'open_sanssemibold';
                    }
                    .nav>li>a:hover, .nav>li>a:focus {
                        text-decoration: none;
                        background-color:transparent;
                    }
                    .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
                        background-color:transparent;
                    }
                    .navbar-default .navbar-nav>li>a:focus {
                        color: #fff;
                        background-color: transparent;
                    }
                    .menu-bg{
                        background:#00a5ff ;
                    }
        
                    #navbar-collapse-1 {
                        margin-top: -12px;
        
                    }
                </style>
        </div>

        <script src="{{asset('assets/js/orderModal.js')}}"></script>
        <script src="{{asset('assets/js/vendor/jquery-1.11.2.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
        {{-- <script src="{{asset('assets/js/vendor/isotope.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script> --}}
        <script src="{{asset('assets/js/main.js')}}"></script>
    </body>
</html>
