
@extends('UI.layouts.header')
@section('content')

<!-- Products section -->
<section id="aa-product">
    <div class="container">
        @if(Session::has('Success'))
        <div class="alert alert-success" style="font-size: 20px; text-align: center;">
            {{Session::get('Success')}}
        </div>
        @endif

        @if(count($errors) > 0)
          <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
            <ul>
              @foreach($errors->all() as $error)
                <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                        <!-- start prduct navigation -->
                            <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#all" data-toggle="tab">All</a></li>
                                <li><a href="#ozonied" data-toggle="tab">Ozonied</a></li>
                                <li><a href="#mineral" data-toggle="tab">Mineral</a></li>
                                <li><a href="#ro" data-toggle="tab">RO</a></li>
                                <li><a href="#bottle" data-toggle="tab">Bottle</a></li>
                                <li><a href="#can" data-toggle="tab">Can</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                    {{-- Start All Product Category --}}
                                <div class="tab-pane fade in active" id="all"> 
                                    @foreach($products as $product)
                                        <div class="col-md-3">
                                            <div class="block">
                                                <div class="top">
                                                    <ul>
                                                    {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                    {{-- <li style="color:red;"><span class="offer">Sale 40% Off</span></li> --}}
                                                    {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i></a></li> --}}
                                                    </ul>
                                                </div>
                                                
                                                <div class="middle">
                                                    <a href="{{route('shop.product', $product->slug)}}">
                                                        <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                                    </a>
                                                </div>
                                                
                                                <div class="bottom">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <h3 class="product-name">{{$product->brand_name}}</h3>
                                                        </a>
                                                    <h4 class="product-details">{{$product->volume.'Ltr '.$product->product_name}}</h4>
                                                    <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                                    <form action="{{route('cart.store')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$product->id}}">
                                                        <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                        <input type="hidden" name="price" value="{{$product->price}}">
                                                        <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                    </form> 
                                                    {{-- <a href="" class="btn btn-success" id="addToCartBtn"> <span class="fa fa-shopping-cart"></span>  Add To Cart</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                     @endforeach
                                </div>
        <!-- Start Ozonized  product category -->

                                <div class="tab-pane fade" id="ozonied">  
                                    @foreach($products as $product)
                                    @if($product->type == "Ozonized")
                                    <div class="col-md-3">
                                        <div class="block">
                                            <div class="top">
                                              <ul>
                                                {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i></a></li> --}}
                                              </ul>
                                            </div>
                                            
                                            <div class="middle">
                                                <a href="{{route('shop.product', $product->slug)}}">
                                              <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                            </a>
                                            </div>
                                            
                                            <div class="bottom">
                                                    <a href="{{route('shop.product', $product->slug)}}">
                                                        <h3 class="product-name">{{$product->brand_name}}</h3>
                                                    </a>
                                                <h4 class="product-details">{{$product->volume.' '.$product->product_name}}</h4>
                                                <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                                <form action="{{route('cart.store')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$product->id}}">
                                                    <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                    <input type="hidden" name="price" value="{{$product->price}}">
                                                    <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                </form> 
                                                {{-- <a href="" class="btn btn-success" id="addToCartBtn"> <span class="fa fa-shopping-cart"></span>  Add To Cart</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>

        <!-- Start Mineral  product category -->

                                <div class="tab-pane fade" id="mineral">
                                    @foreach($products as $product)
                                        @if($product->type == "Mineral")
                                            <div class="col-md-3">
                                                <div class="block">
                                                    <div class="top">
                                                        <ul>
                                                        {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                            <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                        {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                                        {{-- </a></li> --}}
                                                        </ul>
                                                    </div>
                                                    
                                                    <div class="middle">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                                        <a>
                                                    </div>
                                                    
                                                    <div class="bottom">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <h3 class="product-name">{{$product->brand_name}}</h3>
                                                        </a>                                                            
                                                        <h4 class="product-details">{{$product->volume.' '.$product->product_name}}</h4>
                                                        <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 9}}</span></h3>
                                                        <form action="{{route('cart.store')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$product->id}}">
                                                            <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                            <input type="hidden" name="price" value="{{$product->price}}">
                                                            <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                        </form>                                                         
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
        <!-- Start RO  product category -->

                                <div class="tab-pane fade" id="ro">
                                    @foreach($products as $product)
                                        @if($product->type == "RO")
                                            <div class="col-md-3">
                                                <div class="block">
                                                    <div class="top">
                                                        <ul>
                                                            {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                            <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                            {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                                        {{-- </a></li> --}}
                                                        </ul>
                                                    </div>        
                                                    <div class="middle">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                                        <a>                                                            
                                                    </div>       
                                                    <div class="bottom">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <h3 class="product-name">{{$product->brand_name}}</h3>
                                                        </a>
                                                        <h4 class="product-details">{{$product->volume.' '.$product->product_name}}</h4>
                                                        <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 15}}</span></h3>
                                                        <form action="{{route('cart.store')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$product->id}}">
                                                            <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                            <input type="hidden" name="price" value="{{$product->price}}">
                                                            <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                        </form>                                                             
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
    
  <!-- Start Bottle product category -->
                                <div class="tab-pane fade" id="bottle">                            
                                    @foreach($products as $product)
                                        @if($product->product_name == "Bottle")
                                            <div class="col-md-3">
                                                <div class="block">
                                                    <div class="top">
                                                        <ul>
                                                            {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                            <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                            {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                                            {{-- </a></li> --}}
                                                        </ul>
                                                    </div>
                                            
                                                    <div class="middle">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                                        <a>               
                                                    </div>
                
                                                    <div class="bottom">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <h3 class="product-name">{{$product->brand_name}}</h3>
                                                        </a>                    
                                                        <h4 class="product-details">{{$product->volume.' '.$product->product_name}}</h4>
                                                        <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                                        <form action="{{route('cart.store')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$product->id}}">
                                                            <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                            <input type="hidden" name="price" value="{{$product->price}}">
                                                            <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                        </form>                 
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
  <!-- Start Can product category -->
                                <div class="tab-pane fade" id="can">                           
                                    @foreach($products as $product)
                                       @if($product->product_name == "Can")
                                            <div class="col-md-3">
                                                <div class="block">
                                                    <div class="top">
                                                        <ul>
                                                            {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                            <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                            {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                                            {{-- </a></li> --}}
                                                        </ul>
                                                    </div>
                                                    <div class="middle">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />                    
                                                        <a>
                                                    </div>
                                                    <div class="bottom">
                                                        <a href="{{route('shop.product', $product->slug)}}">
                                                            <h3 class="product-name">{{$product->brand_name}}</h3>
                                                        </a>                    
                                                        <h4 class="product-details">{{$product->volume.' '.$product->product_name}}</h4>
                                                        <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                                        <form action="{{route('cart.store')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$product->id}}">
                                                            <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                            <input type="hidden" name="price" value="{{$product->price}}">
                                                            <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                                        </form>                 
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination" style="float: right;">{{$products->links()}}</div>
    </div>
</section>    
<!-- Product Section Ends -->

@include('UI.layouts.footer')
@endsection