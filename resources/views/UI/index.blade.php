@extends('UI.layouts.header')
@section('content')
<!-- Start slider -->
<section id="aa-slider">
    <div class="aa-slider-area">
      <div id="sequence" class="seq">
        <div class="seq-screen">
          <ul class="seq-canvas">
            <!-- single slide item -->
            <?php for($i = 0; $i <4; $i++) { ?> 
            <li>
              <div class="seq-model">
                <img data-seq src="{{asset('/assets/thirsty/img/slider/header.jpg')}}" alt="Men slide img" />
              </div>
              <div class="seq-title">
                <h2 data-seq>Best Can Collection</h2>                
                {{-- <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p> --}}
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">ORDER NOW</a>
              </div>
            </li>
            <?php } ?>
            <!-- single slide item -->         
          </ul>
        </div>
        <!-- slider navigation btn -->
        <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
          <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
          <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
        </fieldset>
      </div>
    </div>
  </section>
  <!-- / slider -->
<h2></h2>
  <!-- Start How To Order Sections -->
 <section id="service" class="service sections">
        <div class="container">
            <div class="heading text-center">
                <h1>How To Order</h1>
                <div class="separator"></div>
            </div>
            <!-- Example row of columns -->
            <div class="row">
                <div class="wrapper">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-item text-center">
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                            <h5>SELECT</h5>
                            <div class="separator2"></div>
                            <p>Select Your Favorite Brand and Choose The Container</p>
                        </div>
                    </div> 

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-item text-center">
                            <i class="fa fa-phone"></i>
                            <h5>ORDER</h5>
                            <div class="separator2"></div>
                            <p>Place the order without any difficulty</p>
                        </div>
                    </div> 

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-item text-center">
                            <i class="fa fa-truck"></i>
                            <h5>DELIVERED</h5>
                            <div class="separator2"></div>
                            <p>Water delivered at your door step based on your preferred timings</p>
                        </div>
                    </div> 

                  <!--  <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="service-item text-center">
                            <i class="fa fa-lock"></i>
                            <h5>Illustrations</h5>
                            <div class="separator2"></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry.</p>
                        </div>
                    </div> 
                    -->
                </div>
            </div>
        </div> <!-- /container -->       
    </section>
<!-- End How To Order Sections -->

 
  {{-- <!-- Start Promo section -->
  <section id="aa-promo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-promo-area">
            <div class="row">
              <!-- promo left -->
              <div class="col-md-5 no-padding">                
                <div class="aa-promo-left">
                  <div class="aa-promo-banner">                    
                    <img src="{{asset('/assets/thirsty/img/slider/kinley-bottle.jpg')}}" alt="img">                    
                    <div class="aa-prom-content">
                      <span>20% Off</span>
                      <h4><a href="#">On ALl Brands</a></h4>                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- promo right -->
              <div class="col-md-7 no-padding">
                <div class="aa-promo-right">
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="{{asset('/assets/thirsty/img/slider/kinley-bottle.jpg')}}" alt="img">                      
                      <div class="aa-prom-content">
                        <span>Exclusive Item</span>
                        <h4><a href="#">New Brands</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="{{asset('/assets/thirsty/img/slider/kinley-bottle.jpg')}}" alt="img">                      
                      <div class="aa-prom-content">
                        <span> &#8377 30 Off</span>
                        <h4><a href="#">On Kinely</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="{{asset('/assets/thirsty/img/slider/kinley-bottle.jpg')}}" alt="img">                      
                      <div class="aa-prom-content">
                        <span>New Arrivals</span>
                        <h4><a href="#">Mineral</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="{{asset('/assets/thirsty/img/slider/kinley-bottle.jpg')}}" alt="img">                      
                      <div class="aa-prom-content">
                        <span>25% Off</span>
                        <h4><a href="#">For New Customer</a></h4>                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>  --}}
  <!-- / Promo section -->
  
  <!-- Products section -->
<!-- Products section -->
<section id="aa-product">
    <div class="container">
        @if(Session::has('Success'))
          <div class="alert alert-success" style="font-size: 20px; text-align: center;">
            {{Session::get('Success')}}
          </div>
        @endif
        
        @if(count($errors) > 0)
          <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
            <ul>
              @foreach($errors->all() as $error)
                <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
          <div class="row">
              <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                        <!-- start prduct navigation -->
                            {{-- <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#ozonied" data-toggle="tab">Ozonied</a></li>
                                <li><a href="#mineral" data-toggle="tab">Mineral</a></li>
                                <li><a href="#ro" data-toggle="tab">RO</a></li>
                                <li><a href="#bottle" data-toggle="tab">Bottle</a></li>
                                <li><a href="#can" data-toggle="tab">Can</a></li>

                            </ul> --}}
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start product category -->
                                <div class="tab-pane fade in active">
                                    @foreach($products as $product)
                                      <div class="col-md-3">
                                          <div class="block">
                                            <div class="top">
                                              <ul>
                                                {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                                <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                                {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                          {{-- </a></li> --}}
                                              </ul>
                                            </div>  
                                            
                                            <div class="middle">
                                              <a href="{{route('shop.product', $product->slug)}}">
                                                <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="pic" />
                                              </a>
                                            </div>

                                            <div class="bottom">
                                              <h3 class="product-name"> 
                                                <a href="{{route('shop.product', $product->slug)}}">
                                                  {{$product->brand_name.' '.$product->product_name}}
                                                </a>
                                              </h3>
                                              <h4 class="product-details">{{$product->volume.' Ltr'}}</h4>
                                              <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                              <form action="{{route('cart.store')}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$product->id}}">
                                                <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                                <input type="hidden" name="price" value="{{$product->price}}">
                                                <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                              </form>                                             
                                            </div>
                                          </div>
                                      </div>
                                    @endforeach
                                    <a class="aa-browse-btn" href="/shop">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    <!-- Product Section Ends -->        
                          



<!-- Testimonial -->
<section id="aa-testimonial">  
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-testimonial-area">
            <h2>ABOUT US</h2>
           
                <div class="aa-testimonial-single">
                <img class="aa-testimonial-img" src="{{asset('assets/images/brand/header.jpg')}}" width="50%" height="250px" alt="testimonial img">
                <p style="color: #fff; margin-top: 2%;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Est nesciunt et deserunt perferendis exercitationem nisi, minima porro laboriosam, 
                  obcaecati ducimus similique? Molestias nisi laborum dolores itaque rerum quae error ipsa! Lorem ipsum dolor sit amet consectetur adipisicing elit.
                   Est nesciunt et deserunt perferendis exercitationem nisi, minima porro laboriosam, 
                  obcaecati ducimus similique? Molestias nisi laborum dolores itaque rerum quae error ipsa!
                </p>                  
                  </div>
                </div>
                </div>
              
          </div>
        </div>
  </section>
  <!-- / Testimonial -->

  <!-- Support section -->
  <section id="aa-support">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-support-area">
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-truck"></span>
                <h4>FREE SHIPPING</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-clock-o"></span>
                <h4>30 DAYS MONEY BACK</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-phone"></span>
                <h4>SUPPORT 24/7</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Support section -->
  
  <!-- Latest Blog -->
  <section id="aa-latest-blog">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-latest-blog-area">
            <h2>LATEST BLOG</h2>
            <div class="row">
              <!-- single latest blog -->
              @foreach($blogs as $blog)
                <div class="col-md-4 col-sm-4">
                  <div class="aa-latest-blog-single">
                    <figure class="aa-blog-img">                    
                      <a href="{{route('blog.singleBlog', $blog->id)}}"><img src="{{asset('/storage/blog_images/'.$blog->image)}}" width="100%" alt="img"></a>  
                                              
                    </figure>
                    <div class="aa-blog-info">
                      <h3 class="aa-blog-title"><a href=" {{route('blog.singleBlog', $blog->id)}} ">{{$blog->title}} </a></h3>
                      <p> {!!$blog->posts!!} </p> 
                      <a href="{{route('blog.singleBlog', $blog->id)}}" class="aa-read-mor-btn">Read more <span class="fa fa-long-arrow-right"></span></a>
                    </div>
                  </div>
                </div>
              @endforeach
              <!-- single latest blog -->
              {{-- <div class="col-md-4 col-sm-4">
                <div class="aa-latest-blog-single">
                  <figure class="aa-blog-img">                    
                    <a href="#"><img src="{{asset('/assets/thirsty/img/slider/ro-bottle.jpg')}}" alt="img"></a>  
                                            
                  </figure>
                  <div class="aa-blog-info">
                    <h3 class="aa-blog-title"><a href="#">Lorem ipsum dolor sit amet</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, ad? Autem quos natus nisi aperiam, beatae, fugiat odit vel impedit dicta enim repellendus animi. Expedita quas reprehenderit incidunt, voluptates corporis.</p> 
                     <a href="#" class="aa-read-mor-btn">Read more <span class="fa fa-long-arrow-right"></span></a>         
                  </div>
                </div>
              </div>
              <!-- single latest blog -->
              <div class="col-md-4 col-sm-4">
                <div class="aa-latest-blog-single">
                  <figure class="aa-blog-img">                    
                    <a href="#"><img src="{{asset('/assets/thirsty/img/slider/ro-bottle.jpg')}}" alt="img"></a>  
                                            
                  </figure>
                  <div class="aa-blog-info">
                    <h3 class="aa-blog-title"><a href="#">Lorem ipsum dolor sit amet</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, ad? Autem quos natus nisi aperiam, beatae, fugiat odit vel impedit dicta enim repellendus animi. Expedita quas reprehenderit incidunt, voluptates corporis.</p> 
                    <a href="#" class="aa-read-mor-btn">Read more <span class="fa fa-long-arrow-right"></span></a>
                  </div>
                </div>
              </div> --}}
            </div>
          </div>
        </div>    
      </div>
    </div>
  </section>
  <!-- / Latest Blog -->



@include('UI.layouts.footer')
@endsection