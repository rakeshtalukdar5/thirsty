@extends('UI.layouts.header')
@section('content')
<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{asset('/assets/thirsty/img/slider/header.jpg')}}" height="200px" width="100%" alt="fashion img">
    <div class="aa-catg-head-banner-area">
          <div class="container">
           <div class="aa-catg-head-banner-content">
             <h2>Blog Archive</h2>
           </div>
          </div>
        </div>
       </section>
       <!-- / catg header banner section -->
     
       <!-- Blog Archive -->
       <section id="aa-blog-archive">
         <div class="container">
           <div class="row">
             <div class="col-md-12">
               <div class="aa-blog-archive-area aa-blog-archive-2">
                 <div class="row">
                   <div class="col-md-9">
                     <div class="aa-blog-content">
                        @if(count($blogs) < 1)
                        <h2 class="alert alert-danger">No blogs Found</h2>
                        @else
                       <div class="row">
                        @foreach($blogs as $blog)
                         <div class="col-md-4 col-sm-4">
                           <article class="aa-latest-blog-single">
                             <figure class="aa-blog-img">                    
                               <a href="#"><img alt="img" src="{{asset('assets/images/brand').'/'.$blog->image}}"></a>  
                                 <figcaption class="aa-blog-img-caption">
                                 <span href="#"><i class="fa fa-clock-o"></i>Written On {{ $blog->created_at}}</span>
                               </figcaption>                          
                             </figure>
                             <div class="aa-blog-info">
                               <h3 class="aa-blog-title"><a href="{{route('blog.singleBlog', $blog->id)}}">{{$blog->title}}</a></h3>
                               <p> {!!$blog->posts!!} </p> 
                               <a class="aa-read-mor-btn" href="{{route('blog.singleBlog', $blog->id)}}">Read more <span class="fa fa-long-arrow-right"></span></a>
                             </div>
                           </article>
                         </div>     
                         @endforeach      
                       </div>
                      @endif
                     </div>
                     <!-- Blog Pagination -->
                   
                   </div>
                 </div>
                
               </div>
             </div>
           </div>
           <div class="pagination" style="float: right;">{{$blogs->links()}}</div>
         </div>
       </section>
       <!-- / Blog Archive -->
   

<style> 
.blogs {
    margin-top: 5%;
    margin-bottom: 25%;
}
</style>













     
@include('UI.layouts.footer')
@endsection