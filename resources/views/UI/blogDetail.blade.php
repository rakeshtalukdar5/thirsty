@extends('UI.layouts.header')
@section('content')
<div class="container">
    <a href="/blog" class="btn btn-default" style="margin-top: 10px;">Back</a>
    <div class="blogs">
        {{-- @if(count($blog) > 0) --}}
                <div class="well">
                        <a href="#" style="float: center;"><img src="{{asset('assets/images/brand').'/'.$blog->image}}" width="30%" height="30%" alt="img"></a>  
                    <h3> <a href="{{route('blog.singleBlog', $blog->id)}}"> {{$blog->title}} </a></h3>
                    <p> {!!$blog->posts!!} </p>
                    <small>Written On {{ $blog->created_at}}</small>
                </div>
        {{-- @else 
            <h2 class="alert alert-danger">No Blogs Found!!</h2>
        @endif --}}
    </div>
            
</div>

<style> 
.blogs {
    margin-top: 5%;
    margin-bottom: 25%;
}
</style>

@include('UI.layouts.footer')
@endsection