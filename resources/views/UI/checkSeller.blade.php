@extends('UI.layouts.header')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-dark table-striped">
                <thead>
                    @if($sellers != null)
                    <tr>
                        <th>Shop Name</th>
                        <th>Area</th>
                        <th>Pin</th>
                        <th>Street</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sellers as $seller)
                    <tr>
                        <td>{{$seller->shop_name}} </td>
                        <td>{{$seller->locality}}</td>
                        <td>{{$seller->pin}} </td>
                        <td>{{$seller->street}}</td>
                    </tr>
                    @endforeach
                    @else
                        <h2 class="alert alert-danger" >Sorry!!! No Seller Found</h5>
                    @endif
                </tbody>
            </table>
                
        </div>
        
    </div>
    <a href="{{ URL::previous() }}" class="btn btn-success btn-lg">Return Back</a>
</div>

@endsection