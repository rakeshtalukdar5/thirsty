@extends('UI.layouts.header')
@section('title')
{{-- Product Details     --}}
@endsection
@section('content')
 
 <!-- catg header banner section -->
 <section id="aa-catg-head-banner">
    <img src="{{asset('/assets/thirsty/img/slider/header.jpg')}}" height="200px" width="100%" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
       <div class="aa-catg-head-banner-content">
         <h2>{{$product->brand_name.' '.$product->product_name}}</h2>
         <ol class="breadcrumb">
           <li><a href="/">Home</a></li>         
           <li><a href="/shop">Shop</a></li>
           <li class="active"><?php echo ucfirst($product->slug); ?></li>
         </ol>
       </div>
      </div>
    </div>
   </section>
   <!-- / catg header banner section -->
 
   <!-- product category -->
   <section id="aa-product-details">
     <div class="container">
        @if(Session::has('Success'))
          <div class="alert alert-success" style="font-size: 20px; text-align: center;">
            {{Session::get('Success')}}
          </div>
          @endif

        @if(count($errors) > 0)
          <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
            <ul>
              @foreach($errors->all() as $error)
                <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif

          <div class="row">
          <div class="col-md-12">
       
           <div class="aa-product-details-area">
             <div class="aa-product-details-content">
               <div class="row">
                 <!-- Modal view slider -->
                 <div class="col-md-5 col-sm-5 col-xs-12">                              
                   <div class="aa-product-view-slider">                                
                     <div id="demo-1" class="simpleLens-gallery-container">
                       <div class="simpleLens-container">
                         <div class="simpleLens-big-image-container"><a class="simpleLens-lens-image">
                            <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" height="400px"  class="simpleLens-big-image"></a></div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <!-- Modal view content -->
                 <div class="col-md-7 col-sm-7 col-xs-12">
                   <div class="aa-product-view-content">
                     <h3>{{$product->brand_name.' '.$product->product_name}}</h3>
                     <div class="aa-price-block">
                       <span class="aa-product-view-price">&#8377 {{$product->price}}</span>
                       <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                     </div>
                     <p>{{$product->description}}</p>
                     <h4>{{$product->volume}}L</h4>
                     {{-- <div class="aa-prod-view-size">
                       <a href="#">1L</a>
                       <a href="#">500ml</a>
                       <a href="#">2L</a>
                       <a href="#">5L</a>
                     </div> --}}
                     {{-- <div class="aa-prod-quantity">
                       <form action="">
                         <select id="" name="">
                           <option selected="1" value="0">1</option>
                           <option value="1">2</option>
                           <option value="2">3</option>
                           <option value="3">4</option>
                           <option value="4">5</option>
                           <option value="5">6</option>
                         </select>
                       </form> --}}
                       {{-- <p class="aa-prod-category">
                         Category: <a href="#">Polo T-Shirt</a>
                       </p> --}}
                    </div>
                    <div class="aa-prod-view-bottom">
                        <form action="{{route('cart.store')}}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                            <input type="hidden" name="price" value="{{$product->price}}">
                            <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                        </form>    
                       {{-- <a class="aa-add-to-cart-btn" href="#">Wishlist</a>
                       <a class="aa-add-to-cart-btn" href="#">Compare</a> --}}
                        <br>
                        <form action="{{route('sellerLocationCheck')}}" method="POST">
                          @csrf
                          <input type="text" placeholder="Enter Area Pin" name="pin">
                          <button class="btn btn-warning" type="submit" style="background-color:tomato; color:#fff;">Check Seller</button>
                        </form>
                        {{-- @foreach($sellers as $seller)
                          <ul>
                            <li>$seller->shop_name</li>
                          </ul>
                        @endforeach --}}
                      </div>
                   </div>
                 </div>
               </div>
             </div>
             
             <div class="aa-product-details-bottom">
               <ul class="nav nav-tabs" id="myTab2">
                 <li><a href="#review" data-toggle="tab">Reviews</a></li>                
               </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade " id="review">
                 <div class="aa-product-review-area">
                   <h4>2 Reviews for T-Shirt</h4> 
                   <ul class="aa-review-nav">
                     <li>
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img class="media-object" src="img/testimonial-img-3.jpg" alt="girl image">
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading"><strong>Marla Jobs</strong> - <span>March 26, 2016</span></h4>
                            <div class="aa-product-rating">
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star-o"></span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img class="media-object" src="img/testimonial-img-3.jpg" alt="girl image">
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading"><strong>Marla Jobs</strong> - <span>March 26, 2016</span></h4>
                            <div class="aa-product-rating">
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star-o"></span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                          </div>
                        </div>
                      </li>
                   </ul>
                   <h4>Add a review</h4>
                   <div class="aa-your-rating">
                     <p>Your Rating</p>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                   </div>
                   <!-- review form -->
                   <form action="" class="aa-review-form">
                      <div class="form-group">
                        <label for="message">Your Review</label>
                        <textarea class="form-control" rows="3" id="message"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name">
                      </div>  
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="example@gmail.com">
                      </div>

                      <button type="submit" class="btn btn-default aa-review-submit">Submit</button>
                   </form>
                 </div>
                </div>            
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Related product -->
<section>
    <div class="aa-product-related-item">
        <h3 style="text-align: center;">Related Products</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row">                                                     
                    @foreach($productMightAlsoLike as $product)
                        <div class="col-md-3">
                            <div class="block">
                                <div class="top">
                                    <ul>
                                        {{-- <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li> --}}
                                        <li style="color:red;"><span class="offer">Sale 40% Off</span></li>
                                        {{-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> --}}
                                    {{-- </a></li> --}}
                                    </ul>
                                </div>
                            
                                <div class="middle">
                                    <a href="{{route('shop.product', $product->slug)}}">
                                        <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" alt="{{$product->brand_name.' '.$product->product_name}}" />
                                    </a>
                                </div>
                            
                                <div class="bottom">
                                    <a href="{{route('shop.product', $product->slug)}}">
                                        <h3 class="product-name">{{$product->brand_name}}</h3>
                                    </a>
                                    <h4 class="product-details">{{$product->volume.'Ltr '.$product->product_name}}</h4>
                                    <h3 class="price">&#8377 {{$product->price}} <span class="old-price">&#8377 {{$product->price + 10}}</span></h3>
                                    <form action="{{route('cart.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$product->id}}">
                                        <input type="hidden" name="name" value="{{$product->brand_name.' '.$product->product_name}}">
                                        <input type="hidden" name="price" value="{{$product->price}}">
                                        <button class="aa-add-to-cart-btn" type="submit" style="background-color: #000; color:#fff;">Add To Cart</button>
                                    </form>                                   
                                  </div>
                            </div>
                        </div>
                    @endforeach
                </div> 
            </div>
        </div>
</section>





       



@include('UI.layouts.footer')
@endsection