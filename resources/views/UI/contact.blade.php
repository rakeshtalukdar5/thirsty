@extends('UI.layouts.header')
@section('content')
<!-- start contact section -->
<section id="aa-contact">
  <div class="container">
    @if(Session::has('status'))
      <div class="alert alert-success"> {{Session::get('status')}} </div>
    @endif

    @if(count($errors) > 0)
      <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="aa-contact-area">
          <div class="aa-contact-top">
            <h2>We are wating to assist you..</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, quos.</p>
          </div>
          <!-- contact map -->
          <div class="aa-contact-map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.3714257064535!2d-86.7550931378034!3d34.66757706940219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8862656f8475892d%3A0xf3b1aee5313c9d4d!2sHuntsville%2C+AL+35813%2C+USA!5e0!3m2!1sen!2sbd!4v1445253385137" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <!-- Contact address -->
          <div class="aa-contact-address">
            <div class="row">
              <div class="col-md-8">
                <div class="aa-contact-address-left">
                  <form class="comments-form contact-form" action=" {{route('user.feedback')}}" method="POST">
                      @csrf
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" placeholder="Your Name" name="name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="email" placeholder="Email" name="email" class="form-control">
                        </div>
                      </div>
                    
                      <div class="col-md-12">
                        <div class="form-group">                        
                          <input type="text" placeholder="Subject" name="subject" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-12">
                      <div class="form-group">                        
                          <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                      </div>
                      </div>
                      <button type="submit" class="aa-secondary-btn">Send</button>
                    </div>                  
                  
                  </form>
                </div>
              </div>
              <div class="col-md-4">
                <div class="aa-contact-address-right">
                  <address>
                    <h4>Thirsty</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum modi dolor facilis! Nihil error, eius.</p>
                    <p><span class="fa fa-home"></span>Guwahati, Azara 781017, Assam</p>
                    <p><span class="fa fa-phone"></span>123456789</p>
                    <p><span class="fa fa-envelope"></span>Email: support@thirsty.com</p>
                  </address>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


@include('UI.layouts.footer')
@endsection