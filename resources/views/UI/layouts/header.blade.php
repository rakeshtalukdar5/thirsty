<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    @yield('title')
    
    <!-- Font awesome -->
    <link href="{{asset('/assets/thirsty/css/font-awesome.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('/assets/thirsty/css/bootstrap.css')}}" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="{{asset('/assets/thirsty/css/jquery.smartmenus.bootstrap.css')}}" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/thirsty/css/jquery.simpleLens.css')}}">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/thirsty/css/slick.css')}}">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/thirsty/css/nouislider.css')}}">
    <!-- Theme color -->
    <link id="switcher" href="{{asset('/assets/thirsty/css/theme-color/default-theme.css')}}" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="{{asset('/assets/thirsty/css/sequence-theme.modern-slide-in.css')}}" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="{{asset('/assets/thirsty/css/style.css')}}" rel="stylesheet">    

    <!-- Google Font -->
    {{-- <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> --}}
    {{-- <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'> --}}
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  

  </head>
  <body> 
    <!-- wpf loader Two -->
     <div id="wpf-loader-two">          
       <div class="wpf-loader-two-inner">
         <span>Loading</span>
       </div>
     </div> 
     <!-- / wpf loader Two -->       
   <!-- SCROLL TOP BUTTON -->
     <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
   <!-- END SCROLL TOP BUTTON -->
 
 
   <!-- Start header section -->
   <header id="aa-header">
     <!-- start header top  -->
     <div class="aa-header-top">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="aa-header-top-area">
               <!-- start header top left -->
     
             </div>
           </div>
         </div>
       </div>
     </div>
     <!-- / header top  -->
 
     <!-- start header bottom  -->
     <div class="aa-header-bottom">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="aa-header-bottom-area">
               <!-- logo  -->
               <div class="aa-logo">
                 <!-- Text based logo -->
                 <a href="/">
                   <span class="fa fa-tint" style="color: #00BFFF"></span>
                   <p ><strong >Thirsty</strong> <span> <b> One Tap Drinking Water Delivery </b></span></p>
                 </a>
                 <!-- img based logo -->
                 <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
               </div>
               <!-- / logo  -->
                <!-- cart box -->
             <!--   <div class="aa-cartbox">
                 <a class="aa-cart-link" href="#">
                   <span class="fa fa-shopping-basket"></span>
                   <span class="aa-cart-title">SHOPPING CART</span>
                   <span class="aa-cart-notify">2</span>
                 </a>
                 <div class="aa-cartbox-summary">
                   <ul>
                     <li>
                       <a class="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img"></a>
                       <div class="aa-cartbox-info">
                         <h4><a href="#">Product Name</a></h4>
                         <p>1 x $250</p>
                       </div>
                       <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                     </li>
                     <li>
                       <a class="aa-cartbox-img" href="#"><img src="img/woman-small-1.jpg" alt="img"></a>
                       <div class="aa-cartbox-info">
                         <h4><a href="#">Product Name</a></h4>
                         <p>1 x $250</p>
                       </div>
                       <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                     </li>                    
                     <li>
                       <span class="aa-cartbox-total-title">
                         Total
                       </span>
                       <span class="aa-cartbox-total-price">
                         $500
                       </span>
                     </li>
                   </ul>
                   <a class="aa-cartbox-checkout aa-primary-btn" href="checkout.html">Checkout</a>
                 </div>
               </div> -->
               <!-- / cart box -->
               <!-- search box -->

              <div class="aa-search-box">
                  <form action="{{route('searchProduct')}}" method="POST">
                    @csrf 
                    <input type="text" name="search" id="" placeholder="Search Here eg. 'Products', 'Brands', 'Product type' ">
                    <button type="submit"><span class="fa fa-search"></span></button>
                  </form>
                </div>
               <!-- / search box -->             
             </div>
           </div>
         </div>
       </div>
     </div>
     <!-- / header bottom  -->
   </header>
   <!-- / header section -->
   <!-- menu -->
   <section id="menu">
     <div class="container">
       <div class="menu-area">
         <!-- Navbar -->
         <div class="navbar navbar-default" role="navigation">
           <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>          
           </div>
           <div class="navbar-collapse collapse">
             <!-- Left nav -->
             <ul class="nav navbar-nav">
               <li><a href="/">Home</a></li>
               <li><a href="{{route('shop')}}">Shop</a> </li>
               <li><a href="/blog">Blog</a> </li>              
               <li><a href="/">About Us</a> </li>
                <li><a href="/contact">Contact Us</a> </li>
               <li><a href="#"> <i class="fa fa-user" aria-hidden="true"></i>My Account <span class="caret"></span></a>
                  <ul class="dropdown-menu">  
                  @if (Auth::check())
                  {{-- {{  Auth::user() }} --}}

                    @if (Auth::user()->role == 'user')
                      <li><a href="{{route('user.profile')}}">My Profile</a></li>                                                  
                      <li><a href=" {{route('user.myOrder')}} ">My Orders</a></li>              
                      <li><a href="{{route('user.logout')}}">Log Out</a></li>
                    @else
                      <li><a href="{{route('user.signup')}}">Register</a></li>
                      <li><a href="{{route('user.signin')}}">Log In</a></li>
                    @endif 
                  @else
                    <li><a href="{{route('user.signup')}}">Register</a></li>
                    <li><a href="{{route('user.signin')}}">Log In</a></li>
                  @endif 

                 </ul>
               </li>
               
               <li>
                 <a href="{{route('cart.index')}}" >
                    <i class="fa fa-shopping-cart" style="font-size:17px" aria-hidden="true">Cart</i>
                    @if(Cart::instance('default')->count() > 0)
                      <span class="badge">{{Cart::instance('default')->count()}}</span>
                    @endif
                 </a>
               </li>
             </ul>
           </div><!--/.nav-collapse -->
         </div>
       </div>       
     </div>
   </section>

   @yield('content')




   

   <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="{{asset('/assets/thirsty/js/bootstrap.js')}}"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="{{asset('/assets/thirsty/js/jquery.smartmenus.js')}}"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="{{asset('/assets/thirsty/js/jquery.smartmenus.bootstrap.js')}}"></script>  
  <!-- To Slider JS -->
  <script src="{{asset('/assets/thirsty/js/sequence.js')}}"></script>
  <script src="{{asset('/assets/thirsty/js/sequence-theme.modern-slide-in.js')}}"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="{{asset('/assets/thirsty/js/jquery.simpleGallery.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/thirsty/js/jquery.simpleLens.js')}}"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="{{asset('/assets/thirsty/js/slick.js')}}"></script>
  <!-- Price picker slider -->
  {{-- <script type="text/javascript" src="{{asset('/assets/thirsty/js/nouislider.js')}}"></script> --}}
  <!-- Custom js -->
  <script src="{{asset('/assets/thirsty/js/custom.js')}}"></script> 

  </body>
</html>
