@extends('layouts.layout')
@section('body')
<div class="container">
    <h2>Booking Form</h2>
    <form action="/customer/order" method="POST">
      @csrf
      <div class="form-group">
        <label>Product</label>
        <input type="text" class="form-control" id="email" placeholder="Product Name" name="product_name">
      </div>
      <div class="form-group">
        <label>Quantity</label>
        <input type="text" class="form-control" id="pwd" placeholder="Quantity" name="quantity">
      </div>
      <div class="form-group">
          <label>Pin</label>
          <input type="text" name="pin" id="pin" placeholder="Enter Pin" class="form-control">
          <div id="ajaxCall"></div>
          @csrf
      </div>
      <button type="submit" class="btn btn-success" style="margin: auto; display: block;">Submit</button>
    </form> 
  </div>
  <script>
  $(document).ready(function(){
    // alert('Testing');
    $('#pin').keyup(function(){ 
        let query = $(this).val();
        if(query != '') {
            let _token = $('input[name="_token"]').val();
            // alert(_token);
            $.ajax({
                url:"{{ route('test.getData') }}",
                method:"POST",
                data: { query:query, _token:_token },
                success:function(data)
                {
                    $('#ajaxCall').fadeIn();  
                    $('#ajaxCall').html(data);
                }
            });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#pin').val($(this).text());  
        $('#ajaxCall').fadeOut();  
    });  
});

  </script>
  

@endsection