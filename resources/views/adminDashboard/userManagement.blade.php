@extends('adminDashboard.layouts.dashboard')
{{-- @include('layouts.layout') --}}

@section('body')
   <h2>User Management</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
        <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

    {{-- Add Brands Ends Here --}}
   <table class="table table-striped table-hover col-md-4">
        <thead>
            <tr>
                <th>SL. No</th>
                <th>User Id</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>City</th>
                <th>Locality</th>
                <th>Pin</th>
                <th>Street</th>
                <th>House No</th>
                <th>Gender</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($users as $user)
            <tr>
                <td> {{$i++}} </td>
                <td> {{$user->user_id}}</td>
                <td> {{$user->firstname.' '.$user->lastname}} </td>
                <td> {{$user->email}} </td>
                {{-- <td> {{$user->status}} </td> --}}
                <td> {{$user->mobile_number}} </td>
                <td> {{$user->city}} </td>
                <td> {{$user->locality}} </td>
                <td> {{$user->pin}} </td>
                <td> {{$user->street}} </td>
                <td> {{$user->houseno}} </td>
                <td> {{$user->gender}} </td>
                @if($user->status == false)
                    <td><form action="{{route('admin.blockUser', ['id' => $user->user_id])}}" method="POST">@csrf <button class="btn btn-danger btn-sm">Block</button></form></td>
                @else
                    <td><form action="{{route('admin.blockUser', ['id' => $user->user_id])}}" method="POST">@csrf <button class="btn btn-success btn-sm">Active</button></form></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination" style="float: right;">{{$users->links()}}</div>
</div>

@endsection