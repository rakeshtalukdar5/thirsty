<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>A</b>LT</span> --
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Thirsty</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
        
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
                <a href="{{ route('admin.logout') }}">
                    {{-- onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"> --}}
                    Logout
                </a>
{{-- 
                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form> --}}
        </li>
  </ul>
  </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
   
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="http://simple4marry.com/assets/extra-images/male.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
            <a href="/admin">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                <span class="pull-right-container">
                  <i class="pull-right"></i>
                </span>
              </a>
        </li>
        {{-- <li class="">
        <a href="/admin/NewOrder">
            <i class="fa fa-dashboard"></i> <span>New Order</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li> --}}
        <li class="">
          {{-- <a href="{{route('enquiry',['status'=> App\Enquiry::PENDING_ENQ])}}"> --}}
          <a href="/admin/NewOrder">
            <i class="fa fa-shopping-cart"></i> <span>Pending Order</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li>
        <li class="">
        {{-- <a href="{{route('enquiry',['status'=> App\Enquiry::COMPLETED_ENQ])}}"> --}}
            <a href="{{route('admin.approvedOrder')}}">
            <i class="fa fa-shopping-cart"></i> <span>Approved Order</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li> 

        <li class="">
          {{-- <a href="{{route('enquiry',['status'=> App\Enquiry::COMPLETED_ENQ])}}"> --}}
          <a href="{{route('admin.Order-status')}}">
            <i class="fa fa-shopping-cart"></i> <span>Order Status</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li> 

        <li class="">
            {{-- <a href="{{route('enquiry',['status'=> App\Enquiry::COMPLETED_ENQ])}}"> --}}
                <a href="{{route('admin.deliveredOrder')}}">
                <i class="fa fa-shopping-cart"></i> <span>Delivered Order</span>
                <span class="pull-right-container">
                  <i class="pull-right"></i>
                </span>
              </a>
            </li> 
        <li class="">
        {{-- <a href="{{route('enquiry',['status'=> App\Enquiry::DELETED_ENQ])}}"> --}}
            <a href="{{route('admin.cancelledOrder')}}">
            <i class="fa fa-shopping-cart"></i> <span>Cancelled Order</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-tags" aria-hidden="true"></i> Products <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" id="productsDropdown">
            <li> <a class="dropdown-item" href="{{route('admin.viewProducts')}}">View Products</a></li>
            <li role="separator" class="divider"></li>
            <li> <a class="dropdown-item" href="{{route('admin.addProducts')}}">Add Products</a></li>
          </ul>
          
          <style>
            #productsDropdown {
              background-color: #222d32;
              color: white;
              margin-left: 20%;
              margin-bottom: 50%;
            }
            #productsDropdown li a:hover {
              background-color: #ADFF2F;
            }
          
          </style>
        </li>
        <li class="">
          <a href="{{route('admin.sellerManagement')}}">
            <i class="fa fa-truck"></i> <span>Seller Management</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li>

        <li class="">
            <a href="{{route('admin.userManagement')}}">
              <i class="fa fa-user"></i> <span>User Management</span>
              <span class="pull-right-container">
                <i class="pull-right"></i>
              </span>
            </a>
        </li>

        <li class="dropdown">
            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Blogs <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" id="blogsDropdown">
              <li> <a class="dropdown-item" href="{{route('admin.blogsCreate')}}">Create Blogs</a></li>
              <li role="separator" class="divider"></li>
              <li> <a class="dropdown-item" href="{{route('admin.blogsView')}}">View Blogs</a></li> 
            </ul>
            
            <style>
              #blogsDropdown {
                background-color: #222d32;
                color: white;
                margin-left: 20%;
                margin-bottom: 50%;
              }
              #blogsDropdown li a:hover {
                background-color: #ADFF2F;
              }
            
            </style>
          </li>
        <li class="">
            {{-- <a href="{{route('getFeedback')}}"> --}}
                <a href="/admin/discount">
              <i class="fa fa-money"></i> <span>Discount</span>
              <span class="pull-right-container">
                <i class="pull-right"></i>
              </span>
            </a>
        </li>
        <li class="">
          {{-- <a href="{{route('getFeedback')}}"> --}}
              <a href="{{route('admin.feedback')}}">
            <i class="fa fa-comments"></i> <span>Feedback</span>
            <span class="pull-right-container">
              <i class="pull-right"></i>
            </span>
          </a>
        </li>
       
        <li class="">
            {{-- <a href="{{route('getGetInTouch')}}"> --}}
                <a href="#">
              <i class="fa fa-user"></i> <span>Profile</span>
              <span class="pull-right-container">
                <i class="pull-right"></i>
              </span>
            </a>
          </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
      <h1>
        {{-- {{ucfirst($label)}} --}}
      </h1>
      @if(Session::has('message'))
          <div id="hideMe"  class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</div>
        @endif
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
          
              @yield('body')
           
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  
  
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->





    <div id="info-form" class="modal" style="z-index:222;">
    <h3 class="text-center">Information</h3>
        <div class="text-left" id="modal-content"></div>
    </div>  
<script src="{{ asset('js/vendors/jquery/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script type="text/javascript">
function showData(data) {
html = [
' <div class="row"><div class="col-xs-4">Name:</div><div class="col-xs-7">'+data['name']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Mobile:</div><div class="col-xs-7">'+data['mobile']+'</div> </div>',

' <div class="row"><div class="col-xs-4">Service: </div><div class="col-xs-7">'+data['product_name']+' </div> </div>',
' <div class="row"><div class="col-xs-4">Service Type:</div> <div class="col-xs-7">'+data['service_name']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Brand:</div><div class="col-xs-7">'+data['brand']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Location:</div><div class="col-xs-7">'+data['location']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Status:</div><div class="col-xs-7">'+data['status']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Warranty:</div><div class="col-xs-7">'+data['warranty']+'</div> </div>',
' <div class="row"><div class="col-xs-4">Date:</div><div class="col-xs-7">'+data['created_at']+'</div> </div>'];
$("#modal-content").html(html.join(""));
$("#info-form").modal();
}




$(document).ready(() => {
    $(".action-button").click((e)=> {
        if (confirm("Are You Sure?")) {
            return true;
        } else {
            e.preventDefault();

        }
        setTimeout(() => {
            debugger;
            $("#hideMe").hide();
        }, 5000);


    })
})
</script>
<style>
    .modal a.close-modal {
    position: absolute;
    top: 1.5px;
    right: 2.5px;}

</style>

</body>
</html>
