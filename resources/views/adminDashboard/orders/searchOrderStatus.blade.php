@extends('adminDashboard.layouts.dashboard')
@section('body')

<div class="row header">
    <div class="col-md-11">
            <h2>Order Status</h2>
           {{-- <style> -- Styles only for the above h2 tag which is the heading --}}
                {{-- <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">    --}}
            {{-- </style> --}}
    </div>
    <div class="col-md-1">
        <!-- search form -->
        {{-- <form action="{{route('admin.Search-Order-Status')}}" method="POST" class="form-group">
            @csrf
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> --}}
        <a href="{{route('admin.Order-status')}}" class="btn btn-danger btn-md back-btn">Back</a>
        <!-- /.search form --> 
        <style>
        .header {
            text-align: center;
            text-transform: uppercase;
            font-weight: bold;
            letter-spacing: 2px;
            border: 1px dotted black;
            border-radius: 5px;
            padding: 0px;
            background-color: #0080ff;
            color: #fff;
        }
        .header h2 {
            font-weight: bold;

        }
        .back-btn {
            margin-top: 15px;
        }
        </style>
    </div>
</div>
@if($resultCount > 0)
    <h4 class="alert alert-success text-center">{{$resultCount}} results found </h4>
 
<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Order Status</th>
                {{-- <th>Shipped</th> --}}
                <th id="hh" style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            @foreach($searchOrders as $order)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user_id }}</td>
                    <td>{{ $order->total }}</td>
                    <td>{{ $order->pin}}</td>
                    <td>{{ $order->firstname.' '.$order->lastname }}</td>
                    <td> {{$order->mobile_number}} </td>
                    <td> {{$order->billing_email}} </td>
                    <td> {{$order->locality}} </td>
                    <td> {{$order->created_at}} </td>
                    @if($order->order_status === 'Cancelled' || $order->order_status === 'CancelledBySeller' || $order->order_status === 'CancelledByUser')
                        <td> <button class="btn btn-danger btn-sm"> {{$order->order_status}} </button> </td>
                    @else
                        <td> <button class="btn btn-primary btn-sm"> {{$order->order_status}} </button> </td>
                    @endif   
                    {{-- @if($order->shipped == false)
                        <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                    @else
                        <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                    @endif --}}
                    <td><form action="{{route('order.detail.view', $order->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                    {{-- <td><form action="{{route('admin.approveOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Approve</button> </form></td>
                    <td><form action="{{route('admin.cancelOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row"> --}}
                        
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
    <h2 class="alert alert-danger text-center">Oops!! No Results Found</h2>
@endif
        {{-- <div class="box-body">
            <div class="row">  {{$orders->links()}}</div>
            <div class="row"> Total Records: {{ $orders->total() }}</div>
        </div> --}}
    {{-- </div>
    <div class="pagination" style="float: right;">{{$orders->links()}}</div>
</div> --}}

@endsection