@extends('adminDashboard.layouts.dashboard')
@section('body')

<div class="row header">
    <div class="col-md-9">
            <h2>Order Status</h2>
           {{-- <style> -- Styles only for the above h2 tag which is the heading --}}
                {{-- <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">    --}}
            {{-- </style> --}}
    </div>
    <div class="col-md-3">
        <!-- search form -->
        <form action="{{route('admin.Search-Order-Status')}}" method="POST" class="form-group">
            @csrf
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit"  id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form --> 
        <style>
        .header {
            text-align: center;
            text-transform: uppercase;
            font-weight: bold;
            letter-spacing: 2px;
            border: 1px dotted black;
            border-radius: 5px;
            padding: 0px;
            background-color: #0080ff;
            color: #fff;
        }
        .header h2 {
            font-weight: bold;

        }
        .input-group {
            margin-top: 20px;
        }
        </style>
    </div>
</div>
 
<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Order Status</th>
                {{-- <th>Shipped</th> --}}
                <th id="hh" style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
             $i = 1;?>
            
            @foreach ($orders as $order)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $order->id }}</td>
                <td>{{ $order->user_id }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->pin}}</td>
                <td>{{ $order->firstname.' '.$order->lastname }}</td>
                <td> {{$order->mobile_number}} </td>
                <td> {{$order->billing_email}} </td>
                <td> {{$order->locality}} </td>
                <td> {{$order->created_at}} </td>
                @if($order->order_status === 'Cancelled' || $order->order_status === 'CancelledBySeller' || $order->order_status === 'CancelledByUser')
                    <td> <button class="btn btn-danger btn-sm disabled"> {{$order->order_status}} </button> </td>
                @else
                    <td> <button class="btn btn-primary btn-sm disabled"> {{$order->order_status}} </button> </td>
                @endif   
                {{-- @if($order->shipped == false)
                    <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                @else
                    <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                @endif --}}
                <td><form action="{{route('order.detail.view', $order->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                {{-- <td><form action="{{route('admin.approveOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Approve</button> </form></td>
                <td><form action="{{route('admin.cancelOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row"> --}}
                    
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$orders->links()}}</div>
            <div class="row"> Total Records: {{ $orders->total() }}</div>
        </div> --}}
    {{-- </div>
    <div class="pagination" style="float: right;">{{$orders->links()}}</div>
</div> --}}

@endsection