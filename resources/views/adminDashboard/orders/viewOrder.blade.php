@extends('adminDashboard.layouts.dashboard')
@section('body')
<div class="order-product-details">
    <table class="table table-stripped table-dark">
        <thead>
            <tr>
                <th>Sl No.</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Shipping</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1 ?>
            @foreach($products as $product)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$product->brand_name.' '.$product->product_name}} </td>
                <td>{{$product->pivot->quantity}}</td>
                <td>{{$product->price}}</td>
                @endforeach
                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalOrderInfo">Shipping Info</button></td>

            </tr>

        </tbody>

    </table>
<style>



</style>
    
    
<!-- Modal: modalCart -->
<div class="modal fade" id="modalOrderInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" >
  <div class="modal-dialog" role="document" style="width: 100%; float: center;">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel" style="background-color: tomato; color: #fff; border: 2px solid #000; text-align:center;">Shipping Details</h4>
      </div>
      <!--Body-->
    
      <div class="modal-body">
        <ul>
            <li> <b> Customer Name: </b>{{$order->firstname.' '.$order->lastname}}</li>
            <li> <b> Mobile Number: </b>{{$order->mobile_number}}</li>
            <li><b>Locality: </b>{{$order->locality}}</li>
            <li><b>Pin: </b>{{$order->pin}}</li> 
            <li><b>Street: </b>{{$order->street}}</li>
            <li><b>House Number: </b>{{$order->houseno}}</li>  
           <li><b>Tax: </b>{{$order->tax}}</li>
            <li><b>Total: </b>{{$order->total}}</li> 
        </ul>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-outline-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

   
</div>




             
              
@endsection