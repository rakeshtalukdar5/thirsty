@extends('adminDashboard.layouts.dashboard')
@section('body')
<h2>Shipped Orders</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
        <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Shipped</th>
                <th id="hh" style="text-align:center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
             $i = 1;?>
            
            @foreach ($getSellers as $getSeller)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $getSeller->id }}</td>
                {{-- @if($getSeller->id != null)
                <td>{{ $getSeller->user_id }}</td>
                @else
                    <td>Guest</td>
                @endif --}}
                {{-- <td>{{ $getSeller-> }}</td> --}}
                <td>{{ $getSeller->pin}}</td>
                <td>{{ $getSeller->firstname.' '.$getSeller->lastname }}</td>
                <td> {{$getSeller->mobile_number}} </td>
                {{-- <td> {{$getSeller->billing_email}} </td> --}}
                {{-- <td> {{$getSeller->locality}} </td> --}}
                {{-- <td> {{$getSeller->created_at}} </td> --}}
                {{-- @if($getSeller->shipped== false)
                    <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                @else
                    <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                @endif
                <td><form action="{{route('order.detail.view', $getSeller->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                <td><form action="{{route('admin.assignOrder', $getSeller->pin)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Assign</button> </form></td>
                <td><form action="{{route('admin.cancelOrder', $getSeller->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row"> --}}
                    <?php
                        // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                        // if (!empty($showAllButtons)) {
                        //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                        //     if ($showPending) {
                        //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                        //     }
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                        // }
                    ?>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$getSellers->links()}}</div>
            <div class="row"> Total Records: {{ $getSellers->total() }}</div>
        </div> --}}
    </div>
    {{-- <div class="pagination" style="float: right;">{{$getSellers->links()}}</div> --}}
</div>

@endsection