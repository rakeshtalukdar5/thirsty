@extends('adminDashboard.layouts.dashboard')
@section('body')
<h2>Approved Orders</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
     <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        @if(session()->has('status'))
            <div class="alert alert-danger text-center">{{session()->get('status')}}</div>
        @endif
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Shipped</th>
                <th id="hh" colspan="3" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
             $i = 1;?>
            
            @foreach ($approvedOrders as $approvedOrder)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $approvedOrder->id }}</td>
                @if($approvedOrder->user_id != null)
                <td>{{ $approvedOrder->user_id }}</td>
                @else
                    <td>Guest</td>
                @endif
                <td>{{ $approvedOrder->total }}</td>
                <td>{{ $approvedOrder->pin}}</td>
                <td>{{ $approvedOrder->firstname.' '.$approvedOrder->lastname }}</td>
                <td> {{$approvedOrder->mobile_number}} </td>
                <td> {{$approvedOrder->billing_email}} </td>
                <td> {{$approvedOrder->locality}} </td>
                <td> {{$approvedOrder->created_at}} </td>
                @if($approvedOrder->shipped== false)
                    <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                @else
                    <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                @endif
                <td><form action="{{route('order.detail.view', $approvedOrder->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                <td><form action="{{route('admin.assignOrder', $approvedOrder->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="assign"> Assign</button> </form></td>
                <td><form action="{{route('admin.cancelOrder', $approvedOrder->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row">
                    <?php
                        // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                        // if (!empty($showAllButtons)) {
                        //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                        //     if ($showPending) {
                        //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                        //     }
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                        // }
                    ?>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$approvedOrders->links()}}</div>
            <div class="row"> Total Records: {{ $approvedOrders->total() }}</div>
        </div> --}}
    </div>
    <div class="pagination" style="float: right;">{{$approvedOrders->links()}}</div>
</div>

@endsection