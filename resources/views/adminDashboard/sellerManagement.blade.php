@extends('adminDashboard.layouts.dashboard')
{{-- @include('layouts.layout') --}}

@section('body')
   <h2>Seller Management</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
        <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

    {{-- Add Brands Ends Here --}}
   <table class="table table-striped table-hover col-md-4">
        <thead>
            <tr>
                <th>SL. No</th>
                <th>Seller Name</th>
                <th>Shop Name</th>
                <th>Email</th>
                <th>Reg ID</th>
                <th>Mobile</th>
                <th>City</th>
                <th>Locality</th>
                <th>Pin</th>
                <th>Street</th>
                <th>Verifed</th>
                <th style="text-align: right;">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($sellers as $seller)
            <tr>
                <td> {{$i++}} </td>
                <td> {{$seller->firstname.' '.$seller->lastname}} </td>
                <td> {{$seller->shop_name}}</td>
                <td> {{$seller->email}} </td>
                <td> {{$seller->registration_id}} </td>
                <td> {{$seller->mobile_number}} </td>
                <td> {{$seller->city}} </td>
                <td> {{$seller->locality}} </td>
                <td> {{$seller->pin}} </td>
                <td> {{$seller->street}} </td>
                @if($seller->isVerified == false)
                    <td><form action="{{route('admin.sellerVerification', $seller->id)}}" method="POST">@csrf <button class="btn btn-danger btn-sm">No</button></form></td>
                @else
                    <td><form action="{{route('admin.sellerVerification', $seller->id)}}" method="POST">@csrf <button class="btn btn-success btn-sm">Yes</button></form></td>
                @endif
                <td><form action="{{route('admin.editProduct', ['id' => $seller->id])}}" method="POST"> @csrf <button class="btn btn-secondary btn-sm"  type="submit" name="edit"> Edit</button> </form></td>
                <td><form action="{{route('admin.deleteProduct', ['id' => $seller->id])}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Delete</button> </form></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination" style="float: right;">{{$sellers->links()}}</div>
</div>

@endsection