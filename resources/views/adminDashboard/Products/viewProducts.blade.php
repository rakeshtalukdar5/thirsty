@extends('adminDashboard.layouts.dashboard')
{{-- @include('layouts.layout') --}}

@section('body')
<div  class="container-fluid">
    {{--Start Products View --}}
    <div id="sellerProductsView" class=""> 
        <div id="addButtons" style="flex: right;">
            <a href="#addProducts" class="btn btn-primary" id="addProductsBtn" data-toggle="collapse" role="button">Add Products</a>
            
            <a href="#addBrands" class="btn btn-primary" id="addBrandsBtn" data-toggle="collapse" role="button">Add Brands</a>
        </div>

        {{-- Add Products Starts Here --}}
        <div class="tab form-group collapse" id="addProducts">
            <h3>ADD PRODUCTS</h3> 
            <form action="{{route('admin.storeProducts')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Product Name</label>
                    <input type="text" class="form-control" name="product_name" placeholder="Enter Product Name">
                </div>
                
                <div class="form-group">
                    <label>Brand Name</label>
                    <input type="text" class="form-control" name="brand_name" placeholder="Enter Brand Name">
                </div>
                
                <div class="form-group">
                    <label>Image</label>
                    <input type="text" class="form-control" name="banner_image" placeholder="Upload Image">
                </div>
                
                <label>Description</label>
                <div class="form-group">
                    <textarea name="description" id="description" cols="45" rows="5" placeholder="Write the Description"></textarea>            
                </div>
                <button class="btn btn-primary" id="saveBtn" type="submit" name="submit">Save</button>
            </form>
        </div>      
    {{-- End Add Products --}} 
    
    {{-- Add Brands Starts Here
    <div class="tab form-group collapse" id="addBrands">
        <h3>Add Brands</h3>
        <form action="/Products/storeBrands" method="POST">
            @csrf
            <div class="form-group">
                <label>Brand Name</label>
                <input type="text" class="form-control" name="brandName" placeholder="Enter Brand Name">
            </div>

                {{-- <div class="form-group">
                    <label>Brand Name</label>
                    <select id="brand_name" name="brand_name" value="brand_name"  class="form-control">
                        <option class="form-control" selected >Select Brand </option>
                        <option class="form-control">Bisleri</option>
                        <option class="form-control">Aquafina</option>
                        <option class="form-control">Bailey</option>
                        <option class="form-control">Non Branded</option>
                    </select>
                </div> --}}
            {{-- <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="banner_image">
            </div>
            <button class="btn btn-primary" id="saveBtn" type="submit" name="submit">Save</button>
        </form>
    </div> --}}

 {{-- </div> --}}


    {{-- Add Brands Ends Here --}}
   <table class="table table-striped table-hover col-md-4">
        <thead>
            <tr>
                <th>SL. No</th>
                <th>Product</th>
                <th>Brand</th>
                <th>Type</th>
                <th>Price</th>
                <th>Volume</th>
                <th>Quantity</th>
                <th>Details</th>
                <th>Added On</th>
                <th>Image</th>
                <th>Status</th>
                <th></th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($products as $product)
            <tr>
                <td> {{$i++}} </td>
                <td> {{$product->product_name}} </td>
                <td> {{$product->brand_name}}</td>
                <td> {{$product->type}} </td>
                <td> {{$product->price}} </td>
                <td> {{$product->volume}}L </td>
                <td> {{$product->quantity}} </td>
                <td> {{$product->description}} </td>
                <td> {{$product->created_at}} </td>

                <td><a href="{{asset('/storage/banner_images/'.$product->banner_image)}}" target="_blank"> <img src="{{asset('/storage/banner_images/'.$product->banner_image)}}" width="60px" height="45px" title={{$product->name}}></a></td>


                {{-- <td><a href="{{asset('assets/images/brand').'/'.$product->banner_image}}" target="_blank"> <img src="{{asset('assets/images/brand').'/'.$product->banner_image}}" width="60px" height="45px" title={{$product->name}}></a></td> --}}
                @if($product->status == false)
                    <td><form action="{{route('admin.productStatus', ['id' => $product->id])}}" method="POST">@csrf <button class="btn btn-danger btn-sm">Inactive</button></form></td>

                @else
                    <td><form action="{{route('admin.productStatus', ['id' => $product->id])}}" method="POST">@csrf <button class="btn btn-success btn-sm">Active</button></form></td>
                @endif
                <td><a href="{{route('admin.singleProductView', $product->id)}}" class="btn btn-primary btn-sm">  View</a> </td>
                <td><form action="{{route('admin.editProduct', ['id' => $product->id])}}" method="POST"> @csrf <button class="btn btn-secondary btn-sm"  type="submit" name="edit"> Edit</button> </form></td>
                <td><form action="{{route('admin.deleteProduct', ['id' => $product->id])}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Delete</button> </form></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination">{{$products->links()}}</div>
</div>



<style>
        #addButtons a{
            background-color: tomato;
            font-size: 1.3em;
            margin: 0.5% 0.5%; 
            display: block; 
            float: right;
            font-weight: bold;
            margin-right: 5px;
        }
        #addButtons a:hover {
            background-color: green;
        }
        .pagination {
            float: right;
        }
        </style>
        <style>
                /* {box-sizing: border-box} */
                /* #addBrands {
                        margin-left: 6%;
                        margin-right: 40%;
                        margin-bottom: 3%;
                } */
                .tab h3 {
                    background-color: #CC4C4C;
                    color: white;
                    text-align: center;
                    font-weight: bold;
                    margin-bottom: 0;
                    text-transform: uppercase;
                }
                /* Style the tab */
                .tab {
                    float: center;
                    border: 1px solid #ccc;
                    /* background-color: #f1f1f1; */
                    background-color: hsla(9, 100%, 64%, 0);
                    color: hsl(0, 0%, 0%);
                    width: 100%;
                    /* height: 150px; */
                    min-height: none;
                    margin-top: 1%;
                }
                #saveBtn {
                    margin: auto; 
                    display:block; 
                    width: 10%; 
                    text-align: center;
                    font-size: 23px;
                }
                /* Change background color of buttons on hover */
                .tab button:hover {
                    background-color: rgb(60, 179, 113);
                }
                    /* Style the tab content */
        </style>  
    
{{--End Products View --}}



@endsection