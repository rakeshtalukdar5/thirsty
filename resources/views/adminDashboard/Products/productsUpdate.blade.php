@extends('adminDashboard/layouts/dashboard')
@section('body')

    <style>
        #addProducts {
            margin-left: 6%;
            margin-right: 40%;
        }
        #description {
            /* margin-left: 6%; */
            margin-right: 6%;
        }
    
    </style>
    {{-- <div class="col-md-4"></div> --}}
    <div class="container" id="addProducts"  style="margin-top: 90px;">
        <h2>Update Products</h2>
        <form action="{{route('admin.updateProduct', ['id' => $product->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" class="form-control" name="product_name" value="{{$product->product_name}}">
                    </div>
                </div>    
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Brand Name</label>
                        <input type="text" class="form-control" name="brand_name" value="{{$product->brand_name}}">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="banner_image" value="{{$product->banner_image}}">
                    </div>
                </div>
                <div class="col-md-9">
                        <label>Description</label>
                </div>
                <div class="col-md-9">
                        <textarea name="description" id="description" cols="45" rows="5">{{$product->description}}</textarea>            
                    </div>
                </div>
                <div class="col-md-9">
                    <button class="btn btn-primary" style="margin: auto; display:block;" type="submit" name="update">Update</button>
                </div>
            </div>
        </form>
    </div>


    <style>

        
    </style>
@endsection
