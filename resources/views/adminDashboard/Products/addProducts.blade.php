@extends('adminDashboard.layouts.dashboard')
@section('body')
    <style>
        #addProducts {
            margin-left: 6%;
            margin-right: 40%;
        }
        #description {
            /* margin-left: 6%; */
            margin-right: 6%;
        }
    
    </style>
    <div id="addProducts"  class="form-group" style="margin-top: 90px;">
        <h2>Add Products</h2>
        <form action="{{route('admin.storeProducts')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Product</label>
                <select  id="productName" name="product_name" value="product_name"  class="form-control">
                    <option class="form-control" selected >Select Product </option>
                    <option>Can</option>
                    <option>Bottle</option>
                </select>
            </div>

            {{-- <div class="form-group">
                <label>Brand Name</label>
                <select id="brand_name" name="brand_name" value="brand_name"  class="form-control">
                    <option class="form-control" selected >Select Brand </option>
                    <option class="form-control">Bisleri</option>
                    <option class="form-control">Aquafina</option>
                    <option class="form-control">Bailey</option>
                    <option class="form-control">Non Branded</option>
                </select>
            </div>
                 --}}
            <div class="form-group">
                <label>Brand Name</label>
                <input type="text" class="form-control" name="brand_name" placeholder="Enter Brand Name">
            </div>

            <div class="form-group">
                <label>Type</label>
                <select  id="productType" name="type" value="type"  class="form-control">
                    <option class="form-control" selected >Select Type </option>
                    <option class="form-control">Mineral</option>
                    <option class="form-control">Ozonized</option>
                    <option class="form-control">RO</option>
                </select>
            </div>

            <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity" placeholder="Quantity">
            </div>

            <div class="form-group">
                <label>Product Volume</label>
                <select id="productVolume" name="volume" value="productVolume"  class="form-control">
                    <option class="form-control" selected >Select Product Volume </option>
                    <option class="form-control">20</option>
                    <option class="form-control">5</option>
                    <option class="form-control">1</option>
                </select>
            </div>
            
            <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="price" placeholder="Price">
            </div>
            
            <label>Description</label>
            <div class="form-group">
                <textarea name="description" id="description" cols="45" rows="5" placeholder="Write the Description"></textarea>            
            </div>

            <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" name="banner_image" placeholder="Upload Image">
            </div>

            <button class="btn btn-primary btn-md" style="margin: auto; display:block;" type="submit" name="submit">Save</button>
    </form>
</div>


    <style>

        
    </style>
@endsection
