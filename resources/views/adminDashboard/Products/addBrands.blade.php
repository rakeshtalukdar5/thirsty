@extends('adminDashboard.layouts.dashboard')
@section('body')
    <div id="addBrands"  class="form-group" style="margin-top: 90px;">
        <h2>ADD BRANDS</h2>
        <form action="/Products/store" method="POST">
            @csrf
            <div class="form-group">
                <label>Brand Name</label>
                <input type="text" class="form-control" name="brand" placeholder="Enter Brand Name">
            </div>

            {{-- <div class="form-group">
                <label>Brand Name</label>
                <select id="brand_name" name="brand_name" value="brand_name"  class="form-control">
                    <option class="form-control" selected >Select Brand </option>
                    <option class="form-control">Bisleri</option>
                    <option class="form-control">Aquafina</option>
                    <option class="form-control">Bailey</option>
                    <option class="form-control">Non Branded</option>
                </select>
            </div> --}}
          

            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="banner_image">
            </div>

            <button class="btn btn-primary btn-md" style="margin: auto; display:block;" type="submit" name="submit">Save</button>
        </form>
    </div>
    
    
    <style>
            #addBrands {
                margin-left: 6%;
                margin-right: 40%;
            }
        
    </style>


@endsection