@extends('adminDashboard/layouts/header')
@section('content')
<div class="container">
    <div class="admin">
        <h2>Welcome To Admin Page</h2>
        <p>The system is in your finger tips</p>
    </div>
</div>

<style>
.admin {
    margin-top: 10%;
    padding: 50px;
    border: 3px dashed tomato;
    background-color: #000;
    color: #fff;
    border-radius: 4px;
}
h2 {
    font-size: 30px;
    text-align: center;
}
p {
    font-size: 20px;
    font-style: italic;
    text-align: center;

}
</style>

@endsection