@extends('adminDashboard.layouts.dashboard')
@section('body')
<h2>Blogs</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
     <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}
    <style>
        .btn {
            margin: auto;
            display: block;
            width: 5%;
        }
    </style>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#post' });</script>

<div class="blogPost">
    <form action=" {{route('admin.postBlog')}} " method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-goup">
            <label>Title</label>
            <input type="text" class="form-control" name="title" placeholder="Blog Title">
            <textarea name="post" id="post" cols="222" rows="20">Write Your Blog Here....</textarea><br>
            <label>Choose Image</label>
            <input type="file" name="image" class="form-control">
            <button type="submit" class="btn btn-success btn-lg">Post</button>
        </div>
    </form>
</div>


@endsection