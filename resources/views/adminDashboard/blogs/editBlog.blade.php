@extends('adminDashboard.layouts.dashboard')
@section('body')
<h2>Edit Blog</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
        <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}
<style>
    .btn  {
        /* margin: auto; */
        background-color: #ff6666;
        color: #fff;
        margin: auto;
        display: block;
        width: 5%;
    }
    .btn:hover {
        background-color: #fff;
        color: #000;
    }
</style>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#post' });</script>

<div class="blogUpdate">
    <form action=" {{route('admin.updateBlog', $blog->id)}} " method="POST">
        @csrf
        <div class="form-goup">
            <label>Title</label>
            <input type="text" class="form-control" name="title" value={{$blog->title}}>
            <textarea name="post" id="post" cols="222" rows="20">{!!$blog->posts!!}</textarea><br>
            <label>Choose Image</label>
            <input type="file" name="image" class="form-control">
            <button type="submit" class="btn">Update</button>
        </div>
    </form>
</div>


@endsection