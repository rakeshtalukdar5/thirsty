@extends('adminDashboard.layouts.dashboard')
@section('body')
<div class="container">
    <h2>Blogs</h2>

    {{-- <style> -- Styles only for the above h2 tag which is the heading --}}
            <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
    {{-- </style> --}}
    <style>
        .btn  {
            /* margin: auto; */
            background-color: #ff6666;
            color: #fff;
            margin: 5px;;
        }
        .btn:hover {
            background-color: #fff;
            color: #000;
        }
        form {
            display: inline;
        }

    </style>
    <div class="blogs">
        @if(count($blogs) > 0)
            @foreach($blogs as $blog)
                <div class="well">
                    <h3> <a href="{{route('blog.singleBlog', $blog->id)}}"> {{$blog->title}} </a></h3>
                    <small>Written On {{ $blog->created_at}}</small>
                    <a href="{{route('blog.singleBlog', $blog->id)}}" class="btn btn-default">  View</a>
                    <form action="{{route('admin.editBlog', $blog->id)}}" method="POST"> @csrf <button class="btn">  Edit</button> </form>
                    <form action="{{route('admin.deleteBlog', $blog->id)}}" method="POST"> @csrf <button class="btn">  Delete</button> </form>
                </div> 
            @endforeach
        @else 
            <h2 class="alert alert-danger">No Blogs Found!!</h2>
        @endif
    </div>
    <div class="pagination" style="float: right;">{{$blogs->links()}}</div>
   
</div>

<style> 
.blogs {
    margin-top: 1%;
    margin-bottom: 25%;
}
</style>

@endsection