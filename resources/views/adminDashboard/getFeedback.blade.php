@extends('adminDashboard.layouts.dashboard')
{{-- @include('layouts.layout') --}}

@section('body')
   <h2>User Feedback</h2>

  {{-- <style> -- Styles only for the above h2 tag which is the heading --}}
        <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}
    {{-- Add Brands Ends Here --}}
   <table class="table table-striped table-hover col-md-4">
        <thead>
            <tr>
                <th>SL. No</th>
                <th>User Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Subject</th>
                <th colspan="2" >Message</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($feedbacks as $feedback)
            <tr>
                <td> {{$i++}} </td>
                <td> {{$feedback->user_id}} </td>
                <td> {{$feedback->name}}</td>
                <td> {{$feedback->email}} </td>
                <td> {{$feedback->subject}} </td>
                <td colspan="2"> {{$feedback->message}} </td>
                {{-- <td><form action="Products/{{$feedback->id}}/view" method="post"> @csrf <button class="btn btn-primary btn-sm" type="submit" name="view"> View</button> </form></td>
                <td><form action="{{route('admin.editProduct', ['id' => $feedback->id])}}" method="POST"> @csrf <button class="btn btn-secondary btn-sm"  type="submit" name="edit"> Edit</button> </form></td> --}}
                <td><form action="{{route('admin.deleteFeedback', $feedback->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Delete</button> </form></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination" style="float: right;">{{$feedbacks->links()}}</div>
</div>

@endsection