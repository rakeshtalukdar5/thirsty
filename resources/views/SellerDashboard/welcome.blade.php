@extends('SellerDashboard/layouts/header')
@section('content')
<div class="container">
    <div class="seller">
        <h2>Welcome To Seller Page</h2>
        <p>Be a step ahead from your competitor by Selling Your Products Online</p>
    </div>
</div>

<style>
.seller {
    margin-top: 10%;
    padding: 50px;
    border: 3px dashed tomato;
    background-color: #000;
    color: #fff;
    border-radius: 4px;
}
h2 {
    font-size: 30px;
    text-align: center;
}
p {
    font-size: 20px;
    font-style: italic;
    text-align: center;

}
</style>

@endsection