@extends('layouts.headerAndSidebar')
@section('content')

    <style>
        #addProducts {
            margin-left: 6%;
            margin-right: 47%;
        }
        #description {
            /* margin-left: 6%; */
            margin-right: 6%;
        }
    
    </style>
    {{-- <div class="col-md-4"></div> --}}
    <div id="updateDeliveryBoy"  class="form-group" style="margin-top: 90px;">
        <h2>Update Delivery Boy</h2>
        <form action="/Delivery-Boy/update/{{$deliveryBoy->id}}" method="POST">
            @csrf

            {{-- <div class="form-group">
                <label>Order ID</label>
                <input type="text" class="form-control" name="order_id" placeholder="Order ID">
            </div> --}}

            <div class="form-group">
                <label>Seller ID</label>
                <input type="text" class="form-control" name="seller_id" value={{$deliveryBoy->id}}>
            </div>




            <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control" name="firstname" value={{$deliveryBoy->firstname}}>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control" name="lastname" value={{$deliveryBoy->lastname}}>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value={{$deliveryBoy->email}}>
            </div>

            <div class="form-group">
                <label>City</label>
                <input type="text" class="form-control" name="city" value={{$deliveryBoy->city}}>
            </div>

            <div class="form-group">
                <label>Pin</label>
                <input type="text" class="form-control" name="pin" value={{$deliveryBoy->pin}}>
            </div>

            <div class="form-group">
                <label>Address</label>
                <textarea name="address" id="" cols="30" rows="10"> {{$deliveryBoy->address}}</textarea>
            </div>

            <div class="form-group">
                <label>Mobile Number</label>
                <input type="text" class="form-control" name="mobile_number" value={{$deliveryBoy->mobile_number}}>
            </div>

            <div class="form-group">
                <label>Gender</label>
                <select id="gender" name="gender" value="Gender"  class="form-control">
                    <option class="form-control" selected >Select Gender </option>
                    <option class="form-control">Male</option>
                    <option class="form-control">Female</option>
                    <option class="form-control">Others</option>
                </select>
            </div>

                     
            {{-- <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug" placeholder="Slug">
            </div>

            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="banner_image">
            </div> --}}

            <button class="btn btn-primary btn-md" style="margin: auto; display:block;" type="submit" name="update">Save</button>
    </form>
</div>


    <style>

        
    </style>
@endsection
