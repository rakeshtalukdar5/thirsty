@extends('layouts.headerAndSidebar')
@section('content')
<style>
#addDeliveryBoyBtn {
    background-color: tomato;
    font-size: 1.3em;
    margin: 0.5% 0.5%; 
    display: block; 
    float: right;
    font-weight: bold;
}
</style>
        
{{--Start Delivery Boy View --}}
<div id="deliveryBoyView" class="col-md-12"> 
    <div id="addDeliveryBoy" style="flex: right;">
        <a href="/AddDeliveryBoy" class="btn btn-primary" id="addDeliveryBoyBtn" role="button">Add Delivery Boy</a>
    </div>
    <div>
        <table class="table table-striped col-md-12">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>Mobile</th>
                    <th>Pin</th>
                    <th>Address</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($deliveryBoys->count() > 0)
                    @foreach ($deliveryBoys as $deliveryBoy)
                    <tr>
                        <td>{{$deliveryBoy->firstname." ".$deliveryBoy->lastname}} </td>
                        <td>{{$deliveryBoy->email}} </td>
                        <td>{{$deliveryBoy->city}} </td>
                        <td>{{$deliveryBoy->mobile_number}}</td>
                        <td>{{$deliveryBoy->pin}} </td>
                        <td>{{$deliveryBoy->address}} </td>
                        <td><form action="/Delivery-Boy/{{$deliveryBoy->id}}/edit" method="POST"> @csrf<button class="btn btn-seconday btn-sm" type="submit" name="edit">Edit</button> </form></td>
                        <td><form action="/Delivery-Boy/delete/{{$deliveryBoy->id}}" method="POST"> @csrf<button class="btn btn-danger btn-sm" type="submit" name="delete">Delete</button> </form></td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
{{-- End Delivery Boy View--}}            
        
@endsection