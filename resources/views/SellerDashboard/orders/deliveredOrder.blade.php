@extends('SellerDashboard.dashboard')
@section('body')
<h2>Delivered Orders</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
    <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}
<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Delivred at</th>
                <th id="hh" style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if( $deliveredOrders != null )
                <?php $i = 1; ?>
                @foreach ($deliveredOrders as $deliveredOrder)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $deliveredOrder->id }}</td>
                    <td>{{ $deliveredOrder->user_id }}</td>
                    <td>{{ $deliveredOrder->total }}</td>
                    <td>{{ $deliveredOrder->pin}}</td>
                    <td>{{ $deliveredOrder->firstname.' '.$deliveredOrder->lastname }}</td>
                    <td> {{$deliveredOrder->mobile_number}} </td>
                    <td> {{$deliveredOrder->billing_email}} </td>
                    <td> {{$deliveredOrder->locality}} </td>
                    <td> {{$deliveredOrder->created_at}} </td>
                    <td> {{$deliveredOrder->updated_at}} </td>
                    {{-- @if($deliveredOrder->shipped== false)
                        <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                    @else
                        <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                    @endif --}}
                    <td><form action="{{route('order.detail.view', $deliveredOrder->id)}}" method="GET"> @csrf <button class="btn btn-primary btn-sm" type="submit" name="view"> View</button> </form></td>
                    {{-- <td><form action="{{route('seller.acceptOrder', $deliveredOrder->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Ship</button> </form></td> --}}
                    {{-- <td><form action="{{route('admin.cancelOrder', $deliveredOrder->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row"> --}}
                        <?php
                            // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                            // if (!empty($showAllButtons)) {
                            //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                            //     if ($showPending) {
                            //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                            //     }
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                            // }
                        ?>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <h3>No Order Found</h3>
            @endif
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$deliveredOrders->links()}}</div>
            <div class="row"> Total Records: {{ $deliveredOrders->total() }}</div>
        </div> --}}
    </div>
</div>

@endsection