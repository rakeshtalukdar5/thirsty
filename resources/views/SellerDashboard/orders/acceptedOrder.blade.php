@extends('SellerDashboard.dashboard')
@section('body')
<h2>Accepted Orders</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
    <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                {{-- <th>Shipped</th> --}}
                <th id="hh" colspan="3" style="text-align:center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if( $acceptedOrders != null )
                <?php $i = 1; ?>
                @foreach ($acceptedOrders as $acceptedOrder)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $acceptedOrder->id }}</td>
                    <td>{{ $acceptedOrder->user_id }}</td>
                    <td>{{ $acceptedOrder->total }}</td>
                    <td>{{ $acceptedOrder->pin}}</td>
                    <td>{{ $acceptedOrder->firstname.' '.$acceptedOrder->lastname }}</td>
                    <td> {{$acceptedOrder->mobile_number}} </td>
                    <td> {{$acceptedOrder->billing_email}} </td>
                    <td> {{$acceptedOrder->locality}} </td>
                    <td> {{$acceptedOrder->created_at}} </td>
                    {{-- @if($acceptedOrder->shipped== false)
                        <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                    @else
                        <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                    @endif --}}
                    <td><form action="{{route('order.detail.view', $acceptedOrder->id)}}" method="GET"> @csrf <button class="btn btn-primary btn-sm" type="submit" name="view"> View</button> </form></td>
                    <td><form action="{{route('seller.shipOrder', $acceptedOrder->id)}}" method="POST"> @csrf <button class="btn btn-success btn-sm"  type="submit"> Ship</button> </form></td>
                    <td><form action="{{route('seller.cancelOrder', $acceptedOrder->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row">
                        <?php
                            // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                            // if (!empty($showAllButtons)) {
                            //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                            //     if ($showPending) {
                            //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                            //     }
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                            // }
                        ?>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <h3>No Order Found</h3>
            @endif
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$acceptedOrders->links()}}</div>
            <div class="row"> Total Records: {{ $acceptedOrders->total() }}</div>
        </div> --}}
    </div>
</div>

@endsection