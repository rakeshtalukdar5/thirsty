@extends('SellerDashboard.dashboard')
@section('body')
<h2>Order Status</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
    <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Order Status</th>
                {{-- <th>Shipped</th> --}}
                <th id="hh" style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
             $i = 1;?>
            
            @foreach ($orders as $order)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $order->id }}</td>
                <td>{{ $order->user_id }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->pin}}</td>
                <td>{{ $order->firstname.' '.$order->lastname }}</td>
                <td> {{$order->mobile_number}} </td>
                <td> {{$order->billing_email}} </td>
                <td> {{$order->locality}} </td>
                <td> {{$order->created_at}} </td>
                @if($order->order_status === 'Cancelled')
                    <td> <button class="btn btn-danger btn-sm"> {{$order->order_status}} </button> </td>
                @else
                    <td> <button class="btn btn-primary btn-sm"> {{$order->order_status}} </button> </td>
                @endif
{{--    
                @if($order->shipped== false)
                    <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                @else
                    <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                @endif --}}
                <td><form action="{{route('order.detail.view', $order->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                {{-- <td><form action="{{route('admin.approveOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Approve</button> </form></td>
                <td><form action="{{route('admin.cancelOrder', $order->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row"> --}}
                    <?php
                        // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                        // if (!empty($showAllButtons)) {
                        //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                        //     if ($showPending) {
                        //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                        //     }
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                        //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                        // }
                    ?>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$orders->links()}}</div>
            <div class="row"> Total Records: {{ $orders->total() }}</div>
        </div> --}}
    {{-- </div>
    <div class="pagination" style="float: right;">{{$orders->links()}}</div>
</div> --}}

@endsection