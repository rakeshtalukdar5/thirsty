@extends('SellerDashboard.dashboard')
@section('body')
<h2>Pending Orders</h2>

{{-- <style> -- Styles only for the above h2 tag which is the heading --}}
    <link href="{{asset('/assets/thirsty/css/admin/orderHeading.css')}}" rel="stylesheet">   
{{-- </style> --}}

<div class="box-body" id="pagination-table">
    <table id="example" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Order ID</th>
                <th>CustomerId</th>
                <th>Amount</th>
                <th>Pin</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Locality</th>
                <th>Ordered at</th>
                <th>Shipped</th>
                <th id="hh" colspan="2" style="text-align:center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if( $getOrders != null )
                <?php $i = 1; ?>
                @foreach ($getOrders as $getOrder)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $getOrder->id }}</td>
                    <td>{{ $getOrder->user_id }}</td>
                    <td>{{ $getOrder->total }}</td>
                    <td>{{ $getOrder->pin}}</td>
                    <td>{{ $getOrder->firstname.' '.$getOrder->lastname }}</td>
                    <td> {{$getOrder->mobile_number}} </td>
                    <td> {{$getOrder->billing_email}} </td>
                    <td> {{$getOrder->locality}} </td>
                    <td> {{$getOrder->created_at}} </td>
                    {{-- @if($getOrder->shipped== false)
                        <td><button class="disabled btn btn-danger btn-sm">No</button></td>
                    @else
                        <td> <button class="disabled btn btn-success btn-sm">Yes</button></td>
                    @endif --}}
                    <td><form action="{{route('order.detail.view', $getOrder->id)}}" method="GET"> @csrf <button class="btn btn-secondary btn-sm" type="submit" name="view"> View</button> </form></td>
                    <td><form action="{{route('seller.acceptOrder', $getOrder->id)}}" method="POST"> @csrf <button class="btn btn-primary btn-sm"  type="submit" name="edit"> Accept</button> </form></td>
                    <td><form action="{{route('seller.cancelOrder', $getOrder->id)}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Cancel</button> </form></td>                    <div class="row">
                        <?php
                            // echo '<div class="col-xs-3 nopadding"><a  href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-info">Info</a></div>';
                            // if (!empty($showAllButtons)) {
                            //     // echo '<a href="#"  onclick=\'showData('.json_encode($enquiry).')\' class="btn btn-block btn-info">Info</a>';
                            //     if ($showPending) {
                            //         echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::PENDING_ENQ]).'"  class="btn btn-warning action-button">Pending</a></div>';
                            //     }
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=>  App\Enquiry::COMPLETED_ENQ]).'" class="btn btn-success action-button">Confirm</a></div>';
                            //     echo '<div class="col-xs-3 nopadding"><a href="'.route("change-status", ["id"=> $enquiry->id, "status"=> App\Enquiry::DELETED_ENQ]).'" class="btn btn-danger action-button">Delete</a></div>';
                            // }
                        ?>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <h3>No Order Found</h3>
            @endif
        </tbody>
    </table>
</div>
        {{-- <div class="box-body">
            <div class="row">  {{$getOrders->links()}}</div>
            <div class="row"> Total Records: {{ $getOrders->total() }}</div>
        </div> --}}
    </div>
</div>

@endsection