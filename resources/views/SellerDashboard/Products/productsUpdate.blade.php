@extends('SellerDashboard.dashboard')
@section('body')

    <style>
        #addProducts {
            margin-left: 6%;
            margin-right: 40%;
        }
        #description {
            /* margin-left: 6%; */
            margin-right: 6%;
        }
    
    </style>
    {{-- <div class="col-md-4"></div> --}}
    <div id="addProducts"  class="form-group" style="margin-top: 90px;">
        <h2>Update Products</h2>
    <form action="/Products/update/{{$product->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label>Product</label>                
                <select  id="productName" name="name" value="name"  class="form-control">
                    <option class="form-control" selected >Select Product </option>
                    <option class="form-control">Can</option>
                    <option class="form-control">Bottle</option>
                    <option class="form-control">Mineral</option>
                </select>
            </div>

            <div class="form-group">
                <label>Brand Name</label>
                <select id="brand_name" name="brand_name" value="brand_name"  class="form-control">
                    <option class="form-control" selected >Select Brand </option>
                    <option class="form-control">Bisleri</option>
                    <option class="form-control">Aquafina</option>
                    <option class="form-control">Bailey</option>
                </select>
            </div>

            {{-- <div class="form-group">
                <label>Brand Name</label>
                <input type="text" class="form-control" name="brand_name" placeholder="Brand Name">
            </div> --}}

            <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" value={{$product->quantity}} name="quantity">
            </div>

            <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="price" value={{$product->price}}>
            </div>
            
            {{-- <div class="form-group">
                <label>Stock</label>
                <select  id="productStock" name="stock" value="Stock"  class="form-control">
                    <option class="form-control" selected >Select Stock </option>
                    <option class="form-control">Available</option>
                    <option class="form-control">Not Available</option>
                </select>        
            </div> --}}

            {{-- <div class="form-group">
                <label>Manufacture Date</label>
                <input type="date" class="form-control" name="manufacture_date" placeholder="Manufacture Date">
            </div> --}}

            <label>Description</label>
            <div class="form-group">
                <textarea name="description" id="description" cols="40" rows="3">{{$product->description}}</textarea>            
            </div>
            <div class="form-group">
                <label>Product Volume</label>
                <select id="productVolume" name="volume" value="productVolume"  class="form-control">
                    <option class="form-control" selected >Select Product Volume </option>
                    <option class="form-control">20</option>
                    <option class="form-control">5</option>
                    <option class="form-control">1</option>
                </select>
            </div>

            <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug" value={{$product->slug}}>
            </div>

            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="banner_image" value={{$product->banner_image}}>
            </div>

            <button class="btn btn-primary btn-md" style="margin: auto; display:block;" type="submit" name="update">Save</button>

    </form>
    
{{-- <form action="carts/store" method="POST">
    @csrf
         <div form-group>
            <input type="text" class="form-control" name="product_id" >
        </div> 
        <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="total_price" placeholder="Price">
            </div>
          
            <div class="form-group">
                <label>Qtty</label>
                <select  name="quantity" value="quantity"  class="form-control">
                    <option class="form-control" selected > Quanity </option>
                    <option class="form-control">20 </option>
                    <option class="form-control">10</option>
                </select>        
            </div>
            <button class="btn btn-primary btn-md" style="margin: auto; display:block;" type="submit" name="submit">Save</button>




</form> --}}

</div>


    <style>

        
    </style>
@endsection
