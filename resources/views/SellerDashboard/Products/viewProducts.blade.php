@extends('SellerDashboard.dashboard')
@section('body')


<style>
#addProductsBtn {
    background-color: tomato;
    font-size: 1.3em;
    margin: 0.5% 0.5%; 
    display: block; 
    float: right;
    font-weight: bold;
}
</style>

{{--Start Products View --}}
<div id="sellerProductsView" class=""> 
    {{-- Start Add Products --}}
    <div id="addProducts" style="flex: right;">
        <a href="/seller/Products/AddProducts" class="btn btn-primary" id="addProductsBtn" role="button">Add Products</a>
    </div>
    {{-- End Add Products --}}            
    <table class="table table-striped table-hover col-md-4">
        <thead>
            <tr>
                <th>SL. No</th>
                <th>Product</th>
                <th>Brand</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Details</th>
                <th>Added On</th>
                <th>Volume</th>
                <th>Image</th>
                <th></th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td> {{$product->id}} </td>
                <td> {{$product->product_name}} </td>
                <td> {{$product->brand_name}}</td>
                <td> {{$product->type}} </td>
                <td> {{$product->quantity}} </td>
                <td> {{$product->description}} </td>
                <td> {{$product->created_at}} </td>
                <td> {{$product->volume}} </td>
                 <td><img src="{{$product->banner_image}}" alt="Bisleri" width="20px" height="30px"></td>
                <td><form action="Products/{{$product->id}}/view" method="post"> @csrf <button class="btn btn-primary btn-sm" type="submit" name="view"> View</button> </form></td>
                <td><form action="Products/{{$product->id}}/edit" method="POST"> @csrf <button class="btn btn-secondary btn-sm"  type="submit" name="edit"> Edit</button> </form></td>
                <td><form action="Products/delete/{{$product->id}}" method="POST"> @csrf <button class="btn btn-danger btn-sm"  type="submit" name="delete"> Delete</button> </form></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{--End Products View --}}

@endsection