@extends('SellerDashboard.dashboard')
@section('body')

    <style>
        #addProducts {
            margin-left: 6%;
            margin-right: 40%;
        }
        #description {
            /* margin-left: 6%; */
            margin-right: 6%;
        }
    
    </style>
    {{-- <div class="col-md-4"></div> --}}
    <div id="addProducts"  class="form-group" style="margin-top: 90px;">
        <h2>Add Products</h2>
        <form action="/seller/Products/store" method="POST">
            @csrf
            <div class="form-group">
                <label>Seller Id</label>
                <input type="text" class="form-control" name="seller_id" placeholder="Enter Seller ID">
            </div>
            {{-- @foreach($products->chunk(1) as $productchunk) --}}
            <div class="form-group">
                <label>Product</label>
                <select  id="productName" name="productId"  class="form-control">
                    <option class="form-control" selected >Select Product </option>
                    @foreach($products as $product)
                        <option value="{{$product->id}}" class="form-control">{{$product->product_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Brand Name</label>
                <select id="brand_name" name="brand_name"   class="form-control">
                    <option class="form-control" selected >Select Brand </option>
                    @foreach($products as $product)
                        <option value="{{$product->id}}" class="form-control">{{$product->brand_name}}</option>
                    @endforeach
                </select>
            </div>
            {{-- @endforeach --}}

            <div class="form-group">
                <label>Type</label>
                <select  id="productType" name="type" value="type"  class="form-control">
                    <option class="form-control" selected >Select Type </option>
                    <option class="form-control">Mineral</option>
                    <option class="form-control">Ozonized</option>
                    <option class="form-control">RO</option>
                </select>
            </div>
            
            {{-- <div class="form-group">
                <label>Brand Name</label>
                <input type="text" class="form-control" name="brand_name" placeholder="Brand Name">
            </div> --}}
            <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity" placeholder="Quantity">
            </div>
            <div class="form-group">
                <label>Product Volume</label>
                <select id="productVolume" name="volume" value="productVolume"  class="form-control">
                    <option class="form-control" selected >Select Product Volume </option>
                    <option class="form-control">20</option>
                    <option class="form-control">5</option>
                    <option class="form-control">1</option>
                </select>
            </div>
            <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="price" placeholder="Price">
            </div claas="btn-group">
                <button class="btn btn-primary btn-lg" style="margin: auto; display:block;" type="submit" name="submit">Save</button>
            </div>
        </div>
    </form>
    <style>

        
    </style>
@endsection
