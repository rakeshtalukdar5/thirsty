@extends('SellerDashboard/layouts/header')
@section('content')

 <section id="aa-myaccount">
  <div class="container">
      @if(Session::has('Success'))
      <div class="alert alert-success" style="font-size: 20px; text-align: center;">
        {{Session::get('Success')}}
      </div>
    @endif

    @if(session()->has('status'))
      <div class="alert alert-success">
        {{session()->get('status')}}
      </div>
    @endif
    @if(count($errors) > 0)
      <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
       <div class="aa-myaccount-area">         
           <div class="row">
             <div class="col-md-9">
               <div class="aa-myaccount-login">
               <h4>Seller Login</h4>
               <form action="{{route('seller.login')}}" method="POST" class="aa-login-form">
                @csrf 
                 <label for="">Email address<span>*</span></label>
                  <input type="email" name="email"  placeholder=" Enter Email" autofocus required>
                  <label for="">Password<span id="error">*</span></label>
                   <input type="password" placeholder="Password" name="password" required>
                   <button type="submit" class="aa-browse-btn">Login</button>
                   {{-- <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label> --}}
                    <p class="aa-lost-password"><a href="{{route('user.forgotPassword')}}">Lost your password?</a></p> <hr>
                 </form>
                 <p> Don't have an account?<a href="{{route('seller.register')}}">Register now!</a> </p>
              </div>
             </div>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
