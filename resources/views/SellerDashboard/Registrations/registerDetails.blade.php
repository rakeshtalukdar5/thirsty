
@extends('SellerDashboard.dashboard')
@section('body')
    
     <section id="checkout">
       <div class="container">
          @if(Session::has('Success'))
            <div class="alert alert-success" style="font-size: 20px; text-align: center;">
              {{Session::get('Success')}}
            </div>
          @endif

          @if(count($errors) > 0)
            <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
              <ul>
                @foreach($errors->all() as $error)
                  <li>{{$error}}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="checkout-area">
                <div class="row">
                  <div class="col-md-8">
                    <div class="checkout-left">
                      <div class="panel-group" id="accordion">
                        
                        {{-- @if(!Auth::check()) --}}
                        <!-- Login section -->
                        {{-- <div class="panel panel-default aa-checkout-login">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                Client Login 
                              </a>
                            </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body"> --}}
                            {{-- <form action="{{route('user.signin')}}" method="POST"> --}}
                                {{-- @csrf --}}
                              {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat voluptatibus modi pariatur qui reprehenderit asperiores fugiat deleniti praesentium enim incidunt.</p>
                              <input type="email" placeholder="Email">
                              <input type="password" placeholder="Password">
                              <button type="submit" class="aa-browse-btn">Login</button>
                              <label for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                              <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                            </form>
                            </div>
                          </div>
                        </div> --}}
                       {{-- @else --}}
                        <!-- Seller Registration Form -->
                        <div class="panel panel-default aa-checkout-billaddress">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              {{-- <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> --}}
                                Seller Registration
                              {{-- </a> --}}
                            </h4>
                          </div>

                          {{-- <div id="collapseFour" class="panel-collapse collapse"> --}}
                            <div class="panel-body">
                            <form method="POST" action="{{route('seller.Registration')}}">
                            @csrf
                             <div class="row">
                                {{-- <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                        <input type="email" name="email" placeholder="Enter Email*">
                                    </div>                             
                                </div>
                                <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                        <input type="password" name="password" placeholder="Password*">
                                    </div>
                                </div> --}}
                                <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                        <input type="text" name="shop_name" placeholder="Shop Name*">
                                    </div>                             
                                </div>
               
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="first_name" placeholder="First Name*">
                                  </div>                             
                                </div>
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="last_name" placeholder="Last Name*">
                                  </div>
                                </div>
                                
                                  {{-- </div>  --}}
                              {{-- <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="email" name="email" placeholder="Email Address*">
                                  </div>                             
                                </div> --}}
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="registration_id" placeholder="Registration ID*">
                                  </div>
                                </div>
                              {{-- </div> --}}
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="tel" name="mobile_number" maxlength="10" minlength="10" placeholder="Phone*">
                                  </div>
                                </div>
                                                              {{-- </div>  --}}
                              {{-- <div class="row"> --}}
                              <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="city" placeholder="City / Town*">
                                  </div>
                            </div>
                            <div class="col-md-6">
                                <div class="aa-checkout-single-bill">
                                    <input type="text" name="pin" placeholder="Postcode / ZIP*">
                                </div>
                            </div>
                              {{-- </div> 
                              <div class="row"> --}}
                                <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                      <input type="text" name="locality" placeholder="Locality/Area.">
                                    </div>                             
                                </div>                            
                              {{-- </div>    --}}
                              {{-- <div class="row"> --}}
                                  <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                      <input type="text" name="street" placeholder="Street">
                                    </div>                             
                                  </div>                            
                              {{-- </div>   --}}

                              {{-- <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" placeholder="District*">
                                  </div>                             
                                </div> --}}
                                
                              {{-- </div>  --}}
                              
                            </div>
                            <button type="submit" class="aa-browse-btn" style="margin: auto; display: block;"> Save </button>   

                         </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                    </div>
                  </div>
                </div>
             </div>
        </section>


    
    {{-- @include('UI.layouts.footer')           --}}
    @endsection