@extends('UI.layouts.header')
@section('content')
 <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="{{asset('/assets/thirsty/img/slider/header.jpg')}}" height="200px" width="100%" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
        <div class="aa-catg-head-banner-content">
          <h2>Shopping Cart</h2>
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>                   
            <li class="active">Cart</li>
          </ol>
        </div>
      </div>
    </div>
  </section>
       <!-- / catg header banner section -->
     
<!-- Cart view section -->
  <section id="cart-view">
    <div class="container">
      {{-- Displaying Error Message --}}
      @if(Session::has('error'))
        <div class="alert alert-danger"> <h4 class="text-center">{{Session::get('error')}}</h4></div>
      @endif
      {{-- Displaying Success Message --}}
      @if(Session::has('Success'))
        <div class="alert alert-success"><h4 class="text-center">{{Session::get('Success')}}</h4></div>
      @endif

      @if(Cart::count() > 0)
        <h4 class="text-center alert alert-info">{{Cart::count()}} item(s) in your cart</h4>  
        <div class="row">
          <div class="col-md-12">
            <div class="cart-view-area">
              <div class="cart-view-table">
                <form action="">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Image</th>
                          <th>Product</th>
                          <th>Price</th>
                          <th>Quantity</th>
                          <th>Total</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($items as $item)
                          <tr> 
                            <td><a href="{{route('shop.product', $item->model->slug)}}"><img src="{{asset('/storage/banner_images/'.$item->model->banner_image)}}" alt="{{$item->brand_name.' '.$item->product_name}}" style="height: 50px;"></a></td>
                            <td><a class="aa-cart-title" href="{{route('shop.product', $item->model->slug)}}">{{$item->name}}</a></td>
                            <td> &#8377 {{$item->price}}</td>
                            {{-- <td><p class="text-center">{{$item->qty}}</p></td> --}}
                            <td class="updateQty">
                              <a  href="{{route('cart.decreaseQty', ['id' => $item->rowId, 'qty' => $item->qty])}}" class="value-button" id="decrease" value="Decrease Value"> <b> - </b></a>
                                <input type="number" id="number" value="{{$item->qty}}" disabled>
                              <a  href="{{route('cart.increaseQty', ['id' => $item->rowId, 'qty' => $item->qty])}}" class="value-button" id="increase" value="Increase Value"><b> + </b></a>
                            </td>
                            <td> &#8377 {{$item->price * $item->qty}}</td>
                            <td>
                              <a class="btn btn-danger btn-xs remove" href="{{route('cart.remove', $item->rowId)}}" style="color: white">Remove</a>
                              <a class="btn btn-primary btn-xs saveForLater" href="{{route('cart.saveForLater', $item->rowId)}}" style="color: white">Save For Later</a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </form>
                  <!-- Cart Total view -->
                <div class="cart-view-total">
                  <h4>Cart Totals</h4>
                  <table class="aa-totals-table">
                    <tbody>
                      <tr>
                        <th>Subtotal</th>
                        <td> &#8377 {{Cart::Subtotal()}}</td>
                      </tr>
                      <tr>
                        <th>Tax</th>
                        <td> &#8377 {{Cart::tax()}}</td>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <td> &#8377 {{Cart::total()}}</td>
                      </tr>
                    </tbody>
                  </table>
                  <a href="{{route('checkout')}}" class="aa-cart-view-btn">Proced to Checkout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      @else 
        
        <div style="text-align:center;">
            <h3>No items in your cart!!</h3>
            <a href="{{route('shop')}}" class="btn btn-success btn-lg">Continue Shopping</a>
        </div>
      @endif


          <!--  Save For Later  -->
      <table class="table">
        <thead>
          <tr>
            <th>Save For Later</th>
          </tr>
        </thead>
        <tbody>
          @if(Cart::instance())
            @foreach(Cart::instance('saveForLater')->content() as $item)
              <tr>
                <td>
                  <a href="{{route('shop.product', $item->model->slug)}}">
                    <img src="{{asset('/storage/banner_images/'.$item->model->banner_image)}}" alt="{{$item->brand_name.' '.$item->product_name}}" style="height: 50px;">
                  </a>
                  <a class="aa-cart-title" href="{{route('shop.product', $item->model->slug)}}">{{$item->name}}</a> &#8377 {{$item->price}} 
                </td>
                <td>
                  <a class="btn btn-danger btn-xs remove" href="{{route('cart.removeSaveLater', $item->rowId)}}" style="color: white">Remove</a>
                  <a class="btn btn-danger btn-xs move" href="{{route('cart.moveToCartFromSaveForLater', $item->rowId)}}" style="color: white">Move To Cart</a>
                </td>
              </tr>
            @endforeach
          @else
          
          @endif
        </tbody>
      </table>
    </div>
  </section>
<!-- / Cart view section -->


{{-- Style fot update buttons --}}
<style>
  .updateQty {
    width: 300px;
    margin: 0 auto;
    text-align: center;
    padding-top: 50px;
  }

  .value-button {
    display: inline-block;
    border: 1px solid #ddd;
    margin: 0px;
    width: 40px;
    height: 20px;
    text-align: center;
    vertical-align: middle;
    padding: 0;
    background: #eee;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  .value-button:hover {
    cursor: pointer;
  }

  .updateQty #decrease {
    margin-right: -4px;
    border-radius: 8px 0 0 8px;
  }

  .updateQty #increase {
    margin-left: -4px;
    border-radius: 0 8px 8px 0;
  }

  .updateQty #input-wrap {
    margin: 0px;
    padding: 0px;
  }

  input#number {
    text-align: center;
    border: none;
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    margin: 0px;
    width: 40px;
    height: 40px;
  }

  input[type=number]::-webkit-inner-spin-button,
  input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
  }
</style>


@include('UI.layouts.footer')          
@endsection