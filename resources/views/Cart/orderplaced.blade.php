@extends('UI.layouts.header')
@section('content')
<div class="container">
        <div class="orderPlacedMessage">
            <h2>Your OrderSuccessfully Placed</h2>
            <p>Thank You For Shopping With Us !! See You Soon.</p>
            <a href="{{route('shop')}}" class="btn btn-success">Continue Shopping</a>
        </div>
    </div>
    
    <style>
    .orderPlacedMessage {
        margin-top: 10%;
        padding: 50px;
        border: 3px dashed tomato;
        background-color: #000;
        color: #fff;
        border-radius: 4px;
    }
    h2 {
        font-size: 30px;
        text-align: center;
    }
    p {
        font-size: 20px;
        font-style: italic;
        text-align: center;
    
    }
    .btn {
        display: block;
        margin: auto;
    }
    /* .btn-success {
        margin-left: 300px;
        margin-right: 300px;
    } */
    </style>






@endsection