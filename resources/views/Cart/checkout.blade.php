
@extends('UI.layouts.header')
@section('content')


<!-- catg header banner section -->
<section id="aa-catg-head-banner">
        <img src="{{asset('/assets/thirsty/img/slider/header.jpg')}}" height="200px" width="100%" alt="fashion img">
        <div class="aa-catg-head-banner-area">
         <div class="container">
          <div class="aa-catg-head-banner-content">
            <h2>Checkout Page</h2>
            <ol class="breadcrumb">
              <li><a href="/">Home</a></li>                   
              <li class="active">Checkout</li>
            </ol>
          </div>
         </div>
       </div>
      </section>
      <!-- / catg header banner section -->
    
     <!-- Cart view section -->
     <section id="checkout">
       <div class="container">
          @if(Session::has('Success'))
            <div class="alert alert-success" style="font-size: 20px; text-align: center;">
              {{Session::get('Success')}}
            </div>
          @endif

          @if(count($errors) > 0)
            <div class="alert alert-danger" style="font-size: 20px; text-align: center;">
              <ul>
                @foreach($errors->all() as $error)
                  <li>{{$error}}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="checkout-area">
                <div class="row">
                  <div class="col-md-8">
                    <div class="checkout-left">
                      <div class="panel-group" id="accordion">
                        <!-- Coupon section -->
                        <div class="panel panel-default aa-checkout-coupon">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                Have a Coupon?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                          
                            <form action="{{route('discount.store')}}" method="POST">
                              @csrf 
                              <input type="text" name="coupon" placeholder="Coupon Code" class="aa-coupon-code">
                              <button type="submit" class="aa-browse-btn">Apply Coupon </button>
                            </form>
                            
                          </div>
                          </div>
                        </div>
                        {{-- @if(!Auth::check()) --}}
                        <!-- Login section -->
                        {{-- <div class="panel panel-default aa-checkout-login">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                Client Login 
                              </a>
                            </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                            <form action="{{route('user.signin')}}" method="POST">
                                @csrf
                              <input type="email" placeholder="Email">
                              <input type="password" placeholder="Password">
                              <button type="submit" class="aa-browse-btn">Login</button>
                              <label for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                              <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                            </form>
                            </div>
                          </div>
                        </div> --}}
                       {{-- @else --}}
                        <!-- Shipping Address -->
                        <div class="panel panel-default aa-checkout-billaddress">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                Shippping Address
                              </a>
                            </h4>
                          </div>
                   @if($user)
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                            <form method="POST" action="{{route('order')}}">
                            @csrf
                             <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <label>First Name</label>
                                    <input type="text" name="first_name"  autofocus value={{$customer->firstname}}>
                                  </div>                             
                                </div>
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" value={{$customer->lastname}}>
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <label>Email</label>
                                    <input type="email" name="email" value={{$user->email}}>
                                  </div>                             
                                </div>
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <label>Mobile Number</label>
                                    <input type="tel" name="mobile_number" value={{$customer->mobile_number}}>
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                              <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                    <label>City</label>
                                    <input type="text" name="city" value={{$customer->city}}>
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                      <label>Locality</label>
                                      <input type="text" name="locality" value={{$customer->locality}}>
                                    </div>                             
                                </div>                            
                              </div>   
                              <div class="row">
                                  <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                      <label>Street</label>
                                      <input type="text" name="street" value={{$customer->street}}>
                                    </div>                             
                                  </div>                            
                              </div>  

                              <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                      <label>House Number</label>
                                      <input type="number" name="houseno" value={{$customer->houseno}}>
                                  </div>                             
                                </div>  
                              {{-- <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" placeholder="District*">
                                  </div>                             
                                </div> --}}
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <label>Area Pin</label>
                                    <input type="text" name="pin" value={{$customer->pin}}>
                                  </div>
                                </div>
                              </div> 
                               <div class="row">
                                <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                    <textarea cols="8" name="note" rows="3">Special Notes</textarea>
                                  </div>                             
                                </div>                            
                              </div>   
                              <div class="aa-payment-method">                    
                                  <label for="cashdelivery"><input type="radio" id="cashdelivery" value="COD" name="payment_mode"> Cash on Delivery </label>
                              </div>
                            </div>
                            <button type="submit" class="aa-browse-btn"> Place Order </button>   

                        </form>
                          </div>
                        </div>
                   @else
                          <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                            <form method="POST" action="{{route('order')}}">
                            @csrf
                             <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="first_name" placeholder="First Name*">
                                  </div>                             
                                </div>
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="last_name" placeholder="Last Name*">
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="email" name="email" placeholder="Email Address*">
                                  </div>                             
                                </div>
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="tel" maxlength="10" minlength="10" name="mobile_number" placeholder="Phone*">
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                              <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" name="city" placeholder="City / Town*">
                                  </div>
                                </div>
                              </div> 
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                      <input type="text" name="locality" placeholder="Locality/Area*">
                                    </div>                             
                                </div>                            
                              </div>   
                              <div class="row">
                                  <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                      <input type="text" name="street" placeholder="Street*">
                                    </div>                             
                                  </div>                            
                              </div>  

                              <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="number" name="houseno" placeholder="House No*">
                                  </div>                             
                                </div>  
                              {{-- <div class="row">
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" placeholder="District*">
                                  </div>                             
                                </div> --}}
                                <div class="col-md-6">
                                  <div class="aa-checkout-single-bill">
                                    <input type="text" maxlength="6" minlength="6" name="pin" placeholder="Pincode*">
                                  </div>
                                </div>
                              </div> 
                               <div class="row">
                                <div class="col-md-12">
                                  <div class="aa-checkout-single-bill">
                                    <textarea cols="8" rows="3">Special Notes</textarea>
                                  </div>                             
                                </div>                            
                              </div>   
                              <div class="aa-payment-method">                    
                                  <label for="cashdelivery"><input type="radio" id="cashdelivery" value="COD" name="payment_mode"> Cash on Delivery </label>
                              </div>
                            </div>
                            <button type="submit" class="aa-browse-btn"> Place Order </button>   

                        </form>
                          </div>
                        </div>
                    @endif
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="checkout-right">
                      <h4>Order Summary</h4>
                      <div class="aa-order-summary-area">
                        <table class="table table-responsive">
                          <thead>
                            <tr>
                              <th>Product</th>
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach(Cart::content() as $item)
                            <tr>
                              <td>{{$item->name}} <strong> {{$item->price}} x {{$item->qty}} </strong></td>
                              <td> {{$item->price * $item->qty}} </td>
                            </tr>
                            @endforeach
                            
                            
                          </tbody>
                          <tfoot>
                            {{-- If session has a coupon card the it will show on the cart --}}
                            @if(session()->has('coupon'))
                              <tr>
                                  <th>Coupon({{session()->get('coupon')['name']}})
                                      <form action="{{route('discount.destroy')}}" method="GET" style="display:inline;">
                                        @csrf
                                        <button class="btn btn-danger btn-xs" style="font-size: 12px;" type="submit">Remove</button>
                                      </form>
                                  </th>
                                  <td> - {{session()->get('coupon')['discount']}} </td>
                              </tr>
                            @endif
                            <tr>
                              <th>Subtotal</th>
                              @if(session()->has('coupon'))
                                <td>{{$newSubtotal}}</td>
                              @else
                                <td> {{Cart::subtotal()}} </td>
                              @endif

                            </tr>

                             <tr>
                              <th>GST(12%)</th>
                              @if(session()->has('coupon'))
                                <td>{{$newTax}}</td>
                              @else
                                <td> {{Cart::tax()}} </td>
                              @endif
                            </tr>
                            {{-- <tr>
                                <th>Shipping</th>
                                <td>30</td>
                            </tr> --}}
                             <tr>
                              <th>Total</th>
                              @if(session()->has('coupon'))
                               <td> &#8377 {{$newTotal }}</td>
                              @else 
                                <td> &#8377 {{Cart::total() }}</td>
                              @endif

                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      {{-- <button type="submit" class="aa-cart-view-btn">Place Order </button> --}}
                      {{-- <div class="aa-payment-method">                    
                        <label for="cashdelivery"><input type="radio" id="cashdelivery" name="cod"> Cash on Delivery </label>
                        <label for="paypal"><input type="radio" id="paypal" name="optionsRadios" checked> Via Paypal </label>
                        <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">    
                      </div> --}}
                    </form>
                    </div>
                  </div>
                </div>
              </form>
             </div>
           </div>
         </div>
       </div>
     </section>
     <!-- / Cart view section -->













    {{-- <div class="container" id="billingForm">
            <h2>Billing Address</h2>
            <h4>Your Total: {{$total}}</h4>
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <form action="{{route('checkout')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <label for="first_name">First Name <span>*</span></label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name"  required>
                            </div>

                            <div class="col-md-6">
                                <label for="last_name">Last Name <span>*</span></label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="phone_number">Phone No <span>*</span></label>
                                <input type="text" class="form-control" name="mobile_number" min="10" max="10" placeholder="Enter Mobile Number" required>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="postcode">Pincode <span>*</span></label>
                                <input type="text" class="form-control" name="pin" placeholder="Enter Pincode" required>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="city">City <span>*</span></label>
                                <input type="text" class="form-control" name="city" placeholder="Enter City" required>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="company">House Number <span>*</span></label>
                                <input type="text" class="form-control" name="houseno" required>
                            </div>
                            
                            <div class="col-md-12">
                                    <label for="company">Locality <span>*</span></label>
                                    <input type="text" class="form-control" name="locality" required>
                            </div>

                            <div class="col-md-12">
                                <label for="company">Street <span>*</span></label>
                                <input type="text" class="form-control" name="street" required>
                            </div>
                
                            
                            <div class="col-md-12">
                                <label for="city"> Gender <span>*</span></label>
                                <input type="text" class="form-control" name="gender" required>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="email_address">Email Address <span>*</span></label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="col-md-4" id="bt">
                                    <button type="submit" class="btn btn-warning btn-lg" id="orderBtn" style="margin:auto; display:block;">Place Order</button>
                            </div>
                        </div>
                    </div>
                </form>
           </div>
        </div>
            
            <style>
                #billingForm {
                    margin-top: 10%;
                    color: black;
                }
                #billingForm h2{
                    text-align: center;
                }
                #orderBtn {
                    background-color: tomato;
                    color: white;
                    /* margin: auto;
                    display: block; */
                }
                #bt {
                    float: center;
                }
            </style> --}}
    
    
    
    @include('UI.layouts.footer')          
    @endsection