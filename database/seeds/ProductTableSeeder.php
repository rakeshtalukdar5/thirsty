<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 2; $i++) {
            $product = new Product ([
                'product_name' => 'Can',
                'brand_name' => 'Bisleri',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 20,
                'price' => 60,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bisleri-Can-bisleri-can-1571586689.png' ,
                'slug' => 'Bisleri can',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Can',
                'brand_name' => 'Bailey',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 20,
                'price' => 50,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bailey-Can-bailley-can-1571586605.jpg',
                'slug' => 'Bailey can',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Can',
                'brand_name' => 'Bisleri',
                'type' => 'Mineral',
                'quantity' => 10,
                'volume' => 20,
                'price' => 80,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bisleri-Can-bisleri-can-1571586689.png',
                'slug' => 'Kinely can',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Can',
                'brand_name' => 'Aquafina',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 20,
                'price' => 70,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Aquafina-Can-aquafina-can-1571586640.jpg',
                'slug' => 'Aquafina can',
            ]);
            $product->save();


            $product = new Product ([
                'product_name' => 'Bottle',
                'brand_name' => 'Bisleri',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 1,
                'price' => 18,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bisleri-Bottle-bisleri-bottle-1571586503.jpg',
                'slug' => 'Bisleri bottle',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Bottle',
                'brand_name' => 'Bailey',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 1,
                'price' => 20,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bailey-Bottle-bailey-bottle-1571586570.jpg',
                'slug' => 'Bailey bottle',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Bottle',
                'brand_name' => 'Kinely',
                'type' => 'Mineral',
                'quantity' => 10,
                'volume' => 1,
                'price' => 40,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Bisleri-Can-bisleri-can-1571586689.png',
                'slug' => 'Kinely bottle',
            ]);
            $product->save();

            $product = new Product ([
                'product_name' => 'Bottle',
                'brand_name' => 'Aquafina',
                'type' => 'Ozonied',
                'quantity' => 10,
                'volume' => 1,
                'price' => 20,
                'description' => 'jb webvewuiwvd dvdibvivbe fvbeefvdv',
                'status' => false,
                'banner_image' => 'Aquafina-Bottle-aquafina-bottle-1571586229.jpg',
                'slug' => 'Aquafina bottle',
            ]);
            $product->save();
        }
    }
}
