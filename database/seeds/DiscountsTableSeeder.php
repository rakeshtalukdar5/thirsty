<?php

use Illuminate\Database\Seeder;
use App\Discount;
class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Discount::create([
            'code' => 'ABCD123',
            'type' => 'fixed',
            'value' => 20,
            // 'percent_off' => 30,
            'status' => false,
        ]);
        Discount::create([
            'code' => 'EFGH123',
            'type' => 'percent',
            // 'value' => 20,
            'percent_off' => 30,
            'status' => false,
        ]);
        Discount::create([
            'code' => 'VXYZ123',
            'type' => 'fixed',
            'value' => 20,
            // 'percent_off' => 30,
            'status' => false,
        ]);
        Discount::create([
            'code' => 'PQRS123',
            'type' => 'percent',
            // 'value' => 20,
            'percent_off' => 30,
            'status' => false,
        ]);
    }
}
