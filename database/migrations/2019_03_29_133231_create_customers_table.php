<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->unique();
            // $table->integer('order_id')->unsigned()->nullable();
            // $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->text('mobile_number')->nullable();
            $table->integer('pin')->nullable();
            $table->text('city')->nullable();
            $table->string('gender')->nullable();
            $table->integer('houseno')->nullable();
            $table->text('locality')->nullable();
            $table->text('street')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
