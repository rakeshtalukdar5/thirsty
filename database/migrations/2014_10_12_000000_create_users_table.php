<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('verify_token')->nullable();
            $table->boolean('status')->default(false);
            $table->enum('role',['admin', 'seller', 'user']);
            // $table->string('firstname')->nullable();
            // $table->string('lastname')->nullable();
            // $table->integer('mobile_number')->nullable();
            // $table->integer('pin')->nullable();
            // $table->text('city')->nullable();
            // $table->string('gender')->nullable();
            // $table->integer('houseno')->nullable();
            // $table->text('locality')->nullable();
            // $table->text('street')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
