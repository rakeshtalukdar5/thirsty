<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id')->unsigned()->nullable()->default(null);
            // $table->foreign('seller_id')->references('id')->on('sellers')->onUpdate('cascade')->onDelete('set null');
            // $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('set null');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');;
            $table->date('schedule_date')->nullable();
            $table->time('schedule_time')->nullable();
            $table->text('payment_mode')->nullable();
            $table->enum('order_status',['Pending','Approved','Assigned','Accepted','Shipped','Delivered', 'Cancelled', 'CancelledBySeller', 'CancelledByUser'])->nullable();
            // $table->boolean('shipped')->default(false);
            $table->float('subtotal')->nullable();
            $table->float('total')->nullable();
            $table->float('tax')->nullable();
            $table->float('discount')->default(0);
            $table->string('discount_code')->nullable();
            $table->integer('quantity')->nullable();
            $table->text('order_history');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('billing_email')->nullable();
            $table->text('mobile_number')->nullable();
            $table->integer('pin')->nullable();
            $table->text('city')->nullable();
            $table->string('houseno')->nullable();
            $table->text('locality')->nullable();
            $table->text('street')->nullable();
            $table->text('error')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
