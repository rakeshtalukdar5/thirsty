<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryBoysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_boys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            // $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            // $table->integer('seller_id')->unsigned()->nullable();
            // $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('cascade');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->nullable();
            $table->string('city');
            $table->integer('pin');
            $table->text('address');
            $table->text('mobile_number', 10);
            $table->string('gender')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_boys');
    }
}
