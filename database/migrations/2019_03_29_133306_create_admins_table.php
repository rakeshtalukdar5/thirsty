<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->string('email');
            // $table->string('password');
            $table->text('mobile_number');
            // $table->string('gender');
            // $table->integer('order_product_id')->unsigned()->nullable();
            // $table->foreign('order_product_id')->references('id')->on('order_product')->onUpdate('cascade')->onDelete(null);
            // $table->integer('seller_product_id')->unsigned()->nullable();
            // $table->foreign('seller_product_id')->references('id')->on('seller_product')->onUpdate('cascade')->onDelete(null);
            // $table->integer('user_id')->unsigned()->nullable();
            // $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
