<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            // $table->integer('order_id')->unsigned()->nullable();
            // $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            // $table->integer('product_id')->unsigned()->nullable();
            // $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            // $table->integer('delivery_boy_id')->unsigned()->nullable();
            // $table->foreign('delivery_boy_id')->references('id')->on('delivery_boys')->onDelete('cascade');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            // $table->string('email');
            // $table->string('password');
            $table->string('shop_name')->nullable();
            $table->string('registration_id')->nullable();
            $table->text('mobile_number', 10)->nullable();
            $table->integer('pin')->nullable();
            $table->string('city')->nullable();
            $table->text('locality')->nullable();
            $table->text('street')->nullable();
            $table->boolean('isVerified')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
