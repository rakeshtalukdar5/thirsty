
  //Get Modal element
  let modal = document.getElementById('bookModal');

  //Get open modal button
  let modalBtn = document.getElementById('modalBtn');
  
  //Get close Modal Button
  let closeBtn = document.getElementsByClassName('closeBtn')[0];
  
  //Listen for Open event CLick
  modalBtn.addEventListener('click', openModal);
  
  //Listen for close event click
  closeBtn.addEventListener('click', closeModal);
  
  //Listen for close if clicks outside
   window.addEventListener('click', outsideClick);
  
  function openModal() {
      modal.style.display = 'block';
  }
  
  function closeModal() {
      modal.style.display = 'none';
  }
  
  function outsideClick(e) {
      if (e.target == modal) {
          modal.style.display = 'none';
      }
  }
  