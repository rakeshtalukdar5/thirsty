<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*====================================================================================
                                                  Seller Routes
  =================================================================================== */
//Registration
Route::get('/seller/register', 'SellerController@getRegister')->name('seller.register');
Route::post('/seller/register', 'SellerController@postRegister')->name('seller.register');
Route::get('verify-seller/{email}/{verify_token}', 'SellerController@sendVerifySellerEmail')->name('sendVerifySellerEmail');
Route::get('/seller/login', 'SellerController@getLogin')->name('seller.login');
Route::post('/seller/login', 'SellerController@postLogin')->name('seller.login');
Route::get('/seller/logout', 'SellerController@getLogout')->name('seller.logout');
Route::post('/locationCheck', 'SellerController@locationCheck')->name('sellerLocationCheck');

Route::group(['middleware' => 'sellerAuth'], function() {
    // Route::get('/seller/Dashboard', 'SellerController@index')->name('seller.dashboard');
    Route::get('/seller/Products', 'ProductWithBrandController@getData');
    Route::get('/seller/Products/AddProducts', 'SellerController@create');
    Route::post('/seller/Products/store','ProductWithBrandController@getData');

    //Registration
    Route::get('/seller/Profile', 'SellerController@getSellerProfile')->name('seller.Profile');
    Route::get('/seller/Registration', 'SellerController@getSellerRegistration')->name('seller.Registration');
    Route::post('/seller/Registration', 'SellerController@registerSellerDetails')->name('seller.Registration');

    //Seller Orders
    Route::get('seller/orders/pendingOrder', 'OrderController@getAssignedOrders')->name('seller.assignedOrder');
    Route::post('/Accept-order/{id}','OrderController@changeOrderStatusToAccepted')->name('seller.acceptOrder');
    Route::get('/Accepted-order','OrderController@getAcceptedOrder')->name('seller.acceptedOrder');
    Route::post('/Ship-order/{id}','OrderController@changeOrderStatusToShipped')->name('seller.shipOrder');
    Route::get('/Shipped-order','OrderController@getShippedOrder')->name('seller.shippedOrder');
    Route::post('/Deliver-order/{id}','OrderController@changeOrderStatusToDelivered')->name('seller.deliverOrder');
    Route::get('/Delivered-order','OrderController@getDeliveredOrder')->name('seller.deliveredOrder');
   
    Route::get('seller/cancelled-order','SellerController@getCancelledOrder')->name('seller.cancelledOrder');
    Route::post('seller/cancel-order/{id}','SellerController@changeOrderStatusToCancelled')->name('seller.cancelOrder');    
    Route::get('seller/order-status/','SellerController@getOrderStatus')->name('seller.Order-status');
    Route::get('/seller/dashboard', 'SellerController@getCountersOnDashboard')->name('seller.dashboard');
    Route::get('/seller/rejected-orders-by-others', 'SellerController@rejectedOrders')->name('seller.rejectedOrderByOthers');

});

Route::get('/seller', function() {
    return view('SellerDashboard/welcome');
});

/*=====================================================================================
                                Cart  Routes
  ==================================================================================== */
// Route::resource('cart', 'CartController');
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::get('/cart/{id}', 'CartController@destroy')->name('cart.remove');
Route::get('/cart/Save-for-later/{id}', 'CartController@saveForLater')->name('cart.saveForLater');
Route::get('/checkout', 'CartController@getCheckout')->name('checkout');
Route::post('/checkout', 'ProductController@postCheckout')->name('checkout');



Route::get('/add-to-cart/{id}', 'ProductController@getAddToCart')->name('addToCart');
Route::get('/shopping-cart', 'ProductController@getCart')->name('shoppingCart');
// Route::get('/checkout', 'ProductController@getCheckout')->name('checkout');
// Route::get('/remove/{id}', 'ProductController@getItemRemove')->name('cart.remove');
Route::get('/cart/decrease-quantity/{id}/{qty}', 'CartController@decreaseQtyByOne')->name('cart.decreaseQty');
Route::get('/cart/increase-quantity/{id}//{qty}', 'CartController@increaseQtyByOne')->name('cart.increaseQty');
Route::get('/cart/remove-savelater/{id}', 'CartController@removeFromSaveLater')->name('cart.removeSaveLater');
Route::get('/cart/move-to-cart-from-save-for-later/{id}', 'CartController@moveToCartFromSaveLater')->name('cart.moveToCartFromSaveForLater');



/*================================================================================
                        Admin Routes
=================================================================================*/
    // Route::get('/admin/dashboard', 'AdminController@index');
Route::group(['middleware' => 'adminAuth'], function() {
    Route::group(['prefix' => '/admin'], function() {
   
        Route::get('/Products', 'ProductController@getProductView')->name('admin.viewProducts');
        Route::get('/Products/addProducts', 'ProductController@addProducts')->name('admin.addProducts');
        // Route::get('/Products/addBrands', 'AdminController@addBrands');
        Route::post('/Products/Store','ProductController@storeProducts')->name('admin.storeProducts');
        Route::post('/Products/delete/{id}', 'ProductController@destroy')->name('admin.deleteProduct');
        Route::post('Products/{id}/edit', 'ProductController@edit')->name('admin.editProduct');
        Route::post('/Products/update/{id}', 'ProductController@update')->name('admin.updateProduct');
    
        Route::get('/Products/{id}/view', 'ProductController@show')->name('admin.singleProductView');
        Route::post('/Products/Status/{id}','ProductController@productStatusUpdate')->name('admin.productStatus');
        
        Route::get('/NewOrder','OrderController@getOrdersOnAdmin')->name('admin.newOrder');
        Route::get('/seller-management','AdminController@getSellers')->name('admin.sellerManagement');
        Route::post('/Verify-seller/{id}','AdminController@verifySeller')->name('admin.sellerVerification');

        Route::get('/user-management','AdminController@getUsers')->name('admin.userManagement');
        Route::post('/admin/user-management/block/{id}','AdminController@blockUser')->name('admin.blockUser');

        Route::get('/approved-order','AdminController@getApprovedOrder')->name('admin.approvedOrder');
        Route::post('/approve-order/{id}','AdminController@changeOrderStatusToApproved')->name('admin.approveOrder');

        Route::get('/order-status/','AdminController@getOrderStatus')->name('admin.Order-status');
        Route::post('/order-status/search','AdminController@searchOrderStatus')->name('admin.Search-Order-Status');


        Route::get('/cancelled-order','AdminController@getCancelledOrder')->name('admin.cancelledOrder');
        Route::post('/cancel-order/{id}','AdminController@changeOrderStatusToCancelled')->name('admin.cancelOrder');
        
        Route::get('/delivered-order','AdminController@getDeliveredOrder')->name('admin.deliveredOrder');

        //Blog    
        Route::get('/blog/create','BlogController@getBlogPage')->name('admin.blogsCreate');
        Route::post('/blog','BlogController@postBlog')->name('admin.postBlog');
        Route::get('/blog/view','BlogController@blogsShowInAdmin')->name('admin.blogsView');
        Route::post('/blog/delete/{id}', 'BlogController@delete')->name('admin.deleteBlog');
        Route::post('/blog/edit/{id}', 'BlogController@edit')->name('admin.editBlog');
        Route::post('/blog/update/{id}', 'BlogController@update')->name('admin.updateBlog');

    });

        //Orders
        // Route::get('/customer/order', 'AdminController@orderStore');
        Route::get('/admin/dashboard', 'AdminController@getCountersOnDashboard')->name('admin.dashboard');
        Route::get('order/view/{id}', 'OrderController@orderProductView')->name('order.detail.view');
        // Route::post('admin/order/assignOrder/{id}', 'OrderController@assignOrder')->name('admin.assignOrder');


        //Discount
        Route::get('/admin/discount', 'DiscountController@index')->name('admin.discountIndex');
        Route::post('/admin/discount', 'DiscountController@setDiscount')->name('admin.setDiscount');
        Route::post('/admin/discount/status/{id}', 'DiscountController@changeDiscountStatus')->name('admin.discountStatus');
        Route::post('/admin/discount/delete/{id}', 'DiscountController@deleteDiscountCode')->name('admin.deleteDiscount');
        Route::post('/admin/discount/{id}/edit', 'DiscountController@edit')->name('admin.editDiscount');
        Route::post('/admin/discount/update/{id}', 'DiscountController@update')->name('admin.updateDiscount');
        Route::get('/admin/discount/items','DiscountController@getDiscountOnItemPage')->name('admin.itemDiscount');


        //contact
        Route::get('/contact', 'UserController@getContactPage')->name('contact');
        Route::post('/contact/feedback','UserController@postFeedback')->name('user.feedback');
        Route::get('/admin/feedback','AdminController@getFeedback')->name('admin.feedback');
        Route::post('/admin/feedback/delete/{id}','AdminController@deleteFeedback')->name('admin.deleteFeedback');
});


//Admin Registration
Route::get('/admin/register', 'AdminController@getRegister')->name('admin.register');
Route::post('/admin/register', 'AdminController@postRegister')->name('admin.register');
Route::get('verify-admin/{email}/{verify_token}', 'AdminController@sendVerifyAdminEmail')->name('sendVerifyAdminEmail');
Route::get('/admin/login', 'AdminController@getLogin')->name('admin.login');
Route::post('/admin/login', 'AdminController@postLogin')->name('admin.login');
Route::get('/admin/logout', 'AdminController@getLogout')->name('admin.logout');

Route::get('/admin', function() {
    return view('adminDashboard/welcome');
});

//Blog for user
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog-in-index', 'BlogController@getBlogInIndex')->name('index.blog');
Route::get('/blog/{id}', 'BlogController@getSingleBlog')->name('blog.singleBlog');

//Assign Order to seller
// Route::post('/admin/assign-order/{$pin}','AdminController@assignOrder')->name('admin.assignOrder');
Route::post('admin/order/assignOrder/{id}', 'OrderController@assignOrder')->name('admin.assignOrder');
Route::get('seller/order/getAssignedOrder', 'SellerController@getAssignedOrder')->name('seller.getAssignedOrder');

// Route::post('/Products/storeBrands','AdminController@storeBrands');


/*================================================================================
                        User Routes
=================================================================================*/
//Users Sign Up and Sign In
Route::group(['prefix' => '/user'], function(){
    Route::group(['middleware' => 'guest'], function(){
        Route::get('/signup', 'UserController@getSignup')->name('user.signup');
        Route::post('/signup', 'UserController@postSignup')->name('user.signup');
        Route::get('/signin', 'UserController@getSignin')->name('user.signin');
        Route::post('/signin', 'UserController@postSignin')->name('user.signin');
    });
    Route::group(['middleware' => 'auth'], function(){
        Route::get('/profile', 'UserController@getUserProfile')->name('user.profile');
        Route::get('/myOrders', 'UserController@getMyOrders')->name('user.myOrder');
        Route::get('/myOrders/cancel-order/{id}', 'UserController@cancelOrder')->name('user.cancelOrder');
        Route::get('/logout', 'UserController@getLogout')->name('user.logout');
    });
});
Route::get('verify-customer/{email}/{verify_token}', 'UserController@sendVerifyEmailDone')->name('sendVerifyEmailDone');
Route::get('user/forgotPassword','UserController@getForgotPassword')->name('user.forgotPassword');
Route::post('user/forgotPassword','UserController@postForgotPassword')->name('user.forgotPassword');
Route::get('/user/passwordReset', 'UserController@getPasswordReset')->name('user.passwordReset');
Route::post('/user/passwordReset', 'UserController@postPasswordReset')->name('user.passwordReset');
//Edit User Profile
Route::get('user/profile/edit/{id}', 'UserController@getProfileEdit')->name('user.editProfile');
Route::post('user/profile/update/{id}', 'UserController@updateProfile')->name('user.updateProfile');
//Edit User Password
Route::get('user/password/edit/{id}', 'UserController@getPasswordEdit')->name('user.editPassword');
Route::post('user/password/update/{id}', 'UserController@updatePassword')->name('user.updatePassword');
//Jquery Ajax current password check
Route::post('/user/check-password/{id}', 'UserController@getCheckPasssword');



//Delivery Boy 
Route::get('/Delivery-boy', 'DeliveryBoyController@index' );
Route::get('/AddDeliveryBoy', 'DeliveryBoyController@create');
Route::post('/DeliveryBoy/store', 'DeliveryBoyController@store');
Route::post('/Delivery-Boy/delete/{id}', 'DeliveryBoyController@destroy');
Route::post('/Delivery-Boy/{id}/edit', 'DeliveryBoyController@edit');
Route::post('/Delivery-Boy/update/{id}', 'DeliveryBoyController@update');


//shop
Route::get('/', 'ProductController@getProductsOnIndex')->name('landing_page');
Route::get('/shop', 'ProductController@getProductsOnShop')->name('shop');
Route::get('/shop/{product}', 'ProductController@getSingleProductShow')->name('shop.product');
Route::post('/search', 'ProductController@searchProduct')->name('searchProduct');
//Discount Coupon
Route::post('/discount', 'DiscountController@store')->name('discount.store');
Route::get('/discount','DiscountController@destroy')->name('discount.destroy');
Route::post('/order', 'CartController@storeOrder')->name('order');



/*
Route::get('/', 'ProductController@getProductsInForm');
Route::get('/form', 'ProductWithBrandController@index' );
Route::post('/form/search', 'ProductWithBrandController@getData')->name('test.getData');
Route::get('/home', 'HomeController@index')->name('home');
*/